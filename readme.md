# Manufacturing

## PCB

__Shield__ is a PCB designed to be attached on the FPGA generator made from Terasic DE0 Nano kit.

__Transducer Board__ is a PCB used for housing an 8x8 array of transducers.