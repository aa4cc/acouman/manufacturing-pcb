<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="projectLib">
<packages>
<package name="MA40S4S" urn="urn:adsk.eagle:footprint:5239/1" locally_modified="yes">
<description>&lt;b&gt;BUZZER&lt;/b&gt;</description>
<circle x="0" y="0" radius="5" width="0.2" layer="21"/>
<text x="3.449" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<pad name="1" x="-2.5" y="0" drill="0.9"/>
<pad name="2" x="2.5" y="0" drill="0.9"/>
<circle x="-3.25" y="1.75" radius="0.75" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.25" x2="-3.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.25" y1="1.25" x2="-3.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.25" y1="1.75" x2="-3.75" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.25" y1="1.75" x2="-2.75" y2="1.75" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SPEAKER">
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="7.0104" y="3.7338" size="1.778" layer="95" rot="R270">&gt;NAME</text>
<text x="4.3434" y="4.4704" size="1.778" layer="96" rot="R270">&gt;VALUE</text>
<wire x1="-0.762" y1="4.318" x2="-1.778" y2="4.318" width="0.127" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="4.826" width="0.127" layer="94"/>
<circle x="-1.27" y="4.318" radius="0.915809375" width="0.1778" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA40S4S" prefix="G">
<gates>
<gate name="G$1" symbol="SPEAKER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA40S4S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-harting-ml" urn="urn:adsk.eagle:library:149">
<description>&lt;b&gt;Harting  &amp; 3M Connectors&lt;/b&gt;&lt;p&gt;
Low profile connectors, straight&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="ML16" urn="urn:adsk.eagle:footprint:6918/1" library_version="1">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-12.7" y1="3.175" x2="12.7" y2="3.175" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-3.175" x2="12.7" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="3.175" x2="-12.7" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="13.97" y1="-4.445" x2="9.271" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="13.97" y1="-4.445" x2="13.97" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-13.97" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-3.175" x2="8.382" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-4.318" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.445" x2="12.7" y2="4.699" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.699" x2="11.43" y2="4.699" width="0.1524" layer="21"/>
<wire x1="11.43" y1="4.445" x2="11.43" y2="4.699" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.445" x2="13.97" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="11.43" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="4.699" x2="-12.7" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.699" x2="-12.7" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="4.699" x2="-11.43" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="5.969" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.858" y1="-3.175" x2="6.858" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="6.858" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-3.175" x2="8.382" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-3.175" x2="6.858" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.969" y1="-4.445" x2="6.35" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-3.937" x2="9.271" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-3.937" x2="8.382" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="6.858" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-3.429" x2="12.954" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="12.954" y1="-3.429" x2="12.954" y2="3.429" width="0.0508" layer="21"/>
<wire x1="12.954" y1="3.429" x2="-12.954" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-12.954" y1="3.429" x2="-12.954" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-12.954" y1="-3.429" x2="-5.842" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.858" y1="-3.429" x2="6.858" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="6.858" y1="-3.937" x2="6.35" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-3.429" x2="8.382" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-3.937" x2="6.858" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="-4.445" x2="-9.652" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-9.652" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.652" y1="-4.318" x2="-8.128" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-4.445" x2="-8.128" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-4.445" x2="-6.731" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-3.429" x2="-5.842" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-3.429" x2="-4.318" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-5.842" y1="-3.175" x2="-5.842" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-3.175" x2="-12.7" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-3.175" x2="-4.318" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-3.175" x2="-5.842" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-3.937" x2="-5.842" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-3.937" x2="-6.35" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-4.445" x2="-6.35" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.937" x2="-3.429" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.937" x2="-4.318" y2="-3.937" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-13.97" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">16</text>
<text x="-11.43" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-11.43" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
</package>
<package name="ML16L" urn="urn:adsk.eagle:footprint:6919/1" library_version="1">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-10.16" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="21"/>
<wire x1="-7.62" y1="10.16" x2="-8.89" y2="7.62" width="0.254" layer="21"/>
<wire x1="-8.89" y1="7.62" x2="-10.16" y2="10.16" width="0.254" layer="21"/>
<wire x1="6.604" y1="9.906" x2="6.604" y2="10.922" width="0.1524" layer="21"/>
<wire x1="6.604" y1="9.906" x2="8.636" y2="9.906" width="0.1524" layer="21"/>
<wire x1="8.636" y1="10.922" x2="8.636" y2="9.906" width="0.1524" layer="21"/>
<wire x1="6.858" y1="5.969" x2="8.382" y2="5.969" width="0.1524" layer="21" curve="-180"/>
<wire x1="8.382" y1="5.969" x2="8.382" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.382" y1="3.683" x2="9.779" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="10.922" x2="2.159" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="10.922" x2="5.461" y2="10.922" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="-2.159" y2="4.445" width="0.1524" layer="21"/>
<wire x1="2.159" y1="3.429" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="4.445" x2="-2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="2.159" y1="4.445" x2="2.159" y2="3.429" width="0.1524" layer="21"/>
<wire x1="6.858" y1="5.969" x2="6.858" y2="4.445" width="0.1524" layer="21"/>
<wire x1="6.858" y1="4.445" x2="6.858" y2="3.683" width="0.1524" layer="21"/>
<wire x1="6.858" y1="4.445" x2="8.382" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.382" y1="4.445" x2="8.382" y2="3.683" width="0.1524" layer="21"/>
<wire x1="9.779" y1="3.683" x2="9.779" y2="10.922" width="0.1524" layer="21"/>
<wire x1="9.779" y1="3.683" x2="9.779" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.032" x2="-8.255" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="2.032" x2="-7.239" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.032" x2="-5.715" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.032" x2="-4.445" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.032" x2="-3.175" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-3.175" y1="2.032" x2="-2.921" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.032" x2="-0.635" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="2.032" x2="0.635" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.032" x2="0.635" y2="2.032" width="0.1524" layer="51"/>
<wire x1="1.27" y1="10.033" x2="1.27" y2="10.287" width="0.508" layer="21"/>
<wire x1="10.541" y1="4.445" x2="13.335" y2="4.445" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.445" x2="13.335" y2="8.255" width="0.1524" layer="21"/>
<wire x1="10.541" y1="8.255" x2="13.335" y2="8.255" width="0.1524" layer="21"/>
<wire x1="10.541" y1="8.255" x2="10.541" y2="4.445" width="0.1524" layer="21"/>
<wire x1="5.461" y1="3.683" x2="6.858" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.461" y1="3.683" x2="5.461" y2="10.922" width="0.1524" layer="21"/>
<wire x1="5.461" y1="3.683" x2="5.461" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="10.922" x2="-6.096" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="2.032" x2="-7.239" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="3.683" x2="-7.239" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="3.683" x2="-5.842" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="4.445" x2="-5.842" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="3.683" x2="-2.921" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="3.683" x2="-2.921" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="4.445" x2="-4.318" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="4.445" x2="-4.318" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="5.969" x2="-5.842" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="5.969" x2="-4.318" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="5.969" x2="-4.318" y2="5.969" width="0.1524" layer="21" curve="-180"/>
<wire x1="-6.096" y1="9.906" x2="-6.096" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="10.922" x2="-4.064" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="9.906" x2="-4.064" y2="9.906" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="10.922" x2="-4.064" y2="9.906" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="10.922" x2="-2.921" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="3.683" x2="-2.921" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="10.922" x2="-2.159" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="10.033" x2="-1.27" y2="10.287" width="0.508" layer="21"/>
<wire x1="4.445" y1="2.032" x2="3.175" y2="2.032" width="0.1524" layer="51"/>
<wire x1="1.905" y1="2.032" x2="3.175" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.032" x2="5.461" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.032" x2="8.255" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.032" x2="5.715" y2="2.032" width="0.1524" layer="51"/>
<wire x1="9.525" y1="2.032" x2="8.255" y2="2.032" width="0.1524" layer="51"/>
<wire x1="5.461" y1="10.922" x2="13.97" y2="10.922" width="0.1524" layer="21"/>
<wire x1="13.97" y1="2.032" x2="13.97" y2="10.922" width="0.1524" layer="21"/>
<wire x1="13.97" y1="2.032" x2="12.827" y2="2.032" width="0.1524" layer="21"/>
<wire x1="12.827" y1="2.032" x2="11.811" y2="2.032" width="0.1524" layer="21"/>
<wire x1="11.811" y1="2.032" x2="9.779" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="10.922" x2="-13.97" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="2.032" x2="-13.97" y2="10.922" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="2.032" x2="-12.827" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-12.827" y1="2.032" x2="-11.811" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="2.032" x2="-9.525" y2="2.032" width="0.1524" layer="21"/>
<wire x1="9.779" y1="2.032" x2="9.525" y2="2.032" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.032" x2="5.715" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="2.032" x2="-1.905" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="2.032" x2="-6.985" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-12.827" y1="2.032" x2="-12.827" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="1.397" x2="-12.827" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="1.397" x2="-11.811" y2="2.032" width="0.1524" layer="21"/>
<wire x1="11.811" y1="2.032" x2="11.811" y2="1.397" width="0.1524" layer="21"/>
<wire x1="12.827" y1="1.397" x2="11.811" y2="1.397" width="0.1524" layer="21"/>
<wire x1="12.827" y1="1.397" x2="12.827" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-10.9728" y="-1.6764" size="1.27" layer="21" ratio="10">1</text>
<text x="-10.9982" y="0.4826" size="1.27" layer="21" ratio="10">2</text>
<text x="-13.9954" y="11.43" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0254" y="11.43" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="12.7" y="5.08" size="1.524" layer="21" ratio="10" rot="R90">16</text>
<rectangle x1="1.016" y1="4.445" x2="1.524" y2="10.287" layer="21"/>
<rectangle x1="-10.033" y1="9.652" x2="-7.747" y2="10.16" layer="21"/>
<rectangle x1="-9.779" y1="9.144" x2="-8.001" y2="9.652" layer="21"/>
<rectangle x1="-9.525" y1="8.636" x2="-8.255" y2="9.144" layer="21"/>
<rectangle x1="-9.271" y1="8.128" x2="-8.509" y2="8.636" layer="21"/>
<rectangle x1="-9.017" y1="7.874" x2="-8.763" y2="8.128" layer="21"/>
<rectangle x1="-1.524" y1="4.445" x2="-1.016" y2="10.287" layer="21"/>
<rectangle x1="-9.144" y1="-0.381" x2="-8.636" y2="0.381" layer="21"/>
<rectangle x1="-9.144" y1="0.381" x2="-8.636" y2="2.032" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-0.381" layer="51"/>
<rectangle x1="-6.604" y1="-0.381" x2="-6.096" y2="0.381" layer="21"/>
<rectangle x1="-6.604" y1="0.381" x2="-6.096" y2="2.032" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-0.381" layer="51"/>
<rectangle x1="-4.064" y1="0.381" x2="-3.556" y2="2.032" layer="51"/>
<rectangle x1="-4.064" y1="-0.381" x2="-3.556" y2="0.381" layer="21"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-0.381" layer="51"/>
<rectangle x1="-1.524" y1="-0.381" x2="-1.016" y2="0.381" layer="21"/>
<rectangle x1="-1.524" y1="0.381" x2="-1.016" y2="2.032" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-0.381" layer="51"/>
<rectangle x1="1.016" y1="0.381" x2="1.524" y2="2.032" layer="51"/>
<rectangle x1="1.016" y1="-0.381" x2="1.524" y2="0.381" layer="21"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-0.381" layer="51"/>
<rectangle x1="3.556" y1="0.381" x2="4.064" y2="2.032" layer="51"/>
<rectangle x1="3.556" y1="-0.381" x2="4.064" y2="0.381" layer="21"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-0.381" layer="51"/>
<rectangle x1="6.096" y1="0.381" x2="6.604" y2="2.032" layer="51"/>
<rectangle x1="6.096" y1="-0.381" x2="6.604" y2="0.381" layer="21"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-0.381" layer="51"/>
<rectangle x1="8.636" y1="0.381" x2="9.144" y2="2.032" layer="51"/>
<rectangle x1="8.636" y1="-0.381" x2="9.144" y2="0.381" layer="21"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-0.381" layer="51"/>
</package>
<package name="3M_16" urn="urn:adsk.eagle:footprint:6920/1" library_version="1">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="19.685" y1="-4.2418" x2="19.685" y2="4.3" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="4.3" x2="-19.685" y2="-4.2418" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="-4.3" x2="-2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.3" x2="-2.54" y2="-3.048" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="2.54" y1="-4.3" x2="19.431" y2="-4.3" width="0.3048" layer="21"/>
<wire x1="19.685" y1="4.3" x2="-19.685" y2="4.3" width="0.3048" layer="21"/>
<wire x1="12.7" y1="-3" x2="2.54" y2="-3" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3" x2="-12.7" y2="-3" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="-3" x2="-12.7" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="12.573" y1="3" x2="-12.7" y2="3" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="1.27" x2="-12.7" y2="3" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="-1.27" x2="-19.558" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="1.27" x2="-19.558" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="2.54" y2="-4.318" width="0.3048" layer="21"/>
<wire x1="12.7" y1="-3" x2="12.7" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="12.7" y1="1.27" x2="12.7" y2="3" width="0.3048" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="19.558" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="12.7" y1="1.27" x2="19.558" y2="1.27" width="0.3048" layer="21"/>
<pad name="1" x="-8.89" y="-1.27" drill="0.8128" shape="square"/>
<pad name="2" x="-8.89" y="1.27" drill="0.8128" shape="square"/>
<pad name="3" x="-6.35" y="-1.27" drill="0.8128" shape="square"/>
<pad name="4" x="-6.35" y="1.27" drill="0.8128" shape="square"/>
<pad name="5" x="-3.81" y="-1.27" drill="0.8128" shape="square"/>
<pad name="6" x="-3.81" y="1.27" drill="0.8128" shape="square"/>
<pad name="8" x="-1.27" y="1.27" drill="0.8128" shape="square"/>
<pad name="9" x="1.27" y="-1.27" drill="0.8128" shape="square"/>
<pad name="10" x="1.27" y="1.27" drill="0.8128" shape="square"/>
<pad name="11" x="3.81" y="-1.27" drill="0.8128" shape="square"/>
<pad name="12" x="3.81" y="1.27" drill="0.8128" shape="square"/>
<pad name="13" x="6.35" y="-1.27" drill="0.8128" shape="square"/>
<pad name="14" x="6.35" y="1.27" drill="0.8128" shape="square"/>
<pad name="15" x="8.89" y="-1.27" drill="0.8128" shape="square"/>
<pad name="16" x="8.89" y="1.27" drill="0.8128" shape="square"/>
<pad name="7" x="-1.27" y="-1.27" drill="0.8128" shape="square"/>
<text x="-19.05" y="5.08" size="2.54" layer="25">&gt;NAME</text>
<text x="5.08" y="5.08" size="2.54" layer="27">&gt;VALUE</text>
<polygon width="0.3048" layer="21">
<vertex x="-10.287" y="-4.826"/>
<vertex x="-7.493" y="-4.826"/>
<vertex x="-8.89" y="-5.969"/>
</polygon>
</package>
<package name="3M_16L" urn="urn:adsk.eagle:footprint:6921/1" library_version="1">
<description>&lt;b&gt;3M&lt;/b&gt;</description>
<wire x1="-19.685" y1="-6.0198" x2="-17.145" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="19.685" y1="-6.0198" x2="19.685" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-17.145" y1="-6.0198" x2="-17.145" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-17.145" y1="-6.0198" x2="-12.4714" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-17.145" y1="-2.032" x2="-14.8082" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="-14.8082" y1="-0.4572" x2="-12.4714" y2="-2.032" width="0.3048" layer="21"/>
<wire x1="-12.4714" y1="-2.032" x2="-12.4714" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-12.4714" y1="-6.0198" x2="12.4714" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="12.4714" y1="-6.0198" x2="12.4714" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="12.4714" y1="-2.0574" x2="14.8082" y2="-0.4572" width="0.3048" layer="21"/>
<wire x1="14.8082" y1="-0.4572" x2="17.145" y2="-2.0574" width="0.3048" layer="21"/>
<wire x1="17.145" y1="-2.0574" x2="17.145" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="12.4714" y1="-6.0198" x2="17.145" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="17.145" y1="-6.0198" x2="19.685" y2="-6.0198" width="0.3048" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-16.8148" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="19.685" y1="2.54" x2="16.8148" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="10.9982" x2="-16.8148" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-12.7" y1="8.89" x2="-12.7" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="7.874" x2="-2.54" y2="6.604" width="0.3048" layer="21" curve="-90"/>
<wire x1="-12.7" y1="8.89" x2="-11.684" y2="7.874" width="0.3048" layer="21" curve="90"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="6.6802" width="0.3048" layer="21"/>
<wire x1="2.54" y1="6.604" x2="3.81" y2="7.874" width="0.3048" layer="21" curve="-90"/>
<wire x1="-12.7" y1="1.27" x2="-12.7" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="5.08" x2="-12.7" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.27" x2="-10.16" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.7" y1="10.9982" x2="16.8148" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="-11.684" y1="7.874" x2="11.684" y2="7.874" width="0.3048" layer="21"/>
<wire x1="12.7" y1="8.89" x2="12.7" y2="10.9982" width="0.3048" layer="21"/>
<wire x1="11.684" y1="7.874" x2="12.7" y2="8.89" width="0.3048" layer="21" curve="90"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.27" x2="12.7" y2="3.81" width="0.1524" layer="21"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="7.62" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.27" x2="10.16" y2="1.27" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<circle x="-15.5448" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<circle x="15.5448" y="3.81" radius="0.9398" width="0.1524" layer="21"/>
<circle x="-14.8082" y="-3.2766" radius="1.9304" width="0" layer="42"/>
<circle x="14.8082" y="-3.2766" radius="1.9304" width="0" layer="42"/>
<circle x="-14.8082" y="-3.2766" radius="1.9304" width="0" layer="41"/>
<circle x="14.8082" y="-3.2766" radius="1.9304" width="0" layer="41"/>
<pad name="1" x="-8.89" y="-5.08" drill="0.8128" shape="square"/>
<pad name="2" x="-8.89" y="-2.54" drill="0.8128" shape="square"/>
<pad name="3" x="-6.35" y="-5.08" drill="0.8128" shape="square"/>
<pad name="4" x="-6.35" y="-2.54" drill="0.8128" shape="square"/>
<pad name="5" x="-3.81" y="-5.08" drill="0.8128" shape="square"/>
<pad name="6" x="-3.81" y="-2.54" drill="0.8128" shape="square"/>
<pad name="8" x="-1.27" y="-2.54" drill="0.8128" shape="square"/>
<pad name="9" x="1.27" y="-5.08" drill="0.8128" shape="square"/>
<pad name="10" x="1.27" y="-2.54" drill="0.8128" shape="square"/>
<pad name="11" x="3.81" y="-5.08" drill="0.8128" shape="square"/>
<pad name="12" x="3.81" y="-2.54" drill="0.8128" shape="square"/>
<pad name="13" x="6.35" y="-5.08" drill="0.8128" shape="square"/>
<pad name="14" x="6.35" y="-2.54" drill="0.8128" shape="square"/>
<pad name="15" x="8.89" y="-5.08" drill="0.8128" shape="square"/>
<pad name="16" x="8.89" y="-2.54" drill="0.8128" shape="square"/>
<pad name="7" x="-1.27" y="-5.08" drill="0.8128" shape="square"/>
<text x="-19.05" y="-10.16" size="2.54" layer="25">&gt;NAME</text>
<text x="5.08" y="-10.16" size="2.54" layer="27">&gt;VALUE</text>
<hole x="-14.8082" y="-3.2766" drill="2.54"/>
<hole x="14.8082" y="-3.2766" drill="2.54"/>
<polygon width="0.3048" layer="21">
<vertex x="-10.16" y="6.35"/>
<vertex x="-7.62" y="6.35"/>
<vertex x="-8.89" y="3.81"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="ML16" urn="urn:adsk.eagle:package:6969/1" type="box" library_version="1">
<description>HARTING</description>
<packageinstances>
<packageinstance name="ML16"/>
</packageinstances>
</package3d>
<package3d name="ML16L" urn="urn:adsk.eagle:package:6973/1" type="box" library_version="1">
<description>HARTING</description>
<packageinstances>
<packageinstance name="ML16L"/>
</packageinstances>
</package3d>
<package3d name="3M_16" urn="urn:adsk.eagle:package:6960/1" type="box" library_version="1">
<description>3M</description>
<packageinstances>
<packageinstance name="3M_16"/>
</packageinstances>
</package3d>
<package3d name="3M_16L" urn="urn:adsk.eagle:package:6968/1" type="box" library_version="1">
<description>3M</description>
<packageinstances>
<packageinstance name="3M_16L"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="16P" urn="urn:adsk.eagle:symbol:6917/1" library_version="1">
<wire x1="3.81" y1="-12.7" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<text x="-3.81" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ML16" urn="urn:adsk.eagle:component:7002/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="16P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML16">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6969/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="L" package="ML16L">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6973/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="3M" package="3M_16">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6960/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="3ML" package="3M_16L">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6968/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G65" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G66" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G67" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G68" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G69" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G70" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G71" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G72" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G73" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G74" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G75" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G76" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G77" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G78" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G79" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G80" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G81" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G82" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G83" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G84" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G85" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G86" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G87" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G88" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G89" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G90" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G91" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G92" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G93" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G94" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G95" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G96" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G97" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G98" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G99" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G100" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G101" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G102" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G103" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G104" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G105" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G106" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G107" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G108" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G109" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G110" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G111" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G112" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G113" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G114" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G115" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G116" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G117" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G118" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G119" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G120" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G121" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G122" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G123" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G124" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G125" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G126" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G127" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G128" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SV9" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH0-CH7"/>
<part name="SV10" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH7-CH15"/>
<part name="SV11" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH16-CH23"/>
<part name="SV12" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH24-CH31"/>
<part name="SV13" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH32-CH39"/>
<part name="SV14" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH40-CH47"/>
<part name="SV15" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH48-CH55"/>
<part name="SV16" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH56-CH63"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND49" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G193" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G194" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G195" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G196" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G197" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G198" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G199" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G200" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND50" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G201" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G202" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G203" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G204" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G205" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G206" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G207" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G208" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND51" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G209" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G210" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G211" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G212" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G213" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G214" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G215" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G216" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND52" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G217" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G218" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G219" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G220" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G221" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G222" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G223" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G224" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND53" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G225" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G226" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G227" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G228" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G229" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G230" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G231" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G232" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND54" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G233" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G234" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G235" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G236" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G237" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G238" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G239" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G240" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND55" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G241" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G242" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G243" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G244" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G245" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G246" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G247" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G248" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND56" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G249" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G250" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G251" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G252" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G253" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G254" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G255" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G256" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND57" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SV25" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH0-CH7"/>
<part name="SV26" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH7-CH15"/>
<part name="SV27" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH16-CH23"/>
<part name="SV28" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH24-CH31"/>
<part name="SV29" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH32-CH39"/>
<part name="SV30" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH40-CH47"/>
<part name="SV31" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH48-CH55"/>
<part name="SV32" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH56-CH63"/>
<part name="GND58" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND59" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND60" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND61" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND62" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND63" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND64" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G129" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G130" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G131" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G132" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G133" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G134" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G135" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G136" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G137" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G138" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G139" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G140" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G141" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G142" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G143" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G144" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G145" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G146" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G147" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G148" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G149" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G150" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G151" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G152" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G153" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G154" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G155" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G156" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G157" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G158" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G159" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G160" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G161" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G162" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G163" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G164" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G165" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G166" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G167" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G168" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G169" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G170" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G171" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G172" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G173" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G174" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G175" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G176" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G177" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G178" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G179" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G180" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G181" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G182" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G183" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G184" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G185" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G186" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G187" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G188" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G189" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G190" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G191" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G192" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SV17" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH0-CH7"/>
<part name="SV18" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH7-CH15"/>
<part name="SV19" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH16-CH23"/>
<part name="SV20" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH24-CH31"/>
<part name="SV21" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH32-CH39"/>
<part name="SV22" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH40-CH47"/>
<part name="SV23" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH48-CH55"/>
<part name="SV24" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH56-CH63"/>
<part name="GND42" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND43" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND45" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND46" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND47" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND48" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND65" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G257" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G258" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G259" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G260" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G261" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G262" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G263" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G264" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND66" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G265" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G266" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G267" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G268" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G269" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G270" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G271" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G272" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND67" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G273" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G274" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G275" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G276" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G277" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G278" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G279" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G280" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND68" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G281" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G282" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G283" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G284" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G285" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G286" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G287" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G288" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND69" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G289" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G290" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G291" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G292" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G293" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G294" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G295" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G296" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND70" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G297" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G298" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G299" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G300" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G301" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G302" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G303" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G304" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND71" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G305" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G306" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G307" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G308" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G309" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G310" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G311" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G312" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND72" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="G313" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G314" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G315" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G316" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G317" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G318" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G319" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="G320" library="projectLib" deviceset="MA40S4S" device=""/>
<part name="GND73" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SV33" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH0-CH7"/>
<part name="SV34" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH7-CH15"/>
<part name="SV35" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH16-CH23"/>
<part name="SV36" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH24-CH31"/>
<part name="SV37" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH32-CH39"/>
<part name="SV38" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH40-CH47"/>
<part name="SV39" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH48-CH55"/>
<part name="SV40" library="con-harting-ml" library_urn="urn:adsk.eagle:library:149" deviceset="ML16" device="" package3d_urn="urn:adsk.eagle:package:6969/1" value="CH56-CH63"/>
<part name="GND74" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND75" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND76" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND77" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND78" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND79" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND80" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="GND17" gate="1" x="193.04" y="43.18" smashed="yes">
<attribute name="VALUE" x="190.5" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="G65" gate="G$1" x="50.8" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="220.3704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="217.7034" size="1.778" layer="96"/>
</instance>
<instance part="G66" gate="G$1" x="50.8" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="207.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="205.0034" size="1.778" layer="96"/>
</instance>
<instance part="G67" gate="G$1" x="50.8" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="194.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="192.3034" size="1.778" layer="96"/>
</instance>
<instance part="G68" gate="G$1" x="50.8" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="182.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="179.6034" size="1.778" layer="96"/>
</instance>
<instance part="G69" gate="G$1" x="50.8" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="169.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="166.9034" size="1.778" layer="96"/>
</instance>
<instance part="G70" gate="G$1" x="50.8" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="156.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="154.2034" size="1.778" layer="96"/>
</instance>
<instance part="G71" gate="G$1" x="50.8" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="144.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="141.5034" size="1.778" layer="96"/>
</instance>
<instance part="G72" gate="G$1" x="50.8" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="131.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="128.8034" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="58.42" y="116.84" smashed="yes">
<attribute name="VALUE" x="55.88" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="G73" gate="G$1" x="114.3" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="220.3704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="217.7034" size="1.778" layer="96"/>
</instance>
<instance part="G74" gate="G$1" x="114.3" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="207.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="205.0034" size="1.778" layer="96"/>
</instance>
<instance part="G75" gate="G$1" x="114.3" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="194.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="192.3034" size="1.778" layer="96"/>
</instance>
<instance part="G76" gate="G$1" x="114.3" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="182.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="179.6034" size="1.778" layer="96"/>
</instance>
<instance part="G77" gate="G$1" x="114.3" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="169.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="166.9034" size="1.778" layer="96"/>
</instance>
<instance part="G78" gate="G$1" x="114.3" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="156.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="154.2034" size="1.778" layer="96"/>
</instance>
<instance part="G79" gate="G$1" x="114.3" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="144.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="141.5034" size="1.778" layer="96"/>
</instance>
<instance part="G80" gate="G$1" x="114.3" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="131.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="128.8034" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="121.92" y="116.84" smashed="yes">
<attribute name="VALUE" x="119.38" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="G81" gate="G$1" x="177.8" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="220.3704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="217.7034" size="1.778" layer="96"/>
</instance>
<instance part="G82" gate="G$1" x="177.8" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="207.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="205.0034" size="1.778" layer="96"/>
</instance>
<instance part="G83" gate="G$1" x="177.8" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="194.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="192.3034" size="1.778" layer="96"/>
</instance>
<instance part="G84" gate="G$1" x="177.8" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="182.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="179.6034" size="1.778" layer="96"/>
</instance>
<instance part="G85" gate="G$1" x="177.8" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="169.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="166.9034" size="1.778" layer="96"/>
</instance>
<instance part="G86" gate="G$1" x="177.8" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="156.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="154.2034" size="1.778" layer="96"/>
</instance>
<instance part="G87" gate="G$1" x="177.8" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="144.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="141.5034" size="1.778" layer="96"/>
</instance>
<instance part="G88" gate="G$1" x="177.8" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="131.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="128.8034" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="185.42" y="116.84" smashed="yes">
<attribute name="VALUE" x="182.88" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="G89" gate="G$1" x="241.3" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="220.3704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="217.7034" size="1.778" layer="96"/>
</instance>
<instance part="G90" gate="G$1" x="241.3" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="207.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="205.0034" size="1.778" layer="96"/>
</instance>
<instance part="G91" gate="G$1" x="241.3" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="194.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="192.3034" size="1.778" layer="96"/>
</instance>
<instance part="G92" gate="G$1" x="241.3" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="182.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="179.6034" size="1.778" layer="96"/>
</instance>
<instance part="G93" gate="G$1" x="241.3" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="169.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="166.9034" size="1.778" layer="96"/>
</instance>
<instance part="G94" gate="G$1" x="241.3" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="156.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="154.2034" size="1.778" layer="96"/>
</instance>
<instance part="G95" gate="G$1" x="241.3" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="144.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="141.5034" size="1.778" layer="96"/>
</instance>
<instance part="G96" gate="G$1" x="241.3" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="131.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="128.8034" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="248.92" y="116.84" smashed="yes">
<attribute name="VALUE" x="246.38" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="G97" gate="G$1" x="50.8" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="106.0704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="103.4034" size="1.778" layer="96"/>
</instance>
<instance part="G98" gate="G$1" x="50.8" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="93.3704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="90.7034" size="1.778" layer="96"/>
</instance>
<instance part="G99" gate="G$1" x="50.8" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="80.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="78.0034" size="1.778" layer="96"/>
</instance>
<instance part="G100" gate="G$1" x="50.8" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="67.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="65.3034" size="1.778" layer="96"/>
</instance>
<instance part="G101" gate="G$1" x="50.8" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="55.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="52.6034" size="1.778" layer="96"/>
</instance>
<instance part="G102" gate="G$1" x="50.8" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="42.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="39.9034" size="1.778" layer="96"/>
</instance>
<instance part="G103" gate="G$1" x="50.8" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="29.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="27.2034" size="1.778" layer="96"/>
</instance>
<instance part="G104" gate="G$1" x="50.8" y="10.16" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="17.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="14.5034" size="1.778" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="58.42" y="2.54" smashed="yes">
<attribute name="VALUE" x="55.88" y="0" size="1.778" layer="96"/>
</instance>
<instance part="G105" gate="G$1" x="114.3" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="108.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="105.9434" size="1.778" layer="96"/>
</instance>
<instance part="G106" gate="G$1" x="114.3" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="95.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="93.2434" size="1.778" layer="96"/>
</instance>
<instance part="G107" gate="G$1" x="114.3" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="83.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="80.5434" size="1.778" layer="96"/>
</instance>
<instance part="G108" gate="G$1" x="114.3" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="70.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="67.8434" size="1.778" layer="96"/>
</instance>
<instance part="G109" gate="G$1" x="114.3" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="57.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="55.1434" size="1.778" layer="96"/>
</instance>
<instance part="G110" gate="G$1" x="114.3" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="45.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="42.4434" size="1.778" layer="96"/>
</instance>
<instance part="G111" gate="G$1" x="114.3" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="32.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="29.7434" size="1.778" layer="96"/>
</instance>
<instance part="G112" gate="G$1" x="114.3" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="19.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="17.0434" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="121.92" y="5.08" smashed="yes">
<attribute name="VALUE" x="119.38" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="G113" gate="G$1" x="177.8" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="108.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="105.9434" size="1.778" layer="96"/>
</instance>
<instance part="G114" gate="G$1" x="177.8" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="95.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="93.2434" size="1.778" layer="96"/>
</instance>
<instance part="G115" gate="G$1" x="177.8" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="83.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="80.5434" size="1.778" layer="96"/>
</instance>
<instance part="G116" gate="G$1" x="177.8" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="70.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="67.8434" size="1.778" layer="96"/>
</instance>
<instance part="G117" gate="G$1" x="177.8" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="57.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="55.1434" size="1.778" layer="96"/>
</instance>
<instance part="G118" gate="G$1" x="177.8" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="45.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="42.4434" size="1.778" layer="96"/>
</instance>
<instance part="G119" gate="G$1" x="177.8" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="32.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="29.7434" size="1.778" layer="96"/>
</instance>
<instance part="G120" gate="G$1" x="177.8" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="19.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="17.0434" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="185.42" y="5.08" smashed="yes">
<attribute name="VALUE" x="182.88" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="G121" gate="G$1" x="241.3" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="108.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="105.9434" size="1.778" layer="96"/>
</instance>
<instance part="G122" gate="G$1" x="241.3" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="95.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="93.2434" size="1.778" layer="96"/>
</instance>
<instance part="G123" gate="G$1" x="241.3" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="83.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="80.5434" size="1.778" layer="96"/>
</instance>
<instance part="G124" gate="G$1" x="241.3" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="70.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="67.8434" size="1.778" layer="96"/>
</instance>
<instance part="G125" gate="G$1" x="241.3" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="57.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="55.1434" size="1.778" layer="96"/>
</instance>
<instance part="G126" gate="G$1" x="241.3" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="45.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="42.4434" size="1.778" layer="96"/>
</instance>
<instance part="G127" gate="G$1" x="241.3" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="32.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="29.7434" size="1.778" layer="96"/>
</instance>
<instance part="G128" gate="G$1" x="241.3" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="19.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="17.0434" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="248.92" y="5.08" smashed="yes">
<attribute name="VALUE" x="246.38" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="SV9" gate="1" x="15.24" y="167.64" smashed="yes" rot="MR180">
<attribute name="VALUE" x="11.43" y="182.88" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="11.43" y="156.718" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV10" gate="1" x="78.74" y="167.64" smashed="yes" rot="MR180">
<attribute name="VALUE" x="74.93" y="182.88" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="74.93" y="156.718" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV11" gate="1" x="142.24" y="167.64" smashed="yes" rot="MR180">
<attribute name="VALUE" x="138.43" y="182.88" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="138.43" y="156.718" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV12" gate="1" x="205.74" y="167.64" smashed="yes" rot="MR180">
<attribute name="VALUE" x="201.93" y="182.88" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="201.93" y="156.718" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV13" gate="1" x="15.24" y="53.34" smashed="yes" rot="MR180">
<attribute name="VALUE" x="11.43" y="68.58" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="11.43" y="42.418" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV14" gate="1" x="78.74" y="55.88" smashed="yes" rot="MR180">
<attribute name="VALUE" x="74.93" y="71.12" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="74.93" y="44.958" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV15" gate="1" x="142.24" y="55.88" smashed="yes" rot="MR180">
<attribute name="VALUE" x="138.43" y="71.12" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="138.43" y="44.958" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV16" gate="1" x="205.74" y="55.88" smashed="yes" rot="MR180">
<attribute name="VALUE" x="201.93" y="71.12" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="201.93" y="44.958" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND26" gate="1" x="193.04" y="154.94" smashed="yes">
<attribute name="VALUE" x="190.5" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="129.54" y="154.94" smashed="yes">
<attribute name="VALUE" x="127" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="129.54" y="43.18" smashed="yes">
<attribute name="VALUE" x="127" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="GND29" gate="1" x="66.04" y="43.18" smashed="yes">
<attribute name="VALUE" x="63.5" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="GND30" gate="1" x="66.04" y="154.94" smashed="yes">
<attribute name="VALUE" x="63.5" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="2.54" y="154.94" smashed="yes">
<attribute name="VALUE" x="0" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="2.54" y="40.64" smashed="yes">
<attribute name="VALUE" x="0" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="GND49" gate="1" x="452.12" y="45.72" smashed="yes">
<attribute name="VALUE" x="449.58" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="G193" gate="G$1" x="309.88" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="222.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="220.2434" size="1.778" layer="96"/>
</instance>
<instance part="G194" gate="G$1" x="309.88" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="210.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="207.5434" size="1.778" layer="96"/>
</instance>
<instance part="G195" gate="G$1" x="309.88" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="197.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="194.8434" size="1.778" layer="96"/>
</instance>
<instance part="G196" gate="G$1" x="309.88" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="184.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="182.1434" size="1.778" layer="96"/>
</instance>
<instance part="G197" gate="G$1" x="309.88" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="172.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="169.4434" size="1.778" layer="96"/>
</instance>
<instance part="G198" gate="G$1" x="309.88" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="159.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="156.7434" size="1.778" layer="96"/>
</instance>
<instance part="G199" gate="G$1" x="309.88" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="146.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="144.0434" size="1.778" layer="96"/>
</instance>
<instance part="G200" gate="G$1" x="309.88" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="134.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="131.3434" size="1.778" layer="96"/>
</instance>
<instance part="GND50" gate="1" x="317.5" y="119.38" smashed="yes">
<attribute name="VALUE" x="314.96" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="G201" gate="G$1" x="373.38" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="222.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="220.2434" size="1.778" layer="96"/>
</instance>
<instance part="G202" gate="G$1" x="373.38" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="210.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="207.5434" size="1.778" layer="96"/>
</instance>
<instance part="G203" gate="G$1" x="373.38" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="197.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="194.8434" size="1.778" layer="96"/>
</instance>
<instance part="G204" gate="G$1" x="373.38" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="184.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="182.1434" size="1.778" layer="96"/>
</instance>
<instance part="G205" gate="G$1" x="373.38" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="172.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="169.4434" size="1.778" layer="96"/>
</instance>
<instance part="G206" gate="G$1" x="373.38" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="159.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="156.7434" size="1.778" layer="96"/>
</instance>
<instance part="G207" gate="G$1" x="373.38" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="146.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="144.0434" size="1.778" layer="96"/>
</instance>
<instance part="G208" gate="G$1" x="373.38" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="134.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="131.3434" size="1.778" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="381" y="119.38" smashed="yes">
<attribute name="VALUE" x="378.46" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="G209" gate="G$1" x="436.88" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="222.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="220.2434" size="1.778" layer="96"/>
</instance>
<instance part="G210" gate="G$1" x="436.88" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="210.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="207.5434" size="1.778" layer="96"/>
</instance>
<instance part="G211" gate="G$1" x="436.88" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="197.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="194.8434" size="1.778" layer="96"/>
</instance>
<instance part="G212" gate="G$1" x="436.88" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="184.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="182.1434" size="1.778" layer="96"/>
</instance>
<instance part="G213" gate="G$1" x="436.88" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="172.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="169.4434" size="1.778" layer="96"/>
</instance>
<instance part="G214" gate="G$1" x="436.88" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="159.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="156.7434" size="1.778" layer="96"/>
</instance>
<instance part="G215" gate="G$1" x="436.88" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="146.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="144.0434" size="1.778" layer="96"/>
</instance>
<instance part="G216" gate="G$1" x="436.88" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="134.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="131.3434" size="1.778" layer="96"/>
</instance>
<instance part="GND52" gate="1" x="444.5" y="119.38" smashed="yes">
<attribute name="VALUE" x="441.96" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="G217" gate="G$1" x="500.38" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="222.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="220.2434" size="1.778" layer="96"/>
</instance>
<instance part="G218" gate="G$1" x="500.38" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="210.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="207.5434" size="1.778" layer="96"/>
</instance>
<instance part="G219" gate="G$1" x="500.38" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="197.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="194.8434" size="1.778" layer="96"/>
</instance>
<instance part="G220" gate="G$1" x="500.38" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="184.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="182.1434" size="1.778" layer="96"/>
</instance>
<instance part="G221" gate="G$1" x="500.38" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="172.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="169.4434" size="1.778" layer="96"/>
</instance>
<instance part="G222" gate="G$1" x="500.38" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="159.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="156.7434" size="1.778" layer="96"/>
</instance>
<instance part="G223" gate="G$1" x="500.38" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="146.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="144.0434" size="1.778" layer="96"/>
</instance>
<instance part="G224" gate="G$1" x="500.38" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="134.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="131.3434" size="1.778" layer="96"/>
</instance>
<instance part="GND53" gate="1" x="508" y="119.38" smashed="yes">
<attribute name="VALUE" x="505.46" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="G225" gate="G$1" x="309.88" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="108.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="105.9434" size="1.778" layer="96"/>
</instance>
<instance part="G226" gate="G$1" x="309.88" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="95.9104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="93.2434" size="1.778" layer="96"/>
</instance>
<instance part="G227" gate="G$1" x="309.88" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="83.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="80.5434" size="1.778" layer="96"/>
</instance>
<instance part="G228" gate="G$1" x="309.88" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="70.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="67.8434" size="1.778" layer="96"/>
</instance>
<instance part="G229" gate="G$1" x="309.88" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="57.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="55.1434" size="1.778" layer="96"/>
</instance>
<instance part="G230" gate="G$1" x="309.88" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="45.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="42.4434" size="1.778" layer="96"/>
</instance>
<instance part="G231" gate="G$1" x="309.88" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="32.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="29.7434" size="1.778" layer="96"/>
</instance>
<instance part="G232" gate="G$1" x="309.88" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="306.1462" y="19.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="305.4096" y="17.0434" size="1.778" layer="96"/>
</instance>
<instance part="GND54" gate="1" x="317.5" y="5.08" smashed="yes">
<attribute name="VALUE" x="314.96" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="G233" gate="G$1" x="373.38" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="111.1504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="108.4834" size="1.778" layer="96"/>
</instance>
<instance part="G234" gate="G$1" x="373.38" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="98.4504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="95.7834" size="1.778" layer="96"/>
</instance>
<instance part="G235" gate="G$1" x="373.38" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="85.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="83.0834" size="1.778" layer="96"/>
</instance>
<instance part="G236" gate="G$1" x="373.38" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="73.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="70.3834" size="1.778" layer="96"/>
</instance>
<instance part="G237" gate="G$1" x="373.38" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="60.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="57.6834" size="1.778" layer="96"/>
</instance>
<instance part="G238" gate="G$1" x="373.38" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="47.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="44.9834" size="1.778" layer="96"/>
</instance>
<instance part="G239" gate="G$1" x="373.38" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="34.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="32.2834" size="1.778" layer="96"/>
</instance>
<instance part="G240" gate="G$1" x="373.38" y="15.24" smashed="yes" rot="R90">
<attribute name="NAME" x="369.6462" y="22.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.9096" y="19.5834" size="1.778" layer="96"/>
</instance>
<instance part="GND55" gate="1" x="381" y="7.62" smashed="yes">
<attribute name="VALUE" x="378.46" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="G241" gate="G$1" x="436.88" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="111.1504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="108.4834" size="1.778" layer="96"/>
</instance>
<instance part="G242" gate="G$1" x="436.88" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="98.4504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="95.7834" size="1.778" layer="96"/>
</instance>
<instance part="G243" gate="G$1" x="436.88" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="85.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="83.0834" size="1.778" layer="96"/>
</instance>
<instance part="G244" gate="G$1" x="436.88" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="73.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="70.3834" size="1.778" layer="96"/>
</instance>
<instance part="G245" gate="G$1" x="436.88" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="60.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="57.6834" size="1.778" layer="96"/>
</instance>
<instance part="G246" gate="G$1" x="436.88" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="47.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="44.9834" size="1.778" layer="96"/>
</instance>
<instance part="G247" gate="G$1" x="436.88" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="34.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="32.2834" size="1.778" layer="96"/>
</instance>
<instance part="G248" gate="G$1" x="436.88" y="15.24" smashed="yes" rot="R90">
<attribute name="NAME" x="433.1462" y="22.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="432.4096" y="19.5834" size="1.778" layer="96"/>
</instance>
<instance part="GND56" gate="1" x="444.5" y="7.62" smashed="yes">
<attribute name="VALUE" x="441.96" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="G249" gate="G$1" x="500.38" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="111.1504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="108.4834" size="1.778" layer="96"/>
</instance>
<instance part="G250" gate="G$1" x="500.38" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="98.4504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="95.7834" size="1.778" layer="96"/>
</instance>
<instance part="G251" gate="G$1" x="500.38" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="85.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="83.0834" size="1.778" layer="96"/>
</instance>
<instance part="G252" gate="G$1" x="500.38" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="73.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="70.3834" size="1.778" layer="96"/>
</instance>
<instance part="G253" gate="G$1" x="500.38" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="60.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="57.6834" size="1.778" layer="96"/>
</instance>
<instance part="G254" gate="G$1" x="500.38" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="47.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="44.9834" size="1.778" layer="96"/>
</instance>
<instance part="G255" gate="G$1" x="500.38" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="34.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="32.2834" size="1.778" layer="96"/>
</instance>
<instance part="G256" gate="G$1" x="500.38" y="15.24" smashed="yes" rot="R90">
<attribute name="NAME" x="496.6462" y="22.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="495.9096" y="19.5834" size="1.778" layer="96"/>
</instance>
<instance part="GND57" gate="1" x="508" y="7.62" smashed="yes">
<attribute name="VALUE" x="505.46" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="SV25" gate="1" x="274.32" y="170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="270.51" y="185.42" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="270.51" y="159.258" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV26" gate="1" x="337.82" y="170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="334.01" y="185.42" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="334.01" y="159.258" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV27" gate="1" x="401.32" y="170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="397.51" y="185.42" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="397.51" y="159.258" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV28" gate="1" x="464.82" y="170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="461.01" y="185.42" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="461.01" y="159.258" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV29" gate="1" x="274.32" y="55.88" smashed="yes" rot="MR180">
<attribute name="VALUE" x="270.51" y="71.12" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="270.51" y="44.958" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV30" gate="1" x="337.82" y="58.42" smashed="yes" rot="MR180">
<attribute name="VALUE" x="334.01" y="73.66" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="334.01" y="47.498" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV31" gate="1" x="401.32" y="58.42" smashed="yes" rot="MR180">
<attribute name="VALUE" x="397.51" y="73.66" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="397.51" y="47.498" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV32" gate="1" x="464.82" y="58.42" smashed="yes" rot="MR180">
<attribute name="VALUE" x="461.01" y="73.66" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="461.01" y="47.498" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND58" gate="1" x="452.12" y="157.48" smashed="yes">
<attribute name="VALUE" x="449.58" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="GND59" gate="1" x="388.62" y="157.48" smashed="yes">
<attribute name="VALUE" x="386.08" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="GND60" gate="1" x="388.62" y="45.72" smashed="yes">
<attribute name="VALUE" x="386.08" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="GND61" gate="1" x="325.12" y="45.72" smashed="yes">
<attribute name="VALUE" x="322.58" y="43.18" size="1.778" layer="96"/>
</instance>
<instance part="GND62" gate="1" x="325.12" y="157.48" smashed="yes">
<attribute name="VALUE" x="322.58" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="GND63" gate="1" x="261.62" y="157.48" smashed="yes">
<attribute name="VALUE" x="259.08" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="261.62" y="43.18" smashed="yes">
<attribute name="VALUE" x="259.08" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="GND33" gate="1" x="193.04" y="271.78" smashed="yes">
<attribute name="VALUE" x="190.5" y="269.24" size="1.778" layer="96"/>
</instance>
<instance part="G129" gate="G$1" x="50.8" y="441.96" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="448.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="446.3034" size="1.778" layer="96"/>
</instance>
<instance part="G130" gate="G$1" x="50.8" y="429.26" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="436.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="433.6034" size="1.778" layer="96"/>
</instance>
<instance part="G131" gate="G$1" x="50.8" y="416.56" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="423.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="420.9034" size="1.778" layer="96"/>
</instance>
<instance part="G132" gate="G$1" x="50.8" y="403.86" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="410.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="408.2034" size="1.778" layer="96"/>
</instance>
<instance part="G133" gate="G$1" x="50.8" y="391.16" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="398.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="395.5034" size="1.778" layer="96"/>
</instance>
<instance part="G134" gate="G$1" x="50.8" y="378.46" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="385.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="382.8034" size="1.778" layer="96"/>
</instance>
<instance part="G135" gate="G$1" x="50.8" y="365.76" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="372.7704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="370.1034" size="1.778" layer="96"/>
</instance>
<instance part="G136" gate="G$1" x="50.8" y="353.06" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="360.0704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="357.4034" size="1.778" layer="96"/>
</instance>
<instance part="GND34" gate="1" x="58.42" y="345.44" smashed="yes">
<attribute name="VALUE" x="55.88" y="342.9" size="1.778" layer="96"/>
</instance>
<instance part="G137" gate="G$1" x="114.3" y="441.96" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="448.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="446.3034" size="1.778" layer="96"/>
</instance>
<instance part="G138" gate="G$1" x="114.3" y="429.26" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="436.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="433.6034" size="1.778" layer="96"/>
</instance>
<instance part="G139" gate="G$1" x="114.3" y="416.56" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="423.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="420.9034" size="1.778" layer="96"/>
</instance>
<instance part="G140" gate="G$1" x="114.3" y="403.86" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="410.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="408.2034" size="1.778" layer="96"/>
</instance>
<instance part="G141" gate="G$1" x="114.3" y="391.16" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="398.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="395.5034" size="1.778" layer="96"/>
</instance>
<instance part="G142" gate="G$1" x="114.3" y="378.46" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="385.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="382.8034" size="1.778" layer="96"/>
</instance>
<instance part="G143" gate="G$1" x="114.3" y="365.76" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="372.7704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="370.1034" size="1.778" layer="96"/>
</instance>
<instance part="G144" gate="G$1" x="114.3" y="353.06" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="360.0704" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="357.4034" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="121.92" y="345.44" smashed="yes">
<attribute name="VALUE" x="119.38" y="342.9" size="1.778" layer="96"/>
</instance>
<instance part="G145" gate="G$1" x="177.8" y="441.96" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="448.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="446.3034" size="1.778" layer="96"/>
</instance>
<instance part="G146" gate="G$1" x="177.8" y="429.26" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="436.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="433.6034" size="1.778" layer="96"/>
</instance>
<instance part="G147" gate="G$1" x="177.8" y="416.56" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="423.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="420.9034" size="1.778" layer="96"/>
</instance>
<instance part="G148" gate="G$1" x="177.8" y="403.86" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="410.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="408.2034" size="1.778" layer="96"/>
</instance>
<instance part="G149" gate="G$1" x="177.8" y="391.16" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="398.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="395.5034" size="1.778" layer="96"/>
</instance>
<instance part="G150" gate="G$1" x="177.8" y="378.46" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="385.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="382.8034" size="1.778" layer="96"/>
</instance>
<instance part="G151" gate="G$1" x="177.8" y="365.76" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="372.7704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="370.1034" size="1.778" layer="96"/>
</instance>
<instance part="G152" gate="G$1" x="177.8" y="353.06" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="360.0704" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="357.4034" size="1.778" layer="96"/>
</instance>
<instance part="GND36" gate="1" x="185.42" y="345.44" smashed="yes">
<attribute name="VALUE" x="182.88" y="342.9" size="1.778" layer="96"/>
</instance>
<instance part="G153" gate="G$1" x="241.3" y="441.96" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="448.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="446.3034" size="1.778" layer="96"/>
</instance>
<instance part="G154" gate="G$1" x="241.3" y="429.26" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="436.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="433.6034" size="1.778" layer="96"/>
</instance>
<instance part="G155" gate="G$1" x="241.3" y="416.56" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="423.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="420.9034" size="1.778" layer="96"/>
</instance>
<instance part="G156" gate="G$1" x="241.3" y="403.86" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="410.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="408.2034" size="1.778" layer="96"/>
</instance>
<instance part="G157" gate="G$1" x="241.3" y="391.16" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="398.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="395.5034" size="1.778" layer="96"/>
</instance>
<instance part="G158" gate="G$1" x="241.3" y="378.46" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="385.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="382.8034" size="1.778" layer="96"/>
</instance>
<instance part="G159" gate="G$1" x="241.3" y="365.76" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="372.7704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="370.1034" size="1.778" layer="96"/>
</instance>
<instance part="G160" gate="G$1" x="241.3" y="353.06" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="360.0704" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="357.4034" size="1.778" layer="96"/>
</instance>
<instance part="GND37" gate="1" x="248.92" y="345.44" smashed="yes">
<attribute name="VALUE" x="246.38" y="342.9" size="1.778" layer="96"/>
</instance>
<instance part="G161" gate="G$1" x="50.8" y="327.66" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="334.6704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="332.0034" size="1.778" layer="96"/>
</instance>
<instance part="G162" gate="G$1" x="50.8" y="314.96" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="321.9704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="319.3034" size="1.778" layer="96"/>
</instance>
<instance part="G163" gate="G$1" x="50.8" y="302.26" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="309.2704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="306.6034" size="1.778" layer="96"/>
</instance>
<instance part="G164" gate="G$1" x="50.8" y="289.56" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="296.5704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="293.9034" size="1.778" layer="96"/>
</instance>
<instance part="G165" gate="G$1" x="50.8" y="276.86" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="283.8704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="281.2034" size="1.778" layer="96"/>
</instance>
<instance part="G166" gate="G$1" x="50.8" y="264.16" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="271.1704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="268.5034" size="1.778" layer="96"/>
</instance>
<instance part="G167" gate="G$1" x="50.8" y="251.46" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="258.4704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="255.8034" size="1.778" layer="96"/>
</instance>
<instance part="G168" gate="G$1" x="50.8" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="47.0662" y="245.7704" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.3296" y="243.1034" size="1.778" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="58.42" y="231.14" smashed="yes">
<attribute name="VALUE" x="55.88" y="228.6" size="1.778" layer="96"/>
</instance>
<instance part="G169" gate="G$1" x="114.3" y="330.2" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="337.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="334.5434" size="1.778" layer="96"/>
</instance>
<instance part="G170" gate="G$1" x="114.3" y="317.5" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="324.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="321.8434" size="1.778" layer="96"/>
</instance>
<instance part="G171" gate="G$1" x="114.3" y="304.8" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="311.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="309.1434" size="1.778" layer="96"/>
</instance>
<instance part="G172" gate="G$1" x="114.3" y="292.1" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="299.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="296.4434" size="1.778" layer="96"/>
</instance>
<instance part="G173" gate="G$1" x="114.3" y="279.4" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="286.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="283.7434" size="1.778" layer="96"/>
</instance>
<instance part="G174" gate="G$1" x="114.3" y="266.7" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="273.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="271.0434" size="1.778" layer="96"/>
</instance>
<instance part="G175" gate="G$1" x="114.3" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="261.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="258.3434" size="1.778" layer="96"/>
</instance>
<instance part="G176" gate="G$1" x="114.3" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="110.5662" y="248.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.8296" y="245.6434" size="1.778" layer="96"/>
</instance>
<instance part="GND39" gate="1" x="121.92" y="233.68" smashed="yes">
<attribute name="VALUE" x="119.38" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="G177" gate="G$1" x="177.8" y="330.2" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="337.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="334.5434" size="1.778" layer="96"/>
</instance>
<instance part="G178" gate="G$1" x="177.8" y="317.5" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="324.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="321.8434" size="1.778" layer="96"/>
</instance>
<instance part="G179" gate="G$1" x="177.8" y="304.8" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="311.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="309.1434" size="1.778" layer="96"/>
</instance>
<instance part="G180" gate="G$1" x="177.8" y="292.1" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="299.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="296.4434" size="1.778" layer="96"/>
</instance>
<instance part="G181" gate="G$1" x="177.8" y="279.4" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="286.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="283.7434" size="1.778" layer="96"/>
</instance>
<instance part="G182" gate="G$1" x="177.8" y="266.7" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="273.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="271.0434" size="1.778" layer="96"/>
</instance>
<instance part="G183" gate="G$1" x="177.8" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="261.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="258.3434" size="1.778" layer="96"/>
</instance>
<instance part="G184" gate="G$1" x="177.8" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="174.0662" y="248.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.3296" y="245.6434" size="1.778" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="185.42" y="233.68" smashed="yes">
<attribute name="VALUE" x="182.88" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="G185" gate="G$1" x="241.3" y="330.2" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="337.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="334.5434" size="1.778" layer="96"/>
</instance>
<instance part="G186" gate="G$1" x="241.3" y="317.5" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="324.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="321.8434" size="1.778" layer="96"/>
</instance>
<instance part="G187" gate="G$1" x="241.3" y="304.8" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="311.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="309.1434" size="1.778" layer="96"/>
</instance>
<instance part="G188" gate="G$1" x="241.3" y="292.1" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="299.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="296.4434" size="1.778" layer="96"/>
</instance>
<instance part="G189" gate="G$1" x="241.3" y="279.4" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="286.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="283.7434" size="1.778" layer="96"/>
</instance>
<instance part="G190" gate="G$1" x="241.3" y="266.7" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="273.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="271.0434" size="1.778" layer="96"/>
</instance>
<instance part="G191" gate="G$1" x="241.3" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="261.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="258.3434" size="1.778" layer="96"/>
</instance>
<instance part="G192" gate="G$1" x="241.3" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="237.5662" y="248.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="236.8296" y="245.6434" size="1.778" layer="96"/>
</instance>
<instance part="GND41" gate="1" x="248.92" y="233.68" smashed="yes">
<attribute name="VALUE" x="246.38" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="SV17" gate="1" x="15.24" y="396.24" smashed="yes" rot="MR180">
<attribute name="VALUE" x="11.43" y="411.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="11.43" y="385.318" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV18" gate="1" x="78.74" y="396.24" smashed="yes" rot="MR180">
<attribute name="VALUE" x="74.93" y="411.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="74.93" y="385.318" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV19" gate="1" x="142.24" y="396.24" smashed="yes" rot="MR180">
<attribute name="VALUE" x="138.43" y="411.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="138.43" y="385.318" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV20" gate="1" x="205.74" y="396.24" smashed="yes" rot="MR180">
<attribute name="VALUE" x="201.93" y="411.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="201.93" y="385.318" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV21" gate="1" x="15.24" y="281.94" smashed="yes" rot="MR180">
<attribute name="VALUE" x="11.43" y="297.18" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="11.43" y="271.018" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV22" gate="1" x="78.74" y="284.48" smashed="yes" rot="MR180">
<attribute name="VALUE" x="74.93" y="299.72" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="74.93" y="273.558" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV23" gate="1" x="142.24" y="284.48" smashed="yes" rot="MR180">
<attribute name="VALUE" x="138.43" y="299.72" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="138.43" y="273.558" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV24" gate="1" x="205.74" y="284.48" smashed="yes" rot="MR180">
<attribute name="VALUE" x="201.93" y="299.72" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="201.93" y="273.558" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND42" gate="1" x="193.04" y="383.54" smashed="yes">
<attribute name="VALUE" x="190.5" y="381" size="1.778" layer="96"/>
</instance>
<instance part="GND43" gate="1" x="129.54" y="383.54" smashed="yes">
<attribute name="VALUE" x="127" y="381" size="1.778" layer="96"/>
</instance>
<instance part="GND44" gate="1" x="129.54" y="271.78" smashed="yes">
<attribute name="VALUE" x="127" y="269.24" size="1.778" layer="96"/>
</instance>
<instance part="GND45" gate="1" x="66.04" y="271.78" smashed="yes">
<attribute name="VALUE" x="63.5" y="269.24" size="1.778" layer="96"/>
</instance>
<instance part="GND46" gate="1" x="66.04" y="383.54" smashed="yes">
<attribute name="VALUE" x="63.5" y="381" size="1.778" layer="96"/>
</instance>
<instance part="GND47" gate="1" x="2.54" y="383.54" smashed="yes">
<attribute name="VALUE" x="0" y="381" size="1.778" layer="96"/>
</instance>
<instance part="GND48" gate="1" x="2.54" y="269.24" smashed="yes">
<attribute name="VALUE" x="0" y="266.7" size="1.778" layer="96"/>
</instance>
<instance part="GND65" gate="1" x="449.58" y="274.32" smashed="yes">
<attribute name="VALUE" x="447.04" y="271.78" size="1.778" layer="96"/>
</instance>
<instance part="G257" gate="G$1" x="307.34" y="444.5" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="451.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="448.8434" size="1.778" layer="96"/>
</instance>
<instance part="G258" gate="G$1" x="307.34" y="431.8" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="438.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="436.1434" size="1.778" layer="96"/>
</instance>
<instance part="G259" gate="G$1" x="307.34" y="419.1" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="426.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="423.4434" size="1.778" layer="96"/>
</instance>
<instance part="G260" gate="G$1" x="307.34" y="406.4" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="413.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="410.7434" size="1.778" layer="96"/>
</instance>
<instance part="G261" gate="G$1" x="307.34" y="393.7" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="400.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="398.0434" size="1.778" layer="96"/>
</instance>
<instance part="G262" gate="G$1" x="307.34" y="381" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="388.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="385.3434" size="1.778" layer="96"/>
</instance>
<instance part="G263" gate="G$1" x="307.34" y="368.3" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="375.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="372.6434" size="1.778" layer="96"/>
</instance>
<instance part="G264" gate="G$1" x="307.34" y="355.6" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="362.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="359.9434" size="1.778" layer="96"/>
</instance>
<instance part="GND66" gate="1" x="314.96" y="347.98" smashed="yes">
<attribute name="VALUE" x="312.42" y="345.44" size="1.778" layer="96"/>
</instance>
<instance part="G265" gate="G$1" x="370.84" y="444.5" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="451.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="448.8434" size="1.778" layer="96"/>
</instance>
<instance part="G266" gate="G$1" x="370.84" y="431.8" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="438.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="436.1434" size="1.778" layer="96"/>
</instance>
<instance part="G267" gate="G$1" x="370.84" y="419.1" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="426.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="423.4434" size="1.778" layer="96"/>
</instance>
<instance part="G268" gate="G$1" x="370.84" y="406.4" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="413.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="410.7434" size="1.778" layer="96"/>
</instance>
<instance part="G269" gate="G$1" x="370.84" y="393.7" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="400.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="398.0434" size="1.778" layer="96"/>
</instance>
<instance part="G270" gate="G$1" x="370.84" y="381" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="388.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="385.3434" size="1.778" layer="96"/>
</instance>
<instance part="G271" gate="G$1" x="370.84" y="368.3" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="375.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="372.6434" size="1.778" layer="96"/>
</instance>
<instance part="G272" gate="G$1" x="370.84" y="355.6" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="362.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="359.9434" size="1.778" layer="96"/>
</instance>
<instance part="GND67" gate="1" x="378.46" y="347.98" smashed="yes">
<attribute name="VALUE" x="375.92" y="345.44" size="1.778" layer="96"/>
</instance>
<instance part="G273" gate="G$1" x="434.34" y="444.5" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="451.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="448.8434" size="1.778" layer="96"/>
</instance>
<instance part="G274" gate="G$1" x="434.34" y="431.8" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="438.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="436.1434" size="1.778" layer="96"/>
</instance>
<instance part="G275" gate="G$1" x="434.34" y="419.1" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="426.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="423.4434" size="1.778" layer="96"/>
</instance>
<instance part="G276" gate="G$1" x="434.34" y="406.4" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="413.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="410.7434" size="1.778" layer="96"/>
</instance>
<instance part="G277" gate="G$1" x="434.34" y="393.7" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="400.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="398.0434" size="1.778" layer="96"/>
</instance>
<instance part="G278" gate="G$1" x="434.34" y="381" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="388.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="385.3434" size="1.778" layer="96"/>
</instance>
<instance part="G279" gate="G$1" x="434.34" y="368.3" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="375.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="372.6434" size="1.778" layer="96"/>
</instance>
<instance part="G280" gate="G$1" x="434.34" y="355.6" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="362.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="359.9434" size="1.778" layer="96"/>
</instance>
<instance part="GND68" gate="1" x="441.96" y="347.98" smashed="yes">
<attribute name="VALUE" x="439.42" y="345.44" size="1.778" layer="96"/>
</instance>
<instance part="G281" gate="G$1" x="497.84" y="444.5" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="451.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="448.8434" size="1.778" layer="96"/>
</instance>
<instance part="G282" gate="G$1" x="497.84" y="431.8" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="438.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="436.1434" size="1.778" layer="96"/>
</instance>
<instance part="G283" gate="G$1" x="497.84" y="419.1" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="426.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="423.4434" size="1.778" layer="96"/>
</instance>
<instance part="G284" gate="G$1" x="497.84" y="406.4" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="413.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="410.7434" size="1.778" layer="96"/>
</instance>
<instance part="G285" gate="G$1" x="497.84" y="393.7" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="400.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="398.0434" size="1.778" layer="96"/>
</instance>
<instance part="G286" gate="G$1" x="497.84" y="381" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="388.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="385.3434" size="1.778" layer="96"/>
</instance>
<instance part="G287" gate="G$1" x="497.84" y="368.3" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="375.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="372.6434" size="1.778" layer="96"/>
</instance>
<instance part="G288" gate="G$1" x="497.84" y="355.6" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="362.6104" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="359.9434" size="1.778" layer="96"/>
</instance>
<instance part="GND69" gate="1" x="505.46" y="347.98" smashed="yes">
<attribute name="VALUE" x="502.92" y="345.44" size="1.778" layer="96"/>
</instance>
<instance part="G289" gate="G$1" x="307.34" y="330.2" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="337.2104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="334.5434" size="1.778" layer="96"/>
</instance>
<instance part="G290" gate="G$1" x="307.34" y="317.5" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="324.5104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="321.8434" size="1.778" layer="96"/>
</instance>
<instance part="G291" gate="G$1" x="307.34" y="304.8" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="311.8104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="309.1434" size="1.778" layer="96"/>
</instance>
<instance part="G292" gate="G$1" x="307.34" y="292.1" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="299.1104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="296.4434" size="1.778" layer="96"/>
</instance>
<instance part="G293" gate="G$1" x="307.34" y="279.4" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="286.4104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="283.7434" size="1.778" layer="96"/>
</instance>
<instance part="G294" gate="G$1" x="307.34" y="266.7" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="273.7104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="271.0434" size="1.778" layer="96"/>
</instance>
<instance part="G295" gate="G$1" x="307.34" y="254" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="261.0104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="258.3434" size="1.778" layer="96"/>
</instance>
<instance part="G296" gate="G$1" x="307.34" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="303.6062" y="248.3104" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.8696" y="245.6434" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="314.96" y="233.68" smashed="yes">
<attribute name="VALUE" x="312.42" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="G297" gate="G$1" x="370.84" y="332.74" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="339.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="337.0834" size="1.778" layer="96"/>
</instance>
<instance part="G298" gate="G$1" x="370.84" y="320.04" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="327.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="324.3834" size="1.778" layer="96"/>
</instance>
<instance part="G299" gate="G$1" x="370.84" y="307.34" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="314.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="311.6834" size="1.778" layer="96"/>
</instance>
<instance part="G300" gate="G$1" x="370.84" y="294.64" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="301.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="298.9834" size="1.778" layer="96"/>
</instance>
<instance part="G301" gate="G$1" x="370.84" y="281.94" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="288.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="286.2834" size="1.778" layer="96"/>
</instance>
<instance part="G302" gate="G$1" x="370.84" y="269.24" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="276.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="273.5834" size="1.778" layer="96"/>
</instance>
<instance part="G303" gate="G$1" x="370.84" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="263.5504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="260.8834" size="1.778" layer="96"/>
</instance>
<instance part="G304" gate="G$1" x="370.84" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="367.1062" y="250.8504" size="1.778" layer="95"/>
<attribute name="VALUE" x="366.3696" y="248.1834" size="1.778" layer="96"/>
</instance>
<instance part="GND71" gate="1" x="378.46" y="236.22" smashed="yes">
<attribute name="VALUE" x="375.92" y="233.68" size="1.778" layer="96"/>
</instance>
<instance part="G305" gate="G$1" x="434.34" y="332.74" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="339.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="337.0834" size="1.778" layer="96"/>
</instance>
<instance part="G306" gate="G$1" x="434.34" y="320.04" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="327.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="324.3834" size="1.778" layer="96"/>
</instance>
<instance part="G307" gate="G$1" x="434.34" y="307.34" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="314.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="311.6834" size="1.778" layer="96"/>
</instance>
<instance part="G308" gate="G$1" x="434.34" y="294.64" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="301.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="298.9834" size="1.778" layer="96"/>
</instance>
<instance part="G309" gate="G$1" x="434.34" y="281.94" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="288.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="286.2834" size="1.778" layer="96"/>
</instance>
<instance part="G310" gate="G$1" x="434.34" y="269.24" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="276.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="273.5834" size="1.778" layer="96"/>
</instance>
<instance part="G311" gate="G$1" x="434.34" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="263.5504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="260.8834" size="1.778" layer="96"/>
</instance>
<instance part="G312" gate="G$1" x="434.34" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="430.6062" y="250.8504" size="1.778" layer="95"/>
<attribute name="VALUE" x="429.8696" y="248.1834" size="1.778" layer="96"/>
</instance>
<instance part="GND72" gate="1" x="441.96" y="236.22" smashed="yes">
<attribute name="VALUE" x="439.42" y="233.68" size="1.778" layer="96"/>
</instance>
<instance part="G313" gate="G$1" x="497.84" y="332.74" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="339.7504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="337.0834" size="1.778" layer="96"/>
</instance>
<instance part="G314" gate="G$1" x="497.84" y="320.04" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="327.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="324.3834" size="1.778" layer="96"/>
</instance>
<instance part="G315" gate="G$1" x="497.84" y="307.34" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="314.3504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="311.6834" size="1.778" layer="96"/>
</instance>
<instance part="G316" gate="G$1" x="497.84" y="294.64" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="301.6504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="298.9834" size="1.778" layer="96"/>
</instance>
<instance part="G317" gate="G$1" x="497.84" y="281.94" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="288.9504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="286.2834" size="1.778" layer="96"/>
</instance>
<instance part="G318" gate="G$1" x="497.84" y="269.24" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="276.2504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="273.5834" size="1.778" layer="96"/>
</instance>
<instance part="G319" gate="G$1" x="497.84" y="256.54" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="263.5504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="260.8834" size="1.778" layer="96"/>
</instance>
<instance part="G320" gate="G$1" x="497.84" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="494.1062" y="250.8504" size="1.778" layer="95"/>
<attribute name="VALUE" x="493.3696" y="248.1834" size="1.778" layer="96"/>
</instance>
<instance part="GND73" gate="1" x="505.46" y="236.22" smashed="yes">
<attribute name="VALUE" x="502.92" y="233.68" size="1.778" layer="96"/>
</instance>
<instance part="SV33" gate="1" x="271.78" y="398.78" smashed="yes" rot="MR180">
<attribute name="VALUE" x="267.97" y="414.02" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="267.97" y="387.858" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV34" gate="1" x="335.28" y="398.78" smashed="yes" rot="MR180">
<attribute name="VALUE" x="331.47" y="414.02" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="331.47" y="387.858" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV35" gate="1" x="398.78" y="398.78" smashed="yes" rot="MR180">
<attribute name="VALUE" x="394.97" y="414.02" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="394.97" y="387.858" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV36" gate="1" x="462.28" y="398.78" smashed="yes" rot="MR180">
<attribute name="VALUE" x="458.47" y="414.02" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="458.47" y="387.858" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV37" gate="1" x="271.78" y="284.48" smashed="yes" rot="MR180">
<attribute name="VALUE" x="267.97" y="299.72" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="267.97" y="273.558" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV38" gate="1" x="335.28" y="287.02" smashed="yes" rot="MR180">
<attribute name="VALUE" x="331.47" y="302.26" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="331.47" y="276.098" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV39" gate="1" x="398.78" y="287.02" smashed="yes" rot="MR180">
<attribute name="VALUE" x="394.97" y="302.26" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="394.97" y="276.098" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SV40" gate="1" x="462.28" y="287.02" smashed="yes" rot="MR180">
<attribute name="VALUE" x="458.47" y="302.26" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="458.47" y="276.098" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND74" gate="1" x="449.58" y="386.08" smashed="yes">
<attribute name="VALUE" x="447.04" y="383.54" size="1.778" layer="96"/>
</instance>
<instance part="GND75" gate="1" x="386.08" y="386.08" smashed="yes">
<attribute name="VALUE" x="383.54" y="383.54" size="1.778" layer="96"/>
</instance>
<instance part="GND76" gate="1" x="386.08" y="274.32" smashed="yes">
<attribute name="VALUE" x="383.54" y="271.78" size="1.778" layer="96"/>
</instance>
<instance part="GND77" gate="1" x="322.58" y="274.32" smashed="yes">
<attribute name="VALUE" x="320.04" y="271.78" size="1.778" layer="96"/>
</instance>
<instance part="GND78" gate="1" x="322.58" y="386.08" smashed="yes">
<attribute name="VALUE" x="320.04" y="383.54" size="1.778" layer="96"/>
</instance>
<instance part="GND79" gate="1" x="259.08" y="386.08" smashed="yes">
<attribute name="VALUE" x="256.54" y="383.54" size="1.778" layer="96"/>
</instance>
<instance part="GND80" gate="1" x="259.08" y="271.78" smashed="yes">
<attribute name="VALUE" x="256.54" y="269.24" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$64" class="0">
<segment>
<wire x1="22.86" y1="177.8" x2="27.94" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="1"/>
<wire x1="27.94" y1="177.8" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G65" gate="G$1" pin="1"/>
<wire x1="27.94" y1="213.36" x2="45.72" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="22.86" y1="175.26" x2="30.48" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="3"/>
<wire x1="30.48" y1="175.26" x2="30.48" y2="200.66" width="0.1524" layer="91"/>
<pinref part="G66" gate="G$1" pin="1"/>
<wire x1="30.48" y1="200.66" x2="45.72" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="22.86" y1="172.72" x2="33.02" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="5"/>
<wire x1="33.02" y1="172.72" x2="33.02" y2="187.96" width="0.1524" layer="91"/>
<pinref part="G67" gate="G$1" pin="1"/>
<wire x1="33.02" y1="187.96" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="SV9" gate="1" pin="7"/>
<wire x1="22.86" y1="170.18" x2="35.56" y2="170.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="170.18" x2="35.56" y2="175.26" width="0.1524" layer="91"/>
<pinref part="G68" gate="G$1" pin="1"/>
<wire x1="35.56" y1="175.26" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="SV9" gate="1" pin="9"/>
<wire x1="22.86" y1="167.64" x2="35.56" y2="167.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="167.64" x2="35.56" y2="162.56" width="0.1524" layer="91"/>
<pinref part="G69" gate="G$1" pin="1"/>
<wire x1="35.56" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="22.86" y1="165.1" x2="33.02" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="11"/>
<wire x1="33.02" y1="165.1" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<pinref part="G70" gate="G$1" pin="1"/>
<wire x1="33.02" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="22.86" y1="162.56" x2="30.48" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="13"/>
<wire x1="30.48" y1="162.56" x2="30.48" y2="137.16" width="0.1524" layer="91"/>
<pinref part="G71" gate="G$1" pin="1"/>
<wire x1="30.48" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="22.86" y1="160.02" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="15"/>
<wire x1="27.94" y1="160.02" x2="27.94" y2="124.46" width="0.1524" layer="91"/>
<pinref part="G72" gate="G$1" pin="1"/>
<wire x1="27.94" y1="124.46" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="86.36" y1="177.8" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="1"/>
<wire x1="93.98" y1="177.8" x2="93.98" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G73" gate="G$1" pin="1"/>
<wire x1="93.98" y1="213.36" x2="109.22" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="G74" gate="G$1" pin="1"/>
<wire x1="96.52" y1="200.66" x2="109.22" y2="200.66" width="0.1524" layer="91"/>
<wire x1="96.52" y1="200.66" x2="96.52" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="3"/>
<wire x1="96.52" y1="175.26" x2="86.36" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="86.36" y1="172.72" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="5"/>
<wire x1="99.06" y1="172.72" x2="99.06" y2="187.96" width="0.1524" layer="91"/>
<pinref part="G75" gate="G$1" pin="1"/>
<wire x1="99.06" y1="187.96" x2="109.22" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="SV10" gate="1" pin="7"/>
<wire x1="86.36" y1="170.18" x2="101.6" y2="170.18" width="0.1524" layer="91"/>
<pinref part="G76" gate="G$1" pin="1"/>
<wire x1="101.6" y1="175.26" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="101.6" y1="170.18" x2="101.6" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="SV10" gate="1" pin="9"/>
<pinref part="G77" gate="G$1" pin="1"/>
<wire x1="101.6" y1="162.56" x2="109.22" y2="162.56" width="0.1524" layer="91"/>
<wire x1="86.36" y1="167.64" x2="101.6" y2="167.64" width="0.1524" layer="91"/>
<wire x1="101.6" y1="167.64" x2="101.6" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="86.36" y1="165.1" x2="99.06" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="11"/>
<wire x1="99.06" y1="165.1" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<pinref part="G78" gate="G$1" pin="1"/>
<wire x1="99.06" y1="149.86" x2="109.22" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="86.36" y1="162.56" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="13"/>
<wire x1="96.52" y1="162.56" x2="96.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="G79" gate="G$1" pin="1"/>
<wire x1="96.52" y1="137.16" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="86.36" y1="160.02" x2="93.98" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="15"/>
<wire x1="93.98" y1="160.02" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="G80" gate="G$1" pin="1"/>
<wire x1="93.98" y1="124.46" x2="109.22" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<wire x1="149.86" y1="177.8" x2="157.48" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="1"/>
<wire x1="157.48" y1="177.8" x2="157.48" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G81" gate="G$1" pin="1"/>
<wire x1="157.48" y1="213.36" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="G82" gate="G$1" pin="1"/>
<wire x1="160.02" y1="200.66" x2="172.72" y2="200.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="200.66" x2="160.02" y2="175.26" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="160.02" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="3"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="G83" gate="G$1" pin="1"/>
<wire x1="162.56" y1="187.96" x2="172.72" y2="187.96" width="0.1524" layer="91"/>
<wire x1="162.56" y1="187.96" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<wire x1="149.86" y1="172.72" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="5"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="SV11" gate="1" pin="7"/>
<wire x1="149.86" y1="170.18" x2="165.1" y2="170.18" width="0.1524" layer="91"/>
<pinref part="G84" gate="G$1" pin="1"/>
<wire x1="165.1" y1="175.26" x2="172.72" y2="175.26" width="0.1524" layer="91"/>
<wire x1="165.1" y1="170.18" x2="165.1" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="G85" gate="G$1" pin="1"/>
<wire x1="165.1" y1="162.56" x2="172.72" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="9"/>
<wire x1="165.1" y1="162.56" x2="165.1" y2="167.64" width="0.1524" layer="91"/>
<wire x1="165.1" y1="167.64" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<wire x1="149.86" y1="165.1" x2="162.56" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="11"/>
<wire x1="162.56" y1="165.1" x2="162.56" y2="149.86" width="0.1524" layer="91"/>
<pinref part="G86" gate="G$1" pin="1"/>
<wire x1="162.56" y1="149.86" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<wire x1="149.86" y1="162.56" x2="160.02" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="13"/>
<wire x1="160.02" y1="162.56" x2="160.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="G87" gate="G$1" pin="1"/>
<wire x1="160.02" y1="137.16" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<wire x1="149.86" y1="160.02" x2="157.48" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="15"/>
<wire x1="157.48" y1="160.02" x2="157.48" y2="124.46" width="0.1524" layer="91"/>
<pinref part="G88" gate="G$1" pin="1"/>
<wire x1="157.48" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<wire x1="213.36" y1="177.8" x2="220.98" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="1"/>
<wire x1="220.98" y1="177.8" x2="220.98" y2="213.36" width="0.1524" layer="91"/>
<pinref part="G89" gate="G$1" pin="1"/>
<wire x1="220.98" y1="213.36" x2="236.22" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="G90" gate="G$1" pin="1"/>
<wire x1="223.52" y1="200.66" x2="236.22" y2="200.66" width="0.1524" layer="91"/>
<wire x1="223.52" y1="200.66" x2="223.52" y2="175.26" width="0.1524" layer="91"/>
<wire x1="213.36" y1="175.26" x2="223.52" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="3"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="G91" gate="G$1" pin="1"/>
<wire x1="226.06" y1="187.96" x2="236.22" y2="187.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="187.96" x2="226.06" y2="172.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="172.72" x2="226.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="5"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="SV12" gate="1" pin="7"/>
<wire x1="213.36" y1="170.18" x2="228.6" y2="170.18" width="0.1524" layer="91"/>
<pinref part="G92" gate="G$1" pin="1"/>
<wire x1="228.6" y1="175.26" x2="236.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="228.6" y1="170.18" x2="228.6" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="G93" gate="G$1" pin="1"/>
<wire x1="228.6" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
<wire x1="228.6" y1="162.56" x2="228.6" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="9"/>
<wire x1="228.6" y1="167.64" x2="213.36" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="213.36" y1="165.1" x2="226.06" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="11"/>
<wire x1="226.06" y1="165.1" x2="226.06" y2="149.86" width="0.1524" layer="91"/>
<pinref part="G94" gate="G$1" pin="1"/>
<wire x1="226.06" y1="149.86" x2="236.22" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="213.36" y1="162.56" x2="223.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="13"/>
<wire x1="223.52" y1="162.56" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="G95" gate="G$1" pin="1"/>
<wire x1="223.52" y1="137.16" x2="236.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="213.36" y1="160.02" x2="220.98" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV12" gate="1" pin="15"/>
<wire x1="220.98" y1="160.02" x2="220.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="G96" gate="G$1" pin="1"/>
<wire x1="220.98" y1="124.46" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="22.86" y1="63.5" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="1"/>
<wire x1="30.48" y1="63.5" x2="30.48" y2="99.06" width="0.1524" layer="91"/>
<pinref part="G97" gate="G$1" pin="1"/>
<wire x1="30.48" y1="99.06" x2="45.72" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="G98" gate="G$1" pin="1"/>
<wire x1="33.02" y1="86.36" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<wire x1="22.86" y1="60.96" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="3"/>
<wire x1="33.02" y1="86.36" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="G99" gate="G$1" pin="1"/>
<wire x1="35.56" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="35.56" y1="73.66" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="22.86" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="5"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="G100" gate="G$1" pin="1"/>
<wire x1="38.1" y1="60.96" x2="45.72" y2="60.96" width="0.1524" layer="91"/>
<wire x1="38.1" y1="60.96" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="7"/>
<wire x1="38.1" y1="55.88" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="SV13" gate="1" pin="9"/>
<wire x1="22.86" y1="53.34" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
<pinref part="G101" gate="G$1" pin="1"/>
<wire x1="38.1" y1="48.26" x2="45.72" y2="48.26" width="0.1524" layer="91"/>
<wire x1="38.1" y1="53.34" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<wire x1="22.86" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="11"/>
<wire x1="35.56" y1="50.8" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<pinref part="G102" gate="G$1" pin="1"/>
<wire x1="35.56" y1="35.56" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<wire x1="22.86" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="13"/>
<pinref part="G103" gate="G$1" pin="1"/>
<wire x1="33.02" y1="22.86" x2="45.72" y2="22.86" width="0.1524" layer="91"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<wire x1="22.86" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="15"/>
<pinref part="G104" gate="G$1" pin="1"/>
<wire x1="30.48" y1="45.72" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="10.16" x2="45.72" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<wire x1="86.36" y1="66.04" x2="93.98" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="1"/>
<pinref part="G105" gate="G$1" pin="1"/>
<wire x1="93.98" y1="66.04" x2="93.98" y2="101.6" width="0.1524" layer="91"/>
<wire x1="93.98" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="G106" gate="G$1" pin="1"/>
<wire x1="96.52" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="88.9" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<wire x1="86.36" y1="63.5" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="3"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="G107" gate="G$1" pin="1"/>
<wire x1="99.06" y1="76.2" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="99.06" y1="76.2" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="60.96" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="5"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="SV14" gate="1" pin="7"/>
<pinref part="G108" gate="G$1" pin="1"/>
<wire x1="101.6" y1="63.5" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="86.36" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="G109" gate="G$1" pin="1"/>
<wire x1="101.6" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="9"/>
<wire x1="101.6" y1="50.8" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<wire x1="86.36" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="11"/>
<wire x1="99.06" y1="53.34" x2="99.06" y2="38.1" width="0.1524" layer="91"/>
<pinref part="G110" gate="G$1" pin="1"/>
<wire x1="99.06" y1="38.1" x2="109.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<wire x1="86.36" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="13"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="G111" gate="G$1" pin="1"/>
<wire x1="96.52" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<wire x1="86.36" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="15"/>
<pinref part="G112" gate="G$1" pin="1"/>
<wire x1="93.98" y1="48.26" x2="93.98" y2="12.7" width="0.1524" layer="91"/>
<wire x1="93.98" y1="12.7" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<wire x1="149.86" y1="66.04" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="1"/>
<pinref part="G113" gate="G$1" pin="1"/>
<wire x1="157.48" y1="66.04" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="157.48" y1="101.6" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="G114" gate="G$1" pin="1"/>
<wire x1="160.02" y1="88.9" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="160.02" y1="88.9" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="3"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="G115" gate="G$1" pin="1"/>
<wire x1="162.56" y1="76.2" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<wire x1="162.56" y1="76.2" x2="162.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="60.96" x2="162.56" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="5"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="SV15" gate="1" pin="7"/>
<pinref part="G116" gate="G$1" pin="1"/>
<wire x1="165.1" y1="63.5" x2="172.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="165.1" y1="58.42" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="SV15" gate="1" pin="9"/>
<pinref part="G117" gate="G$1" pin="1"/>
<wire x1="165.1" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="149.86" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<wire x1="149.86" y1="53.34" x2="162.56" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="11"/>
<wire x1="162.56" y1="53.34" x2="162.56" y2="38.1" width="0.1524" layer="91"/>
<pinref part="G118" gate="G$1" pin="1"/>
<wire x1="162.56" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<wire x1="149.86" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="13"/>
<wire x1="160.02" y1="50.8" x2="160.02" y2="25.4" width="0.1524" layer="91"/>
<pinref part="G119" gate="G$1" pin="1"/>
<wire x1="160.02" y1="25.4" x2="172.72" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<wire x1="149.86" y1="48.26" x2="157.48" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="15"/>
<pinref part="G120" gate="G$1" pin="1"/>
<wire x1="157.48" y1="48.26" x2="157.48" y2="12.7" width="0.1524" layer="91"/>
<wire x1="157.48" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<wire x1="213.36" y1="66.04" x2="220.98" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="1"/>
<pinref part="G121" gate="G$1" pin="1"/>
<wire x1="220.98" y1="66.04" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
<wire x1="220.98" y1="101.6" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="G122" gate="G$1" pin="1"/>
<wire x1="223.52" y1="88.9" x2="236.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="223.52" y1="88.9" x2="223.52" y2="63.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="63.5" x2="223.52" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="3"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="G123" gate="G$1" pin="1"/>
<wire x1="226.06" y1="76.2" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="226.06" y1="76.2" x2="226.06" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="5"/>
<wire x1="226.06" y1="60.96" x2="213.36" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="SV16" gate="1" pin="7"/>
<pinref part="G124" gate="G$1" pin="1"/>
<wire x1="228.6" y1="63.5" x2="236.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="58.42" x2="228.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="228.6" y1="58.42" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="G125" gate="G$1" pin="1"/>
<wire x1="228.6" y1="50.8" x2="236.22" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="9"/>
<wire x1="228.6" y1="50.8" x2="228.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="228.6" y1="55.88" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<wire x1="213.36" y1="53.34" x2="226.06" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="11"/>
<wire x1="226.06" y1="53.34" x2="226.06" y2="38.1" width="0.1524" layer="91"/>
<pinref part="G126" gate="G$1" pin="1"/>
<wire x1="226.06" y1="38.1" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<wire x1="213.36" y1="50.8" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="13"/>
<wire x1="223.52" y1="50.8" x2="223.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="G127" gate="G$1" pin="1"/>
<wire x1="223.52" y1="25.4" x2="236.22" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<wire x1="213.36" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV16" gate="1" pin="15"/>
<pinref part="G128" gate="G$1" pin="1"/>
<wire x1="220.98" y1="48.26" x2="220.98" y2="12.7" width="0.1524" layer="91"/>
<wire x1="220.98" y1="12.7" x2="236.22" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="198.12" y1="50.8" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="193.04" y1="50.8" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="198.12" y1="48.26" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="198.12" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="193.04" y1="55.88" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<wire x1="198.12" y1="53.34" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<wire x1="198.12" y1="60.96" x2="193.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="193.04" y1="60.96" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="66.04" x2="193.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="193.04" y1="66.04" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="63.5" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="193.04" y1="60.96" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
<junction x="193.04" y="60.96"/>
<junction x="193.04" y="63.5"/>
<wire x1="193.04" y1="55.88" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="193.04" y="55.88"/>
<junction x="193.04" y="58.42"/>
<wire x1="193.04" y1="50.8" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<junction x="193.04" y="50.8"/>
<junction x="193.04" y="53.34"/>
<wire x1="193.04" y1="48.26" x2="193.04" y2="45.72" width="0.1524" layer="91"/>
<junction x="193.04" y="48.26"/>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="SV16" gate="1" pin="2"/>
<pinref part="SV16" gate="1" pin="4"/>
<pinref part="SV16" gate="1" pin="6"/>
<pinref part="SV16" gate="1" pin="8"/>
<pinref part="SV16" gate="1" pin="10"/>
<pinref part="SV16" gate="1" pin="12"/>
<pinref part="SV16" gate="1" pin="14"/>
<pinref part="SV16" gate="1" pin="16"/>
</segment>
<segment>
<pinref part="G65" gate="G$1" pin="2"/>
<wire x1="55.88" y1="213.36" x2="58.42" y2="213.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="213.36" x2="58.42" y2="200.66" width="0.1524" layer="91"/>
<pinref part="G66" gate="G$1" pin="2"/>
<wire x1="58.42" y1="200.66" x2="55.88" y2="200.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="200.66" x2="58.42" y2="187.96" width="0.1524" layer="91"/>
<junction x="58.42" y="200.66"/>
<pinref part="G67" gate="G$1" pin="2"/>
<wire x1="58.42" y1="187.96" x2="55.88" y2="187.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="187.96" x2="58.42" y2="175.26" width="0.1524" layer="91"/>
<junction x="58.42" y="187.96"/>
<pinref part="G68" gate="G$1" pin="2"/>
<wire x1="58.42" y1="175.26" x2="55.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="175.26" x2="58.42" y2="162.56" width="0.1524" layer="91"/>
<junction x="58.42" y="175.26"/>
<pinref part="G69" gate="G$1" pin="2"/>
<wire x1="58.42" y1="162.56" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="162.56" x2="58.42" y2="149.86" width="0.1524" layer="91"/>
<junction x="58.42" y="162.56"/>
<pinref part="G70" gate="G$1" pin="2"/>
<wire x1="58.42" y1="149.86" x2="55.88" y2="149.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="149.86" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="58.42" y="149.86"/>
<pinref part="G71" gate="G$1" pin="2"/>
<wire x1="58.42" y1="137.16" x2="55.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="137.16" x2="58.42" y2="124.46" width="0.1524" layer="91"/>
<junction x="58.42" y="137.16"/>
<pinref part="G72" gate="G$1" pin="2"/>
<wire x1="58.42" y1="124.46" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="124.46" x2="58.42" y2="119.38" width="0.1524" layer="91"/>
<junction x="58.42" y="124.46"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G73" gate="G$1" pin="2"/>
<wire x1="119.38" y1="213.36" x2="121.92" y2="213.36" width="0.1524" layer="91"/>
<wire x1="121.92" y1="213.36" x2="121.92" y2="200.66" width="0.1524" layer="91"/>
<pinref part="G74" gate="G$1" pin="2"/>
<wire x1="121.92" y1="200.66" x2="119.38" y2="200.66" width="0.1524" layer="91"/>
<wire x1="121.92" y1="200.66" x2="121.92" y2="187.96" width="0.1524" layer="91"/>
<junction x="121.92" y="200.66"/>
<pinref part="G75" gate="G$1" pin="2"/>
<wire x1="121.92" y1="187.96" x2="119.38" y2="187.96" width="0.1524" layer="91"/>
<wire x1="121.92" y1="187.96" x2="121.92" y2="175.26" width="0.1524" layer="91"/>
<junction x="121.92" y="187.96"/>
<pinref part="G76" gate="G$1" pin="2"/>
<wire x1="121.92" y1="175.26" x2="119.38" y2="175.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="175.26" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<junction x="121.92" y="175.26"/>
<pinref part="G77" gate="G$1" pin="2"/>
<wire x1="121.92" y1="162.56" x2="119.38" y2="162.56" width="0.1524" layer="91"/>
<wire x1="121.92" y1="162.56" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
<junction x="121.92" y="162.56"/>
<pinref part="G78" gate="G$1" pin="2"/>
<wire x1="121.92" y1="149.86" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="149.86" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<junction x="121.92" y="149.86"/>
<pinref part="G79" gate="G$1" pin="2"/>
<wire x1="121.92" y1="137.16" x2="119.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="137.16" x2="121.92" y2="124.46" width="0.1524" layer="91"/>
<junction x="121.92" y="137.16"/>
<pinref part="G80" gate="G$1" pin="2"/>
<wire x1="121.92" y1="124.46" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<wire x1="121.92" y1="124.46" x2="121.92" y2="119.38" width="0.1524" layer="91"/>
<junction x="121.92" y="124.46"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G81" gate="G$1" pin="2"/>
<wire x1="182.88" y1="213.36" x2="185.42" y2="213.36" width="0.1524" layer="91"/>
<wire x1="185.42" y1="213.36" x2="185.42" y2="200.66" width="0.1524" layer="91"/>
<pinref part="G82" gate="G$1" pin="2"/>
<wire x1="185.42" y1="200.66" x2="182.88" y2="200.66" width="0.1524" layer="91"/>
<wire x1="185.42" y1="200.66" x2="185.42" y2="187.96" width="0.1524" layer="91"/>
<junction x="185.42" y="200.66"/>
<pinref part="G83" gate="G$1" pin="2"/>
<wire x1="185.42" y1="187.96" x2="182.88" y2="187.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="187.96" x2="185.42" y2="175.26" width="0.1524" layer="91"/>
<junction x="185.42" y="187.96"/>
<pinref part="G84" gate="G$1" pin="2"/>
<wire x1="185.42" y1="175.26" x2="182.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="185.42" y1="175.26" x2="185.42" y2="162.56" width="0.1524" layer="91"/>
<junction x="185.42" y="175.26"/>
<pinref part="G85" gate="G$1" pin="2"/>
<wire x1="185.42" y1="162.56" x2="182.88" y2="162.56" width="0.1524" layer="91"/>
<wire x1="185.42" y1="162.56" x2="185.42" y2="149.86" width="0.1524" layer="91"/>
<junction x="185.42" y="162.56"/>
<pinref part="G86" gate="G$1" pin="2"/>
<wire x1="185.42" y1="149.86" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
<wire x1="185.42" y1="149.86" x2="185.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="185.42" y="149.86"/>
<pinref part="G87" gate="G$1" pin="2"/>
<wire x1="185.42" y1="137.16" x2="182.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="185.42" y1="137.16" x2="185.42" y2="124.46" width="0.1524" layer="91"/>
<junction x="185.42" y="137.16"/>
<pinref part="G88" gate="G$1" pin="2"/>
<wire x1="185.42" y1="124.46" x2="182.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="124.46" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<junction x="185.42" y="124.46"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G89" gate="G$1" pin="2"/>
<wire x1="246.38" y1="213.36" x2="248.92" y2="213.36" width="0.1524" layer="91"/>
<wire x1="248.92" y1="213.36" x2="248.92" y2="200.66" width="0.1524" layer="91"/>
<pinref part="G90" gate="G$1" pin="2"/>
<wire x1="248.92" y1="200.66" x2="246.38" y2="200.66" width="0.1524" layer="91"/>
<wire x1="248.92" y1="200.66" x2="248.92" y2="187.96" width="0.1524" layer="91"/>
<junction x="248.92" y="200.66"/>
<pinref part="G91" gate="G$1" pin="2"/>
<wire x1="248.92" y1="187.96" x2="246.38" y2="187.96" width="0.1524" layer="91"/>
<wire x1="248.92" y1="187.96" x2="248.92" y2="175.26" width="0.1524" layer="91"/>
<junction x="248.92" y="187.96"/>
<pinref part="G92" gate="G$1" pin="2"/>
<wire x1="248.92" y1="175.26" x2="246.38" y2="175.26" width="0.1524" layer="91"/>
<wire x1="248.92" y1="175.26" x2="248.92" y2="162.56" width="0.1524" layer="91"/>
<junction x="248.92" y="175.26"/>
<pinref part="G93" gate="G$1" pin="2"/>
<wire x1="248.92" y1="162.56" x2="246.38" y2="162.56" width="0.1524" layer="91"/>
<wire x1="248.92" y1="162.56" x2="248.92" y2="149.86" width="0.1524" layer="91"/>
<junction x="248.92" y="162.56"/>
<pinref part="G94" gate="G$1" pin="2"/>
<wire x1="248.92" y1="149.86" x2="246.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="248.92" y1="149.86" x2="248.92" y2="137.16" width="0.1524" layer="91"/>
<junction x="248.92" y="149.86"/>
<pinref part="G95" gate="G$1" pin="2"/>
<wire x1="248.92" y1="137.16" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="248.92" y1="137.16" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<junction x="248.92" y="137.16"/>
<pinref part="G96" gate="G$1" pin="2"/>
<wire x1="248.92" y1="124.46" x2="246.38" y2="124.46" width="0.1524" layer="91"/>
<wire x1="248.92" y1="124.46" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<junction x="248.92" y="124.46"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G97" gate="G$1" pin="2"/>
<wire x1="55.88" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<pinref part="G98" gate="G$1" pin="2"/>
<wire x1="58.42" y1="86.36" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<junction x="58.42" y="86.36"/>
<pinref part="G99" gate="G$1" pin="2"/>
<wire x1="58.42" y1="73.66" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="73.66"/>
<pinref part="G100" gate="G$1" pin="2"/>
<wire x1="58.42" y1="60.96" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<junction x="58.42" y="60.96"/>
<pinref part="G101" gate="G$1" pin="2"/>
<wire x1="58.42" y1="48.26" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="48.26" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<junction x="58.42" y="48.26"/>
<pinref part="G102" gate="G$1" pin="2"/>
<wire x1="58.42" y1="35.56" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="35.56" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<junction x="58.42" y="35.56"/>
<pinref part="G103" gate="G$1" pin="2"/>
<wire x1="58.42" y1="22.86" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="22.86" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<junction x="58.42" y="22.86"/>
<pinref part="G104" gate="G$1" pin="2"/>
<wire x1="58.42" y1="10.16" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="10.16" x2="58.42" y2="5.08" width="0.1524" layer="91"/>
<junction x="58.42" y="10.16"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G105" gate="G$1" pin="2"/>
<wire x1="119.38" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="121.92" y1="101.6" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<pinref part="G106" gate="G$1" pin="2"/>
<wire x1="121.92" y1="88.9" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="121.92" y="88.9"/>
<pinref part="G107" gate="G$1" pin="2"/>
<wire x1="121.92" y1="76.2" x2="119.38" y2="76.2" width="0.1524" layer="91"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<junction x="121.92" y="76.2"/>
<pinref part="G108" gate="G$1" pin="2"/>
<wire x1="121.92" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
<junction x="121.92" y="63.5"/>
<pinref part="G109" gate="G$1" pin="2"/>
<wire x1="121.92" y1="50.8" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="50.8" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<junction x="121.92" y="50.8"/>
<pinref part="G110" gate="G$1" pin="2"/>
<wire x1="121.92" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="121.92" y1="38.1" x2="121.92" y2="25.4" width="0.1524" layer="91"/>
<junction x="121.92" y="38.1"/>
<pinref part="G111" gate="G$1" pin="2"/>
<wire x1="121.92" y1="25.4" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<wire x1="121.92" y1="25.4" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<junction x="121.92" y="25.4"/>
<pinref part="G112" gate="G$1" pin="2"/>
<wire x1="121.92" y1="12.7" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="121.92" y1="12.7" x2="121.92" y2="7.62" width="0.1524" layer="91"/>
<junction x="121.92" y="12.7"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G113" gate="G$1" pin="2"/>
<wire x1="182.88" y1="101.6" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="G114" gate="G$1" pin="2"/>
<wire x1="185.42" y1="88.9" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="185.42" y1="88.9" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="185.42" y="88.9"/>
<pinref part="G115" gate="G$1" pin="2"/>
<wire x1="185.42" y1="76.2" x2="182.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<junction x="185.42" y="76.2"/>
<pinref part="G116" gate="G$1" pin="2"/>
<wire x1="185.42" y1="63.5" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
<wire x1="185.42" y1="63.5" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="185.42" y="63.5"/>
<pinref part="G117" gate="G$1" pin="2"/>
<wire x1="185.42" y1="50.8" x2="182.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="185.42" y1="50.8" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<junction x="185.42" y="50.8"/>
<pinref part="G118" gate="G$1" pin="2"/>
<wire x1="185.42" y1="38.1" x2="182.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="185.42" y1="38.1" x2="185.42" y2="25.4" width="0.1524" layer="91"/>
<junction x="185.42" y="38.1"/>
<pinref part="G119" gate="G$1" pin="2"/>
<wire x1="185.42" y1="25.4" x2="182.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="185.42" y1="25.4" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<junction x="185.42" y="25.4"/>
<pinref part="G120" gate="G$1" pin="2"/>
<wire x1="185.42" y1="12.7" x2="182.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="12.7" x2="185.42" y2="7.62" width="0.1524" layer="91"/>
<junction x="185.42" y="12.7"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G121" gate="G$1" pin="2"/>
<wire x1="246.38" y1="101.6" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="248.92" y1="101.6" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<pinref part="G122" gate="G$1" pin="2"/>
<wire x1="248.92" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="248.92" y="88.9"/>
<pinref part="G123" gate="G$1" pin="2"/>
<wire x1="248.92" y1="76.2" x2="246.38" y2="76.2" width="0.1524" layer="91"/>
<wire x1="248.92" y1="76.2" x2="248.92" y2="63.5" width="0.1524" layer="91"/>
<junction x="248.92" y="76.2"/>
<pinref part="G124" gate="G$1" pin="2"/>
<wire x1="248.92" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="248.92" y1="63.5" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<junction x="248.92" y="63.5"/>
<pinref part="G125" gate="G$1" pin="2"/>
<wire x1="248.92" y1="50.8" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="50.8" x2="248.92" y2="38.1" width="0.1524" layer="91"/>
<junction x="248.92" y="50.8"/>
<pinref part="G126" gate="G$1" pin="2"/>
<wire x1="248.92" y1="38.1" x2="246.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="38.1" x2="248.92" y2="25.4" width="0.1524" layer="91"/>
<junction x="248.92" y="38.1"/>
<pinref part="G127" gate="G$1" pin="2"/>
<wire x1="248.92" y1="25.4" x2="246.38" y2="25.4" width="0.1524" layer="91"/>
<wire x1="248.92" y1="25.4" x2="248.92" y2="12.7" width="0.1524" layer="91"/>
<junction x="248.92" y="25.4"/>
<pinref part="G128" gate="G$1" pin="2"/>
<wire x1="248.92" y1="12.7" x2="246.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="248.92" y1="12.7" x2="248.92" y2="7.62" width="0.1524" layer="91"/>
<junction x="248.92" y="12.7"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="7.62" y1="177.8" x2="2.54" y2="177.8" width="0.1524" layer="91"/>
<wire x1="2.54" y1="177.8" x2="2.54" y2="175.26" width="0.1524" layer="91"/>
<wire x1="7.62" y1="175.26" x2="2.54" y2="175.26" width="0.1524" layer="91"/>
<junction x="2.54" y="175.26"/>
<wire x1="7.62" y1="172.72" x2="2.54" y2="172.72" width="0.1524" layer="91"/>
<wire x1="2.54" y1="172.72" x2="2.54" y2="175.26" width="0.1524" layer="91"/>
<junction x="2.54" y="172.72"/>
<wire x1="2.54" y1="172.72" x2="2.54" y2="170.18" width="0.1524" layer="91"/>
<wire x1="7.62" y1="170.18" x2="2.54" y2="170.18" width="0.1524" layer="91"/>
<junction x="2.54" y="170.18"/>
<wire x1="7.62" y1="167.64" x2="2.54" y2="167.64" width="0.1524" layer="91"/>
<wire x1="2.54" y1="167.64" x2="2.54" y2="170.18" width="0.1524" layer="91"/>
<junction x="2.54" y="167.64"/>
<wire x1="2.54" y1="167.64" x2="2.54" y2="165.1" width="0.1524" layer="91"/>
<wire x1="7.62" y1="165.1" x2="2.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="2.54" y="165.1"/>
<wire x1="7.62" y1="162.56" x2="2.54" y2="162.56" width="0.1524" layer="91"/>
<wire x1="2.54" y1="162.56" x2="2.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="2.54" y="162.56"/>
<wire x1="2.54" y1="162.56" x2="2.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="7.62" y1="160.02" x2="2.54" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV9" gate="1" pin="2"/>
<pinref part="SV9" gate="1" pin="4"/>
<pinref part="SV9" gate="1" pin="6"/>
<pinref part="SV9" gate="1" pin="8"/>
<pinref part="SV9" gate="1" pin="10"/>
<pinref part="SV9" gate="1" pin="12"/>
<pinref part="SV9" gate="1" pin="14"/>
<pinref part="SV9" gate="1" pin="16"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="2.54" y1="157.48" x2="2.54" y2="160.02" width="0.1524" layer="91"/>
<junction x="2.54" y="160.02"/>
</segment>
<segment>
<pinref part="SV13" gate="1" pin="4"/>
<wire x1="7.62" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="8"/>
<wire x1="7.62" y1="55.88" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="12"/>
<wire x1="7.62" y1="50.8" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="16"/>
<wire x1="7.62" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="14"/>
<wire x1="7.62" y1="48.26" x2="2.54" y2="48.26" width="0.1524" layer="91"/>
<wire x1="2.54" y1="48.26" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="2.54" y="48.26"/>
<wire x1="2.54" y1="48.26" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="2.54" y="50.8"/>
<pinref part="SV13" gate="1" pin="10"/>
<wire x1="7.62" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<wire x1="2.54" y1="53.34" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="2.54" y="53.34"/>
<wire x1="2.54" y1="53.34" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<junction x="2.54" y="55.88"/>
<pinref part="SV13" gate="1" pin="6"/>
<wire x1="7.62" y1="58.42" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<wire x1="2.54" y1="58.42" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<junction x="2.54" y="58.42"/>
<wire x1="2.54" y1="58.42" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="2.54" y="60.96"/>
<wire x1="2.54" y1="63.5" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<wire x1="7.62" y1="63.5" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV13" gate="1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="2.54" y1="43.18" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="2.54" y="45.72"/>
</segment>
<segment>
<wire x1="71.12" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="58.42" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="50.8" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="50.8" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<junction x="66.04" y="50.8"/>
<wire x1="66.04" y1="50.8" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<junction x="66.04" y="53.34"/>
<wire x1="71.12" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="55.88" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<junction x="66.04" y="55.88"/>
<wire x1="66.04" y1="55.88" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="66.04" y="58.42"/>
<wire x1="71.12" y1="60.96" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="60.96" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="66.04" y="60.96"/>
<wire x1="66.04" y1="60.96" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<junction x="66.04" y="63.5"/>
<wire x1="71.12" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV14" gate="1" pin="2"/>
<pinref part="SV14" gate="1" pin="4"/>
<pinref part="SV14" gate="1" pin="6"/>
<pinref part="SV14" gate="1" pin="8"/>
<pinref part="SV14" gate="1" pin="10"/>
<pinref part="SV14" gate="1" pin="12"/>
<pinref part="SV14" gate="1" pin="14"/>
<pinref part="SV14" gate="1" pin="16"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="66.04" y1="45.72" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<junction x="66.04" y="48.26"/>
</segment>
<segment>
<wire x1="71.12" y1="177.8" x2="66.04" y2="177.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="177.8" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="175.26" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<junction x="66.04" y="175.26"/>
<wire x1="71.12" y1="172.72" x2="66.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="172.72" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<junction x="66.04" y="172.72"/>
<wire x1="66.04" y1="172.72" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="170.18" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="66.04" y="170.18"/>
<wire x1="71.12" y1="167.64" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="167.64" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="66.04" y="167.64"/>
<wire x1="66.04" y1="167.64" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="165.1" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<junction x="66.04" y="165.1"/>
<wire x1="71.12" y1="162.56" x2="66.04" y2="162.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<junction x="66.04" y="162.56"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="71.12" y1="160.02" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV10" gate="1" pin="2"/>
<pinref part="SV10" gate="1" pin="4"/>
<pinref part="SV10" gate="1" pin="6"/>
<pinref part="SV10" gate="1" pin="8"/>
<pinref part="SV10" gate="1" pin="10"/>
<pinref part="SV10" gate="1" pin="12"/>
<pinref part="SV10" gate="1" pin="14"/>
<pinref part="SV10" gate="1" pin="16"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="66.04" y1="157.48" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
<junction x="66.04" y="160.02"/>
</segment>
<segment>
<wire x1="134.62" y1="177.8" x2="129.54" y2="177.8" width="0.1524" layer="91"/>
<wire x1="129.54" y1="177.8" x2="129.54" y2="175.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="175.26" x2="129.54" y2="175.26" width="0.1524" layer="91"/>
<junction x="129.54" y="175.26"/>
<wire x1="134.62" y1="172.72" x2="129.54" y2="172.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="172.72" x2="129.54" y2="175.26" width="0.1524" layer="91"/>
<junction x="129.54" y="172.72"/>
<wire x1="129.54" y1="172.72" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<wire x1="134.62" y1="170.18" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<junction x="129.54" y="170.18"/>
<wire x1="134.62" y1="167.64" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<wire x1="129.54" y1="167.64" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<junction x="129.54" y="167.64"/>
<wire x1="129.54" y1="167.64" x2="129.54" y2="165.1" width="0.1524" layer="91"/>
<wire x1="134.62" y1="165.1" x2="129.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="129.54" y="165.1"/>
<wire x1="134.62" y1="162.56" x2="129.54" y2="162.56" width="0.1524" layer="91"/>
<wire x1="129.54" y1="162.56" x2="129.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="129.54" y="162.56"/>
<wire x1="129.54" y1="162.56" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="134.62" y1="160.02" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV11" gate="1" pin="2"/>
<pinref part="SV11" gate="1" pin="4"/>
<pinref part="SV11" gate="1" pin="6"/>
<pinref part="SV11" gate="1" pin="8"/>
<pinref part="SV11" gate="1" pin="10"/>
<pinref part="SV11" gate="1" pin="12"/>
<pinref part="SV11" gate="1" pin="14"/>
<pinref part="SV11" gate="1" pin="16"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="129.54" y1="157.48" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<junction x="129.54" y="160.02"/>
</segment>
<segment>
<pinref part="SV15" gate="1" pin="4"/>
<wire x1="134.62" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="8"/>
<wire x1="134.62" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="12"/>
<wire x1="134.62" y1="53.34" x2="129.54" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="16"/>
<wire x1="134.62" y1="48.26" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="14"/>
<wire x1="134.62" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<junction x="129.54" y="50.8"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="53.34" width="0.1524" layer="91"/>
<junction x="129.54" y="53.34"/>
<pinref part="SV15" gate="1" pin="10"/>
<wire x1="134.62" y1="55.88" x2="129.54" y2="55.88" width="0.1524" layer="91"/>
<wire x1="129.54" y1="55.88" x2="129.54" y2="53.34" width="0.1524" layer="91"/>
<junction x="129.54" y="55.88"/>
<wire x1="129.54" y1="55.88" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="129.54" y="58.42"/>
<pinref part="SV15" gate="1" pin="6"/>
<wire x1="134.62" y1="60.96" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<wire x1="129.54" y1="60.96" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="129.54" y="60.96"/>
<wire x1="129.54" y1="60.96" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<junction x="129.54" y="63.5"/>
<wire x1="129.54" y1="66.04" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="134.62" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV15" gate="1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="129.54" y1="45.72" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<junction x="129.54" y="48.26"/>
</segment>
<segment>
<wire x1="198.12" y1="177.8" x2="193.04" y2="177.8" width="0.1524" layer="91"/>
<wire x1="193.04" y1="177.8" x2="193.04" y2="175.26" width="0.1524" layer="91"/>
<wire x1="198.12" y1="175.26" x2="193.04" y2="175.26" width="0.1524" layer="91"/>
<junction x="193.04" y="175.26"/>
<wire x1="198.12" y1="172.72" x2="193.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="193.04" y1="172.72" x2="193.04" y2="175.26" width="0.1524" layer="91"/>
<junction x="193.04" y="172.72"/>
<wire x1="193.04" y1="172.72" x2="193.04" y2="170.18" width="0.1524" layer="91"/>
<wire x1="198.12" y1="170.18" x2="193.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="193.04" y="170.18"/>
<wire x1="198.12" y1="167.64" x2="193.04" y2="167.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="167.64" x2="193.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="193.04" y="167.64"/>
<wire x1="193.04" y1="167.64" x2="193.04" y2="165.1" width="0.1524" layer="91"/>
<wire x1="198.12" y1="165.1" x2="193.04" y2="165.1" width="0.1524" layer="91"/>
<junction x="193.04" y="165.1"/>
<wire x1="198.12" y1="160.02" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="198.12" y1="162.56" x2="193.04" y2="162.56" width="0.1524" layer="91"/>
<wire x1="193.04" y1="162.56" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="193.04" y1="162.56" x2="193.04" y2="165.1" width="0.1524" layer="91"/>
<junction x="193.04" y="162.56"/>
<pinref part="SV12" gate="1" pin="2"/>
<pinref part="SV12" gate="1" pin="4"/>
<pinref part="SV12" gate="1" pin="6"/>
<pinref part="SV12" gate="1" pin="8"/>
<pinref part="SV12" gate="1" pin="10"/>
<pinref part="SV12" gate="1" pin="12"/>
<pinref part="SV12" gate="1" pin="14"/>
<pinref part="SV12" gate="1" pin="16"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="193.04" y1="157.48" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<junction x="193.04" y="160.02"/>
</segment>
<segment>
<wire x1="457.2" y1="53.34" x2="452.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="452.12" y1="53.34" x2="452.12" y2="50.8" width="0.1524" layer="91"/>
<wire x1="457.2" y1="50.8" x2="452.12" y2="50.8" width="0.1524" layer="91"/>
<wire x1="457.2" y1="58.42" x2="452.12" y2="58.42" width="0.1524" layer="91"/>
<wire x1="452.12" y1="58.42" x2="452.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="457.2" y1="55.88" x2="452.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="457.2" y1="63.5" x2="452.12" y2="63.5" width="0.1524" layer="91"/>
<wire x1="452.12" y1="63.5" x2="452.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="457.2" y1="60.96" x2="452.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="457.2" y1="68.58" x2="452.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="452.12" y1="68.58" x2="452.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="457.2" y1="66.04" x2="452.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="452.12" y1="63.5" x2="452.12" y2="66.04" width="0.1524" layer="91"/>
<junction x="452.12" y="63.5"/>
<junction x="452.12" y="66.04"/>
<wire x1="452.12" y1="58.42" x2="452.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="452.12" y="58.42"/>
<junction x="452.12" y="60.96"/>
<wire x1="452.12" y1="53.34" x2="452.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="452.12" y="53.34"/>
<junction x="452.12" y="55.88"/>
<wire x1="452.12" y1="50.8" x2="452.12" y2="48.26" width="0.1524" layer="91"/>
<junction x="452.12" y="50.8"/>
<pinref part="GND49" gate="1" pin="GND"/>
<pinref part="SV32" gate="1" pin="2"/>
<pinref part="SV32" gate="1" pin="4"/>
<pinref part="SV32" gate="1" pin="6"/>
<pinref part="SV32" gate="1" pin="8"/>
<pinref part="SV32" gate="1" pin="10"/>
<pinref part="SV32" gate="1" pin="12"/>
<pinref part="SV32" gate="1" pin="14"/>
<pinref part="SV32" gate="1" pin="16"/>
</segment>
<segment>
<pinref part="G193" gate="G$1" pin="2"/>
<wire x1="314.96" y1="215.9" x2="317.5" y2="215.9" width="0.1524" layer="91"/>
<wire x1="317.5" y1="215.9" x2="317.5" y2="203.2" width="0.1524" layer="91"/>
<pinref part="G194" gate="G$1" pin="2"/>
<wire x1="317.5" y1="203.2" x2="314.96" y2="203.2" width="0.1524" layer="91"/>
<wire x1="317.5" y1="203.2" x2="317.5" y2="190.5" width="0.1524" layer="91"/>
<junction x="317.5" y="203.2"/>
<pinref part="G195" gate="G$1" pin="2"/>
<wire x1="317.5" y1="190.5" x2="314.96" y2="190.5" width="0.1524" layer="91"/>
<wire x1="317.5" y1="190.5" x2="317.5" y2="177.8" width="0.1524" layer="91"/>
<junction x="317.5" y="190.5"/>
<pinref part="G196" gate="G$1" pin="2"/>
<wire x1="317.5" y1="177.8" x2="314.96" y2="177.8" width="0.1524" layer="91"/>
<wire x1="317.5" y1="177.8" x2="317.5" y2="165.1" width="0.1524" layer="91"/>
<junction x="317.5" y="177.8"/>
<pinref part="G197" gate="G$1" pin="2"/>
<wire x1="317.5" y1="165.1" x2="314.96" y2="165.1" width="0.1524" layer="91"/>
<wire x1="317.5" y1="165.1" x2="317.5" y2="152.4" width="0.1524" layer="91"/>
<junction x="317.5" y="165.1"/>
<pinref part="G198" gate="G$1" pin="2"/>
<wire x1="317.5" y1="152.4" x2="314.96" y2="152.4" width="0.1524" layer="91"/>
<wire x1="317.5" y1="152.4" x2="317.5" y2="139.7" width="0.1524" layer="91"/>
<junction x="317.5" y="152.4"/>
<pinref part="G199" gate="G$1" pin="2"/>
<wire x1="317.5" y1="139.7" x2="314.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="317.5" y1="139.7" x2="317.5" y2="127" width="0.1524" layer="91"/>
<junction x="317.5" y="139.7"/>
<pinref part="G200" gate="G$1" pin="2"/>
<wire x1="317.5" y1="127" x2="314.96" y2="127" width="0.1524" layer="91"/>
<wire x1="317.5" y1="127" x2="317.5" y2="121.92" width="0.1524" layer="91"/>
<junction x="317.5" y="127"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G201" gate="G$1" pin="2"/>
<wire x1="378.46" y1="215.9" x2="381" y2="215.9" width="0.1524" layer="91"/>
<wire x1="381" y1="215.9" x2="381" y2="203.2" width="0.1524" layer="91"/>
<pinref part="G202" gate="G$1" pin="2"/>
<wire x1="381" y1="203.2" x2="378.46" y2="203.2" width="0.1524" layer="91"/>
<wire x1="381" y1="203.2" x2="381" y2="190.5" width="0.1524" layer="91"/>
<junction x="381" y="203.2"/>
<pinref part="G203" gate="G$1" pin="2"/>
<wire x1="381" y1="190.5" x2="378.46" y2="190.5" width="0.1524" layer="91"/>
<wire x1="381" y1="190.5" x2="381" y2="177.8" width="0.1524" layer="91"/>
<junction x="381" y="190.5"/>
<pinref part="G204" gate="G$1" pin="2"/>
<wire x1="381" y1="177.8" x2="378.46" y2="177.8" width="0.1524" layer="91"/>
<wire x1="381" y1="177.8" x2="381" y2="165.1" width="0.1524" layer="91"/>
<junction x="381" y="177.8"/>
<pinref part="G205" gate="G$1" pin="2"/>
<wire x1="381" y1="165.1" x2="378.46" y2="165.1" width="0.1524" layer="91"/>
<wire x1="381" y1="165.1" x2="381" y2="152.4" width="0.1524" layer="91"/>
<junction x="381" y="165.1"/>
<pinref part="G206" gate="G$1" pin="2"/>
<wire x1="381" y1="152.4" x2="378.46" y2="152.4" width="0.1524" layer="91"/>
<wire x1="381" y1="152.4" x2="381" y2="139.7" width="0.1524" layer="91"/>
<junction x="381" y="152.4"/>
<pinref part="G207" gate="G$1" pin="2"/>
<wire x1="381" y1="139.7" x2="378.46" y2="139.7" width="0.1524" layer="91"/>
<wire x1="381" y1="139.7" x2="381" y2="127" width="0.1524" layer="91"/>
<junction x="381" y="139.7"/>
<pinref part="G208" gate="G$1" pin="2"/>
<wire x1="381" y1="127" x2="378.46" y2="127" width="0.1524" layer="91"/>
<wire x1="381" y1="127" x2="381" y2="121.92" width="0.1524" layer="91"/>
<junction x="381" y="127"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G209" gate="G$1" pin="2"/>
<wire x1="441.96" y1="215.9" x2="444.5" y2="215.9" width="0.1524" layer="91"/>
<wire x1="444.5" y1="215.9" x2="444.5" y2="203.2" width="0.1524" layer="91"/>
<pinref part="G210" gate="G$1" pin="2"/>
<wire x1="444.5" y1="203.2" x2="441.96" y2="203.2" width="0.1524" layer="91"/>
<wire x1="444.5" y1="203.2" x2="444.5" y2="190.5" width="0.1524" layer="91"/>
<junction x="444.5" y="203.2"/>
<pinref part="G211" gate="G$1" pin="2"/>
<wire x1="444.5" y1="190.5" x2="441.96" y2="190.5" width="0.1524" layer="91"/>
<wire x1="444.5" y1="190.5" x2="444.5" y2="177.8" width="0.1524" layer="91"/>
<junction x="444.5" y="190.5"/>
<pinref part="G212" gate="G$1" pin="2"/>
<wire x1="444.5" y1="177.8" x2="441.96" y2="177.8" width="0.1524" layer="91"/>
<wire x1="444.5" y1="177.8" x2="444.5" y2="165.1" width="0.1524" layer="91"/>
<junction x="444.5" y="177.8"/>
<pinref part="G213" gate="G$1" pin="2"/>
<wire x1="444.5" y1="165.1" x2="441.96" y2="165.1" width="0.1524" layer="91"/>
<wire x1="444.5" y1="165.1" x2="444.5" y2="152.4" width="0.1524" layer="91"/>
<junction x="444.5" y="165.1"/>
<pinref part="G214" gate="G$1" pin="2"/>
<wire x1="444.5" y1="152.4" x2="441.96" y2="152.4" width="0.1524" layer="91"/>
<wire x1="444.5" y1="152.4" x2="444.5" y2="139.7" width="0.1524" layer="91"/>
<junction x="444.5" y="152.4"/>
<pinref part="G215" gate="G$1" pin="2"/>
<wire x1="444.5" y1="139.7" x2="441.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="444.5" y1="139.7" x2="444.5" y2="127" width="0.1524" layer="91"/>
<junction x="444.5" y="139.7"/>
<pinref part="G216" gate="G$1" pin="2"/>
<wire x1="444.5" y1="127" x2="441.96" y2="127" width="0.1524" layer="91"/>
<wire x1="444.5" y1="127" x2="444.5" y2="121.92" width="0.1524" layer="91"/>
<junction x="444.5" y="127"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G217" gate="G$1" pin="2"/>
<wire x1="505.46" y1="215.9" x2="508" y2="215.9" width="0.1524" layer="91"/>
<wire x1="508" y1="215.9" x2="508" y2="203.2" width="0.1524" layer="91"/>
<pinref part="G218" gate="G$1" pin="2"/>
<wire x1="508" y1="203.2" x2="505.46" y2="203.2" width="0.1524" layer="91"/>
<wire x1="508" y1="203.2" x2="508" y2="190.5" width="0.1524" layer="91"/>
<junction x="508" y="203.2"/>
<pinref part="G219" gate="G$1" pin="2"/>
<wire x1="508" y1="190.5" x2="505.46" y2="190.5" width="0.1524" layer="91"/>
<wire x1="508" y1="190.5" x2="508" y2="177.8" width="0.1524" layer="91"/>
<junction x="508" y="190.5"/>
<pinref part="G220" gate="G$1" pin="2"/>
<wire x1="508" y1="177.8" x2="505.46" y2="177.8" width="0.1524" layer="91"/>
<wire x1="508" y1="177.8" x2="508" y2="165.1" width="0.1524" layer="91"/>
<junction x="508" y="177.8"/>
<pinref part="G221" gate="G$1" pin="2"/>
<wire x1="508" y1="165.1" x2="505.46" y2="165.1" width="0.1524" layer="91"/>
<wire x1="508" y1="165.1" x2="508" y2="152.4" width="0.1524" layer="91"/>
<junction x="508" y="165.1"/>
<pinref part="G222" gate="G$1" pin="2"/>
<wire x1="508" y1="152.4" x2="505.46" y2="152.4" width="0.1524" layer="91"/>
<wire x1="508" y1="152.4" x2="508" y2="139.7" width="0.1524" layer="91"/>
<junction x="508" y="152.4"/>
<pinref part="G223" gate="G$1" pin="2"/>
<wire x1="508" y1="139.7" x2="505.46" y2="139.7" width="0.1524" layer="91"/>
<wire x1="508" y1="139.7" x2="508" y2="127" width="0.1524" layer="91"/>
<junction x="508" y="139.7"/>
<pinref part="G224" gate="G$1" pin="2"/>
<wire x1="508" y1="127" x2="505.46" y2="127" width="0.1524" layer="91"/>
<wire x1="508" y1="127" x2="508" y2="121.92" width="0.1524" layer="91"/>
<junction x="508" y="127"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G225" gate="G$1" pin="2"/>
<wire x1="314.96" y1="101.6" x2="317.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="317.5" y1="101.6" x2="317.5" y2="88.9" width="0.1524" layer="91"/>
<pinref part="G226" gate="G$1" pin="2"/>
<wire x1="317.5" y1="88.9" x2="314.96" y2="88.9" width="0.1524" layer="91"/>
<wire x1="317.5" y1="88.9" x2="317.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="317.5" y="88.9"/>
<pinref part="G227" gate="G$1" pin="2"/>
<wire x1="317.5" y1="76.2" x2="314.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="317.5" y1="76.2" x2="317.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="317.5" y="76.2"/>
<pinref part="G228" gate="G$1" pin="2"/>
<wire x1="317.5" y1="63.5" x2="314.96" y2="63.5" width="0.1524" layer="91"/>
<wire x1="317.5" y1="63.5" x2="317.5" y2="50.8" width="0.1524" layer="91"/>
<junction x="317.5" y="63.5"/>
<pinref part="G229" gate="G$1" pin="2"/>
<wire x1="317.5" y1="50.8" x2="314.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="317.5" y1="50.8" x2="317.5" y2="38.1" width="0.1524" layer="91"/>
<junction x="317.5" y="50.8"/>
<pinref part="G230" gate="G$1" pin="2"/>
<wire x1="317.5" y1="38.1" x2="314.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="317.5" y1="38.1" x2="317.5" y2="25.4" width="0.1524" layer="91"/>
<junction x="317.5" y="38.1"/>
<pinref part="G231" gate="G$1" pin="2"/>
<wire x1="317.5" y1="25.4" x2="314.96" y2="25.4" width="0.1524" layer="91"/>
<wire x1="317.5" y1="25.4" x2="317.5" y2="12.7" width="0.1524" layer="91"/>
<junction x="317.5" y="25.4"/>
<pinref part="G232" gate="G$1" pin="2"/>
<wire x1="317.5" y1="12.7" x2="314.96" y2="12.7" width="0.1524" layer="91"/>
<wire x1="317.5" y1="12.7" x2="317.5" y2="7.62" width="0.1524" layer="91"/>
<junction x="317.5" y="12.7"/>
<pinref part="GND54" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G233" gate="G$1" pin="2"/>
<wire x1="378.46" y1="104.14" x2="381" y2="104.14" width="0.1524" layer="91"/>
<wire x1="381" y1="104.14" x2="381" y2="91.44" width="0.1524" layer="91"/>
<pinref part="G234" gate="G$1" pin="2"/>
<wire x1="381" y1="91.44" x2="378.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="381" y1="91.44" x2="381" y2="78.74" width="0.1524" layer="91"/>
<junction x="381" y="91.44"/>
<pinref part="G235" gate="G$1" pin="2"/>
<wire x1="381" y1="78.74" x2="378.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="381" y1="78.74" x2="381" y2="66.04" width="0.1524" layer="91"/>
<junction x="381" y="78.74"/>
<pinref part="G236" gate="G$1" pin="2"/>
<wire x1="381" y1="66.04" x2="378.46" y2="66.04" width="0.1524" layer="91"/>
<wire x1="381" y1="66.04" x2="381" y2="53.34" width="0.1524" layer="91"/>
<junction x="381" y="66.04"/>
<pinref part="G237" gate="G$1" pin="2"/>
<wire x1="381" y1="53.34" x2="378.46" y2="53.34" width="0.1524" layer="91"/>
<wire x1="381" y1="53.34" x2="381" y2="40.64" width="0.1524" layer="91"/>
<junction x="381" y="53.34"/>
<pinref part="G238" gate="G$1" pin="2"/>
<wire x1="381" y1="40.64" x2="378.46" y2="40.64" width="0.1524" layer="91"/>
<wire x1="381" y1="40.64" x2="381" y2="27.94" width="0.1524" layer="91"/>
<junction x="381" y="40.64"/>
<pinref part="G239" gate="G$1" pin="2"/>
<wire x1="381" y1="27.94" x2="378.46" y2="27.94" width="0.1524" layer="91"/>
<wire x1="381" y1="27.94" x2="381" y2="15.24" width="0.1524" layer="91"/>
<junction x="381" y="27.94"/>
<pinref part="G240" gate="G$1" pin="2"/>
<wire x1="381" y1="15.24" x2="378.46" y2="15.24" width="0.1524" layer="91"/>
<wire x1="381" y1="15.24" x2="381" y2="10.16" width="0.1524" layer="91"/>
<junction x="381" y="15.24"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G241" gate="G$1" pin="2"/>
<wire x1="441.96" y1="104.14" x2="444.5" y2="104.14" width="0.1524" layer="91"/>
<wire x1="444.5" y1="104.14" x2="444.5" y2="91.44" width="0.1524" layer="91"/>
<pinref part="G242" gate="G$1" pin="2"/>
<wire x1="444.5" y1="91.44" x2="441.96" y2="91.44" width="0.1524" layer="91"/>
<wire x1="444.5" y1="91.44" x2="444.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="444.5" y="91.44"/>
<pinref part="G243" gate="G$1" pin="2"/>
<wire x1="444.5" y1="78.74" x2="441.96" y2="78.74" width="0.1524" layer="91"/>
<wire x1="444.5" y1="78.74" x2="444.5" y2="66.04" width="0.1524" layer="91"/>
<junction x="444.5" y="78.74"/>
<pinref part="G244" gate="G$1" pin="2"/>
<wire x1="444.5" y1="66.04" x2="441.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="444.5" y1="66.04" x2="444.5" y2="53.34" width="0.1524" layer="91"/>
<junction x="444.5" y="66.04"/>
<pinref part="G245" gate="G$1" pin="2"/>
<wire x1="444.5" y1="53.34" x2="441.96" y2="53.34" width="0.1524" layer="91"/>
<wire x1="444.5" y1="53.34" x2="444.5" y2="40.64" width="0.1524" layer="91"/>
<junction x="444.5" y="53.34"/>
<pinref part="G246" gate="G$1" pin="2"/>
<wire x1="444.5" y1="40.64" x2="441.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="444.5" y1="40.64" x2="444.5" y2="27.94" width="0.1524" layer="91"/>
<junction x="444.5" y="40.64"/>
<pinref part="G247" gate="G$1" pin="2"/>
<wire x1="444.5" y1="27.94" x2="441.96" y2="27.94" width="0.1524" layer="91"/>
<wire x1="444.5" y1="27.94" x2="444.5" y2="15.24" width="0.1524" layer="91"/>
<junction x="444.5" y="27.94"/>
<pinref part="G248" gate="G$1" pin="2"/>
<wire x1="444.5" y1="15.24" x2="441.96" y2="15.24" width="0.1524" layer="91"/>
<wire x1="444.5" y1="15.24" x2="444.5" y2="10.16" width="0.1524" layer="91"/>
<junction x="444.5" y="15.24"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G249" gate="G$1" pin="2"/>
<wire x1="505.46" y1="104.14" x2="508" y2="104.14" width="0.1524" layer="91"/>
<wire x1="508" y1="104.14" x2="508" y2="91.44" width="0.1524" layer="91"/>
<pinref part="G250" gate="G$1" pin="2"/>
<wire x1="508" y1="91.44" x2="505.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="508" y1="91.44" x2="508" y2="78.74" width="0.1524" layer="91"/>
<junction x="508" y="91.44"/>
<pinref part="G251" gate="G$1" pin="2"/>
<wire x1="508" y1="78.74" x2="505.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="508" y1="78.74" x2="508" y2="66.04" width="0.1524" layer="91"/>
<junction x="508" y="78.74"/>
<pinref part="G252" gate="G$1" pin="2"/>
<wire x1="508" y1="66.04" x2="505.46" y2="66.04" width="0.1524" layer="91"/>
<wire x1="508" y1="66.04" x2="508" y2="53.34" width="0.1524" layer="91"/>
<junction x="508" y="66.04"/>
<pinref part="G253" gate="G$1" pin="2"/>
<wire x1="508" y1="53.34" x2="505.46" y2="53.34" width="0.1524" layer="91"/>
<wire x1="508" y1="53.34" x2="508" y2="40.64" width="0.1524" layer="91"/>
<junction x="508" y="53.34"/>
<pinref part="G254" gate="G$1" pin="2"/>
<wire x1="508" y1="40.64" x2="505.46" y2="40.64" width="0.1524" layer="91"/>
<wire x1="508" y1="40.64" x2="508" y2="27.94" width="0.1524" layer="91"/>
<junction x="508" y="40.64"/>
<pinref part="G255" gate="G$1" pin="2"/>
<wire x1="508" y1="27.94" x2="505.46" y2="27.94" width="0.1524" layer="91"/>
<wire x1="508" y1="27.94" x2="508" y2="15.24" width="0.1524" layer="91"/>
<junction x="508" y="27.94"/>
<pinref part="G256" gate="G$1" pin="2"/>
<wire x1="508" y1="15.24" x2="505.46" y2="15.24" width="0.1524" layer="91"/>
<wire x1="508" y1="15.24" x2="508" y2="10.16" width="0.1524" layer="91"/>
<junction x="508" y="15.24"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="266.7" y1="180.34" x2="261.62" y2="180.34" width="0.1524" layer="91"/>
<wire x1="261.62" y1="180.34" x2="261.62" y2="177.8" width="0.1524" layer="91"/>
<wire x1="266.7" y1="177.8" x2="261.62" y2="177.8" width="0.1524" layer="91"/>
<junction x="261.62" y="177.8"/>
<wire x1="266.7" y1="175.26" x2="261.62" y2="175.26" width="0.1524" layer="91"/>
<wire x1="261.62" y1="175.26" x2="261.62" y2="177.8" width="0.1524" layer="91"/>
<junction x="261.62" y="175.26"/>
<wire x1="261.62" y1="175.26" x2="261.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="266.7" y1="172.72" x2="261.62" y2="172.72" width="0.1524" layer="91"/>
<junction x="261.62" y="172.72"/>
<wire x1="266.7" y1="170.18" x2="261.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="261.62" y1="170.18" x2="261.62" y2="172.72" width="0.1524" layer="91"/>
<junction x="261.62" y="170.18"/>
<wire x1="261.62" y1="170.18" x2="261.62" y2="167.64" width="0.1524" layer="91"/>
<wire x1="266.7" y1="167.64" x2="261.62" y2="167.64" width="0.1524" layer="91"/>
<junction x="261.62" y="167.64"/>
<wire x1="266.7" y1="165.1" x2="261.62" y2="165.1" width="0.1524" layer="91"/>
<wire x1="261.62" y1="165.1" x2="261.62" y2="167.64" width="0.1524" layer="91"/>
<junction x="261.62" y="165.1"/>
<wire x1="261.62" y1="165.1" x2="261.62" y2="162.56" width="0.1524" layer="91"/>
<wire x1="266.7" y1="162.56" x2="261.62" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="2"/>
<pinref part="SV25" gate="1" pin="4"/>
<pinref part="SV25" gate="1" pin="6"/>
<pinref part="SV25" gate="1" pin="8"/>
<pinref part="SV25" gate="1" pin="10"/>
<pinref part="SV25" gate="1" pin="12"/>
<pinref part="SV25" gate="1" pin="14"/>
<pinref part="SV25" gate="1" pin="16"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="261.62" y1="160.02" x2="261.62" y2="162.56" width="0.1524" layer="91"/>
<junction x="261.62" y="162.56"/>
</segment>
<segment>
<pinref part="SV29" gate="1" pin="4"/>
<wire x1="266.7" y1="63.5" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="8"/>
<wire x1="266.7" y1="58.42" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="12"/>
<wire x1="266.7" y1="53.34" x2="261.62" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="16"/>
<wire x1="266.7" y1="48.26" x2="261.62" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="14"/>
<wire x1="266.7" y1="50.8" x2="261.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="261.62" y1="50.8" x2="261.62" y2="48.26" width="0.1524" layer="91"/>
<junction x="261.62" y="50.8"/>
<wire x1="261.62" y1="50.8" x2="261.62" y2="53.34" width="0.1524" layer="91"/>
<junction x="261.62" y="53.34"/>
<pinref part="SV29" gate="1" pin="10"/>
<wire x1="266.7" y1="55.88" x2="261.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="261.62" y1="55.88" x2="261.62" y2="53.34" width="0.1524" layer="91"/>
<junction x="261.62" y="55.88"/>
<wire x1="261.62" y1="55.88" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
<junction x="261.62" y="58.42"/>
<pinref part="SV29" gate="1" pin="6"/>
<wire x1="266.7" y1="60.96" x2="261.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="60.96" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
<junction x="261.62" y="60.96"/>
<wire x1="261.62" y1="60.96" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<junction x="261.62" y="63.5"/>
<wire x1="261.62" y1="66.04" x2="261.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="266.7" y1="66.04" x2="261.62" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="2"/>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="261.62" y1="45.72" x2="261.62" y2="48.26" width="0.1524" layer="91"/>
<junction x="261.62" y="48.26"/>
</segment>
<segment>
<wire x1="330.2" y1="66.04" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="330.2" y1="60.96" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="330.2" y1="55.88" x2="325.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="330.2" y1="50.8" x2="325.12" y2="50.8" width="0.1524" layer="91"/>
<wire x1="330.2" y1="53.34" x2="325.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="325.12" y1="53.34" x2="325.12" y2="50.8" width="0.1524" layer="91"/>
<junction x="325.12" y="53.34"/>
<wire x1="325.12" y1="53.34" x2="325.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="325.12" y="55.88"/>
<wire x1="330.2" y1="58.42" x2="325.12" y2="58.42" width="0.1524" layer="91"/>
<wire x1="325.12" y1="58.42" x2="325.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="325.12" y="58.42"/>
<wire x1="325.12" y1="58.42" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="325.12" y="60.96"/>
<wire x1="330.2" y1="63.5" x2="325.12" y2="63.5" width="0.1524" layer="91"/>
<wire x1="325.12" y1="63.5" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="325.12" y="63.5"/>
<wire x1="325.12" y1="63.5" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<junction x="325.12" y="66.04"/>
<wire x1="330.2" y1="68.58" x2="325.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="325.12" y1="68.58" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="2"/>
<pinref part="SV30" gate="1" pin="4"/>
<pinref part="SV30" gate="1" pin="6"/>
<pinref part="SV30" gate="1" pin="8"/>
<pinref part="SV30" gate="1" pin="10"/>
<pinref part="SV30" gate="1" pin="12"/>
<pinref part="SV30" gate="1" pin="14"/>
<pinref part="SV30" gate="1" pin="16"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="325.12" y1="48.26" x2="325.12" y2="50.8" width="0.1524" layer="91"/>
<junction x="325.12" y="50.8"/>
</segment>
<segment>
<wire x1="330.2" y1="180.34" x2="325.12" y2="180.34" width="0.1524" layer="91"/>
<wire x1="325.12" y1="180.34" x2="325.12" y2="177.8" width="0.1524" layer="91"/>
<wire x1="330.2" y1="177.8" x2="325.12" y2="177.8" width="0.1524" layer="91"/>
<junction x="325.12" y="177.8"/>
<wire x1="330.2" y1="175.26" x2="325.12" y2="175.26" width="0.1524" layer="91"/>
<wire x1="325.12" y1="175.26" x2="325.12" y2="177.8" width="0.1524" layer="91"/>
<junction x="325.12" y="175.26"/>
<wire x1="325.12" y1="175.26" x2="325.12" y2="172.72" width="0.1524" layer="91"/>
<wire x1="330.2" y1="172.72" x2="325.12" y2="172.72" width="0.1524" layer="91"/>
<junction x="325.12" y="172.72"/>
<wire x1="330.2" y1="170.18" x2="325.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="325.12" y1="170.18" x2="325.12" y2="172.72" width="0.1524" layer="91"/>
<junction x="325.12" y="170.18"/>
<wire x1="325.12" y1="170.18" x2="325.12" y2="167.64" width="0.1524" layer="91"/>
<wire x1="330.2" y1="167.64" x2="325.12" y2="167.64" width="0.1524" layer="91"/>
<junction x="325.12" y="167.64"/>
<wire x1="330.2" y1="165.1" x2="325.12" y2="165.1" width="0.1524" layer="91"/>
<wire x1="325.12" y1="165.1" x2="325.12" y2="167.64" width="0.1524" layer="91"/>
<junction x="325.12" y="165.1"/>
<wire x1="325.12" y1="165.1" x2="325.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="162.56" x2="325.12" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="2"/>
<pinref part="SV26" gate="1" pin="4"/>
<pinref part="SV26" gate="1" pin="6"/>
<pinref part="SV26" gate="1" pin="8"/>
<pinref part="SV26" gate="1" pin="10"/>
<pinref part="SV26" gate="1" pin="12"/>
<pinref part="SV26" gate="1" pin="14"/>
<pinref part="SV26" gate="1" pin="16"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="325.12" y1="160.02" x2="325.12" y2="162.56" width="0.1524" layer="91"/>
<junction x="325.12" y="162.56"/>
</segment>
<segment>
<wire x1="393.7" y1="180.34" x2="388.62" y2="180.34" width="0.1524" layer="91"/>
<wire x1="388.62" y1="180.34" x2="388.62" y2="177.8" width="0.1524" layer="91"/>
<wire x1="393.7" y1="177.8" x2="388.62" y2="177.8" width="0.1524" layer="91"/>
<junction x="388.62" y="177.8"/>
<wire x1="393.7" y1="175.26" x2="388.62" y2="175.26" width="0.1524" layer="91"/>
<wire x1="388.62" y1="175.26" x2="388.62" y2="177.8" width="0.1524" layer="91"/>
<junction x="388.62" y="175.26"/>
<wire x1="388.62" y1="175.26" x2="388.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="393.7" y1="172.72" x2="388.62" y2="172.72" width="0.1524" layer="91"/>
<junction x="388.62" y="172.72"/>
<wire x1="393.7" y1="170.18" x2="388.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="388.62" y1="170.18" x2="388.62" y2="172.72" width="0.1524" layer="91"/>
<junction x="388.62" y="170.18"/>
<wire x1="388.62" y1="170.18" x2="388.62" y2="167.64" width="0.1524" layer="91"/>
<wire x1="393.7" y1="167.64" x2="388.62" y2="167.64" width="0.1524" layer="91"/>
<junction x="388.62" y="167.64"/>
<wire x1="393.7" y1="165.1" x2="388.62" y2="165.1" width="0.1524" layer="91"/>
<wire x1="388.62" y1="165.1" x2="388.62" y2="167.64" width="0.1524" layer="91"/>
<junction x="388.62" y="165.1"/>
<wire x1="388.62" y1="165.1" x2="388.62" y2="162.56" width="0.1524" layer="91"/>
<wire x1="393.7" y1="162.56" x2="388.62" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="2"/>
<pinref part="SV27" gate="1" pin="4"/>
<pinref part="SV27" gate="1" pin="6"/>
<pinref part="SV27" gate="1" pin="8"/>
<pinref part="SV27" gate="1" pin="10"/>
<pinref part="SV27" gate="1" pin="12"/>
<pinref part="SV27" gate="1" pin="14"/>
<pinref part="SV27" gate="1" pin="16"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="388.62" y1="160.02" x2="388.62" y2="162.56" width="0.1524" layer="91"/>
<junction x="388.62" y="162.56"/>
</segment>
<segment>
<pinref part="SV31" gate="1" pin="4"/>
<wire x1="393.7" y1="66.04" x2="388.62" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="8"/>
<wire x1="393.7" y1="60.96" x2="388.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="12"/>
<wire x1="393.7" y1="55.88" x2="388.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="16"/>
<wire x1="393.7" y1="50.8" x2="388.62" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="14"/>
<wire x1="393.7" y1="53.34" x2="388.62" y2="53.34" width="0.1524" layer="91"/>
<wire x1="388.62" y1="53.34" x2="388.62" y2="50.8" width="0.1524" layer="91"/>
<junction x="388.62" y="53.34"/>
<wire x1="388.62" y1="53.34" x2="388.62" y2="55.88" width="0.1524" layer="91"/>
<junction x="388.62" y="55.88"/>
<pinref part="SV31" gate="1" pin="10"/>
<wire x1="393.7" y1="58.42" x2="388.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="388.62" y1="58.42" x2="388.62" y2="55.88" width="0.1524" layer="91"/>
<junction x="388.62" y="58.42"/>
<wire x1="388.62" y1="58.42" x2="388.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="388.62" y="60.96"/>
<pinref part="SV31" gate="1" pin="6"/>
<wire x1="393.7" y1="63.5" x2="388.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="388.62" y1="63.5" x2="388.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="388.62" y="63.5"/>
<wire x1="388.62" y1="63.5" x2="388.62" y2="66.04" width="0.1524" layer="91"/>
<junction x="388.62" y="66.04"/>
<wire x1="388.62" y1="68.58" x2="388.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="393.7" y1="68.58" x2="388.62" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="2"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="388.62" y1="48.26" x2="388.62" y2="50.8" width="0.1524" layer="91"/>
<junction x="388.62" y="50.8"/>
</segment>
<segment>
<wire x1="457.2" y1="180.34" x2="452.12" y2="180.34" width="0.1524" layer="91"/>
<wire x1="452.12" y1="180.34" x2="452.12" y2="177.8" width="0.1524" layer="91"/>
<wire x1="457.2" y1="177.8" x2="452.12" y2="177.8" width="0.1524" layer="91"/>
<junction x="452.12" y="177.8"/>
<wire x1="457.2" y1="175.26" x2="452.12" y2="175.26" width="0.1524" layer="91"/>
<wire x1="452.12" y1="175.26" x2="452.12" y2="177.8" width="0.1524" layer="91"/>
<junction x="452.12" y="175.26"/>
<wire x1="452.12" y1="175.26" x2="452.12" y2="172.72" width="0.1524" layer="91"/>
<wire x1="457.2" y1="172.72" x2="452.12" y2="172.72" width="0.1524" layer="91"/>
<junction x="452.12" y="172.72"/>
<wire x1="457.2" y1="170.18" x2="452.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="452.12" y1="170.18" x2="452.12" y2="172.72" width="0.1524" layer="91"/>
<junction x="452.12" y="170.18"/>
<wire x1="452.12" y1="170.18" x2="452.12" y2="167.64" width="0.1524" layer="91"/>
<wire x1="457.2" y1="167.64" x2="452.12" y2="167.64" width="0.1524" layer="91"/>
<junction x="452.12" y="167.64"/>
<wire x1="457.2" y1="162.56" x2="452.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="457.2" y1="165.1" x2="452.12" y2="165.1" width="0.1524" layer="91"/>
<wire x1="452.12" y1="165.1" x2="452.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="452.12" y1="165.1" x2="452.12" y2="167.64" width="0.1524" layer="91"/>
<junction x="452.12" y="165.1"/>
<pinref part="SV28" gate="1" pin="2"/>
<pinref part="SV28" gate="1" pin="4"/>
<pinref part="SV28" gate="1" pin="6"/>
<pinref part="SV28" gate="1" pin="8"/>
<pinref part="SV28" gate="1" pin="10"/>
<pinref part="SV28" gate="1" pin="12"/>
<pinref part="SV28" gate="1" pin="14"/>
<pinref part="SV28" gate="1" pin="16"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="452.12" y1="160.02" x2="452.12" y2="162.56" width="0.1524" layer="91"/>
<junction x="452.12" y="162.56"/>
</segment>
<segment>
<wire x1="198.12" y1="279.4" x2="193.04" y2="279.4" width="0.1524" layer="91"/>
<wire x1="193.04" y1="279.4" x2="193.04" y2="276.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="276.86" x2="193.04" y2="276.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="284.48" x2="193.04" y2="284.48" width="0.1524" layer="91"/>
<wire x1="193.04" y1="284.48" x2="193.04" y2="281.94" width="0.1524" layer="91"/>
<wire x1="198.12" y1="281.94" x2="193.04" y2="281.94" width="0.1524" layer="91"/>
<wire x1="198.12" y1="289.56" x2="193.04" y2="289.56" width="0.1524" layer="91"/>
<wire x1="193.04" y1="289.56" x2="193.04" y2="287.02" width="0.1524" layer="91"/>
<wire x1="198.12" y1="287.02" x2="193.04" y2="287.02" width="0.1524" layer="91"/>
<wire x1="198.12" y1="294.64" x2="193.04" y2="294.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="294.64" x2="193.04" y2="292.1" width="0.1524" layer="91"/>
<wire x1="198.12" y1="292.1" x2="193.04" y2="292.1" width="0.1524" layer="91"/>
<wire x1="193.04" y1="289.56" x2="193.04" y2="292.1" width="0.1524" layer="91"/>
<junction x="193.04" y="289.56"/>
<junction x="193.04" y="292.1"/>
<wire x1="193.04" y1="284.48" x2="193.04" y2="287.02" width="0.1524" layer="91"/>
<junction x="193.04" y="284.48"/>
<junction x="193.04" y="287.02"/>
<wire x1="193.04" y1="279.4" x2="193.04" y2="281.94" width="0.1524" layer="91"/>
<junction x="193.04" y="279.4"/>
<junction x="193.04" y="281.94"/>
<wire x1="193.04" y1="276.86" x2="193.04" y2="274.32" width="0.1524" layer="91"/>
<junction x="193.04" y="276.86"/>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="SV24" gate="1" pin="2"/>
<pinref part="SV24" gate="1" pin="4"/>
<pinref part="SV24" gate="1" pin="6"/>
<pinref part="SV24" gate="1" pin="8"/>
<pinref part="SV24" gate="1" pin="10"/>
<pinref part="SV24" gate="1" pin="12"/>
<pinref part="SV24" gate="1" pin="14"/>
<pinref part="SV24" gate="1" pin="16"/>
</segment>
<segment>
<pinref part="G129" gate="G$1" pin="2"/>
<wire x1="55.88" y1="441.96" x2="58.42" y2="441.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="441.96" x2="58.42" y2="429.26" width="0.1524" layer="91"/>
<pinref part="G130" gate="G$1" pin="2"/>
<wire x1="58.42" y1="429.26" x2="55.88" y2="429.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="429.26" x2="58.42" y2="416.56" width="0.1524" layer="91"/>
<junction x="58.42" y="429.26"/>
<pinref part="G131" gate="G$1" pin="2"/>
<wire x1="58.42" y1="416.56" x2="55.88" y2="416.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="416.56" x2="58.42" y2="403.86" width="0.1524" layer="91"/>
<junction x="58.42" y="416.56"/>
<pinref part="G132" gate="G$1" pin="2"/>
<wire x1="58.42" y1="403.86" x2="55.88" y2="403.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="403.86" x2="58.42" y2="391.16" width="0.1524" layer="91"/>
<junction x="58.42" y="403.86"/>
<pinref part="G133" gate="G$1" pin="2"/>
<wire x1="58.42" y1="391.16" x2="55.88" y2="391.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="391.16" x2="58.42" y2="378.46" width="0.1524" layer="91"/>
<junction x="58.42" y="391.16"/>
<pinref part="G134" gate="G$1" pin="2"/>
<wire x1="58.42" y1="378.46" x2="55.88" y2="378.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="378.46" x2="58.42" y2="365.76" width="0.1524" layer="91"/>
<junction x="58.42" y="378.46"/>
<pinref part="G135" gate="G$1" pin="2"/>
<wire x1="58.42" y1="365.76" x2="55.88" y2="365.76" width="0.1524" layer="91"/>
<wire x1="58.42" y1="365.76" x2="58.42" y2="353.06" width="0.1524" layer="91"/>
<junction x="58.42" y="365.76"/>
<pinref part="G136" gate="G$1" pin="2"/>
<wire x1="58.42" y1="353.06" x2="55.88" y2="353.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="353.06" x2="58.42" y2="347.98" width="0.1524" layer="91"/>
<junction x="58.42" y="353.06"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G137" gate="G$1" pin="2"/>
<wire x1="119.38" y1="441.96" x2="121.92" y2="441.96" width="0.1524" layer="91"/>
<wire x1="121.92" y1="441.96" x2="121.92" y2="429.26" width="0.1524" layer="91"/>
<pinref part="G138" gate="G$1" pin="2"/>
<wire x1="121.92" y1="429.26" x2="119.38" y2="429.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="429.26" x2="121.92" y2="416.56" width="0.1524" layer="91"/>
<junction x="121.92" y="429.26"/>
<pinref part="G139" gate="G$1" pin="2"/>
<wire x1="121.92" y1="416.56" x2="119.38" y2="416.56" width="0.1524" layer="91"/>
<wire x1="121.92" y1="416.56" x2="121.92" y2="403.86" width="0.1524" layer="91"/>
<junction x="121.92" y="416.56"/>
<pinref part="G140" gate="G$1" pin="2"/>
<wire x1="121.92" y1="403.86" x2="119.38" y2="403.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="403.86" x2="121.92" y2="391.16" width="0.1524" layer="91"/>
<junction x="121.92" y="403.86"/>
<pinref part="G141" gate="G$1" pin="2"/>
<wire x1="121.92" y1="391.16" x2="119.38" y2="391.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="391.16" x2="121.92" y2="378.46" width="0.1524" layer="91"/>
<junction x="121.92" y="391.16"/>
<pinref part="G142" gate="G$1" pin="2"/>
<wire x1="121.92" y1="378.46" x2="119.38" y2="378.46" width="0.1524" layer="91"/>
<wire x1="121.92" y1="378.46" x2="121.92" y2="365.76" width="0.1524" layer="91"/>
<junction x="121.92" y="378.46"/>
<pinref part="G143" gate="G$1" pin="2"/>
<wire x1="121.92" y1="365.76" x2="119.38" y2="365.76" width="0.1524" layer="91"/>
<wire x1="121.92" y1="365.76" x2="121.92" y2="353.06" width="0.1524" layer="91"/>
<junction x="121.92" y="365.76"/>
<pinref part="G144" gate="G$1" pin="2"/>
<wire x1="121.92" y1="353.06" x2="119.38" y2="353.06" width="0.1524" layer="91"/>
<wire x1="121.92" y1="353.06" x2="121.92" y2="347.98" width="0.1524" layer="91"/>
<junction x="121.92" y="353.06"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G145" gate="G$1" pin="2"/>
<wire x1="182.88" y1="441.96" x2="185.42" y2="441.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="441.96" x2="185.42" y2="429.26" width="0.1524" layer="91"/>
<pinref part="G146" gate="G$1" pin="2"/>
<wire x1="185.42" y1="429.26" x2="182.88" y2="429.26" width="0.1524" layer="91"/>
<wire x1="185.42" y1="429.26" x2="185.42" y2="416.56" width="0.1524" layer="91"/>
<junction x="185.42" y="429.26"/>
<pinref part="G147" gate="G$1" pin="2"/>
<wire x1="185.42" y1="416.56" x2="182.88" y2="416.56" width="0.1524" layer="91"/>
<wire x1="185.42" y1="416.56" x2="185.42" y2="403.86" width="0.1524" layer="91"/>
<junction x="185.42" y="416.56"/>
<pinref part="G148" gate="G$1" pin="2"/>
<wire x1="185.42" y1="403.86" x2="182.88" y2="403.86" width="0.1524" layer="91"/>
<wire x1="185.42" y1="403.86" x2="185.42" y2="391.16" width="0.1524" layer="91"/>
<junction x="185.42" y="403.86"/>
<pinref part="G149" gate="G$1" pin="2"/>
<wire x1="185.42" y1="391.16" x2="182.88" y2="391.16" width="0.1524" layer="91"/>
<wire x1="185.42" y1="391.16" x2="185.42" y2="378.46" width="0.1524" layer="91"/>
<junction x="185.42" y="391.16"/>
<pinref part="G150" gate="G$1" pin="2"/>
<wire x1="185.42" y1="378.46" x2="182.88" y2="378.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="378.46" x2="185.42" y2="365.76" width="0.1524" layer="91"/>
<junction x="185.42" y="378.46"/>
<pinref part="G151" gate="G$1" pin="2"/>
<wire x1="185.42" y1="365.76" x2="182.88" y2="365.76" width="0.1524" layer="91"/>
<wire x1="185.42" y1="365.76" x2="185.42" y2="353.06" width="0.1524" layer="91"/>
<junction x="185.42" y="365.76"/>
<pinref part="G152" gate="G$1" pin="2"/>
<wire x1="185.42" y1="353.06" x2="182.88" y2="353.06" width="0.1524" layer="91"/>
<wire x1="185.42" y1="353.06" x2="185.42" y2="347.98" width="0.1524" layer="91"/>
<junction x="185.42" y="353.06"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G153" gate="G$1" pin="2"/>
<wire x1="246.38" y1="441.96" x2="248.92" y2="441.96" width="0.1524" layer="91"/>
<wire x1="248.92" y1="441.96" x2="248.92" y2="429.26" width="0.1524" layer="91"/>
<pinref part="G154" gate="G$1" pin="2"/>
<wire x1="248.92" y1="429.26" x2="246.38" y2="429.26" width="0.1524" layer="91"/>
<wire x1="248.92" y1="429.26" x2="248.92" y2="416.56" width="0.1524" layer="91"/>
<junction x="248.92" y="429.26"/>
<pinref part="G155" gate="G$1" pin="2"/>
<wire x1="248.92" y1="416.56" x2="246.38" y2="416.56" width="0.1524" layer="91"/>
<wire x1="248.92" y1="416.56" x2="248.92" y2="403.86" width="0.1524" layer="91"/>
<junction x="248.92" y="416.56"/>
<pinref part="G156" gate="G$1" pin="2"/>
<wire x1="248.92" y1="403.86" x2="246.38" y2="403.86" width="0.1524" layer="91"/>
<wire x1="248.92" y1="403.86" x2="248.92" y2="391.16" width="0.1524" layer="91"/>
<junction x="248.92" y="403.86"/>
<pinref part="G157" gate="G$1" pin="2"/>
<wire x1="248.92" y1="391.16" x2="246.38" y2="391.16" width="0.1524" layer="91"/>
<wire x1="248.92" y1="391.16" x2="248.92" y2="378.46" width="0.1524" layer="91"/>
<junction x="248.92" y="391.16"/>
<pinref part="G158" gate="G$1" pin="2"/>
<wire x1="248.92" y1="378.46" x2="246.38" y2="378.46" width="0.1524" layer="91"/>
<wire x1="248.92" y1="378.46" x2="248.92" y2="365.76" width="0.1524" layer="91"/>
<junction x="248.92" y="378.46"/>
<pinref part="G159" gate="G$1" pin="2"/>
<wire x1="248.92" y1="365.76" x2="246.38" y2="365.76" width="0.1524" layer="91"/>
<wire x1="248.92" y1="365.76" x2="248.92" y2="353.06" width="0.1524" layer="91"/>
<junction x="248.92" y="365.76"/>
<pinref part="G160" gate="G$1" pin="2"/>
<wire x1="248.92" y1="353.06" x2="246.38" y2="353.06" width="0.1524" layer="91"/>
<wire x1="248.92" y1="353.06" x2="248.92" y2="347.98" width="0.1524" layer="91"/>
<junction x="248.92" y="353.06"/>
<pinref part="GND37" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G161" gate="G$1" pin="2"/>
<wire x1="55.88" y1="327.66" x2="58.42" y2="327.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="327.66" x2="58.42" y2="314.96" width="0.1524" layer="91"/>
<pinref part="G162" gate="G$1" pin="2"/>
<wire x1="58.42" y1="314.96" x2="55.88" y2="314.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="314.96" x2="58.42" y2="302.26" width="0.1524" layer="91"/>
<junction x="58.42" y="314.96"/>
<pinref part="G163" gate="G$1" pin="2"/>
<wire x1="58.42" y1="302.26" x2="55.88" y2="302.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="302.26" x2="58.42" y2="289.56" width="0.1524" layer="91"/>
<junction x="58.42" y="302.26"/>
<pinref part="G164" gate="G$1" pin="2"/>
<wire x1="58.42" y1="289.56" x2="55.88" y2="289.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="289.56" x2="58.42" y2="276.86" width="0.1524" layer="91"/>
<junction x="58.42" y="289.56"/>
<pinref part="G165" gate="G$1" pin="2"/>
<wire x1="58.42" y1="276.86" x2="55.88" y2="276.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="276.86" x2="58.42" y2="264.16" width="0.1524" layer="91"/>
<junction x="58.42" y="276.86"/>
<pinref part="G166" gate="G$1" pin="2"/>
<wire x1="58.42" y1="264.16" x2="55.88" y2="264.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="264.16" x2="58.42" y2="251.46" width="0.1524" layer="91"/>
<junction x="58.42" y="264.16"/>
<pinref part="G167" gate="G$1" pin="2"/>
<wire x1="58.42" y1="251.46" x2="55.88" y2="251.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="251.46" x2="58.42" y2="238.76" width="0.1524" layer="91"/>
<junction x="58.42" y="251.46"/>
<pinref part="G168" gate="G$1" pin="2"/>
<wire x1="58.42" y1="238.76" x2="55.88" y2="238.76" width="0.1524" layer="91"/>
<wire x1="58.42" y1="238.76" x2="58.42" y2="233.68" width="0.1524" layer="91"/>
<junction x="58.42" y="238.76"/>
<pinref part="GND38" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G169" gate="G$1" pin="2"/>
<wire x1="119.38" y1="330.2" x2="121.92" y2="330.2" width="0.1524" layer="91"/>
<wire x1="121.92" y1="330.2" x2="121.92" y2="317.5" width="0.1524" layer="91"/>
<pinref part="G170" gate="G$1" pin="2"/>
<wire x1="121.92" y1="317.5" x2="119.38" y2="317.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="317.5" x2="121.92" y2="304.8" width="0.1524" layer="91"/>
<junction x="121.92" y="317.5"/>
<pinref part="G171" gate="G$1" pin="2"/>
<wire x1="121.92" y1="304.8" x2="119.38" y2="304.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="304.8" x2="121.92" y2="292.1" width="0.1524" layer="91"/>
<junction x="121.92" y="304.8"/>
<pinref part="G172" gate="G$1" pin="2"/>
<wire x1="121.92" y1="292.1" x2="119.38" y2="292.1" width="0.1524" layer="91"/>
<wire x1="121.92" y1="292.1" x2="121.92" y2="279.4" width="0.1524" layer="91"/>
<junction x="121.92" y="292.1"/>
<pinref part="G173" gate="G$1" pin="2"/>
<wire x1="121.92" y1="279.4" x2="119.38" y2="279.4" width="0.1524" layer="91"/>
<wire x1="121.92" y1="279.4" x2="121.92" y2="266.7" width="0.1524" layer="91"/>
<junction x="121.92" y="279.4"/>
<pinref part="G174" gate="G$1" pin="2"/>
<wire x1="121.92" y1="266.7" x2="119.38" y2="266.7" width="0.1524" layer="91"/>
<wire x1="121.92" y1="266.7" x2="121.92" y2="254" width="0.1524" layer="91"/>
<junction x="121.92" y="266.7"/>
<pinref part="G175" gate="G$1" pin="2"/>
<wire x1="121.92" y1="254" x2="119.38" y2="254" width="0.1524" layer="91"/>
<wire x1="121.92" y1="254" x2="121.92" y2="241.3" width="0.1524" layer="91"/>
<junction x="121.92" y="254"/>
<pinref part="G176" gate="G$1" pin="2"/>
<wire x1="121.92" y1="241.3" x2="119.38" y2="241.3" width="0.1524" layer="91"/>
<wire x1="121.92" y1="241.3" x2="121.92" y2="236.22" width="0.1524" layer="91"/>
<junction x="121.92" y="241.3"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G177" gate="G$1" pin="2"/>
<wire x1="182.88" y1="330.2" x2="185.42" y2="330.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="330.2" x2="185.42" y2="317.5" width="0.1524" layer="91"/>
<pinref part="G178" gate="G$1" pin="2"/>
<wire x1="185.42" y1="317.5" x2="182.88" y2="317.5" width="0.1524" layer="91"/>
<wire x1="185.42" y1="317.5" x2="185.42" y2="304.8" width="0.1524" layer="91"/>
<junction x="185.42" y="317.5"/>
<pinref part="G179" gate="G$1" pin="2"/>
<wire x1="185.42" y1="304.8" x2="182.88" y2="304.8" width="0.1524" layer="91"/>
<wire x1="185.42" y1="304.8" x2="185.42" y2="292.1" width="0.1524" layer="91"/>
<junction x="185.42" y="304.8"/>
<pinref part="G180" gate="G$1" pin="2"/>
<wire x1="185.42" y1="292.1" x2="182.88" y2="292.1" width="0.1524" layer="91"/>
<wire x1="185.42" y1="292.1" x2="185.42" y2="279.4" width="0.1524" layer="91"/>
<junction x="185.42" y="292.1"/>
<pinref part="G181" gate="G$1" pin="2"/>
<wire x1="185.42" y1="279.4" x2="182.88" y2="279.4" width="0.1524" layer="91"/>
<wire x1="185.42" y1="279.4" x2="185.42" y2="266.7" width="0.1524" layer="91"/>
<junction x="185.42" y="279.4"/>
<pinref part="G182" gate="G$1" pin="2"/>
<wire x1="185.42" y1="266.7" x2="182.88" y2="266.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="266.7" x2="185.42" y2="254" width="0.1524" layer="91"/>
<junction x="185.42" y="266.7"/>
<pinref part="G183" gate="G$1" pin="2"/>
<wire x1="185.42" y1="254" x2="182.88" y2="254" width="0.1524" layer="91"/>
<wire x1="185.42" y1="254" x2="185.42" y2="241.3" width="0.1524" layer="91"/>
<junction x="185.42" y="254"/>
<pinref part="G184" gate="G$1" pin="2"/>
<wire x1="185.42" y1="241.3" x2="182.88" y2="241.3" width="0.1524" layer="91"/>
<wire x1="185.42" y1="241.3" x2="185.42" y2="236.22" width="0.1524" layer="91"/>
<junction x="185.42" y="241.3"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G185" gate="G$1" pin="2"/>
<wire x1="246.38" y1="330.2" x2="248.92" y2="330.2" width="0.1524" layer="91"/>
<wire x1="248.92" y1="330.2" x2="248.92" y2="317.5" width="0.1524" layer="91"/>
<pinref part="G186" gate="G$1" pin="2"/>
<wire x1="248.92" y1="317.5" x2="246.38" y2="317.5" width="0.1524" layer="91"/>
<wire x1="248.92" y1="317.5" x2="248.92" y2="304.8" width="0.1524" layer="91"/>
<junction x="248.92" y="317.5"/>
<pinref part="G187" gate="G$1" pin="2"/>
<wire x1="248.92" y1="304.8" x2="246.38" y2="304.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="304.8" x2="248.92" y2="292.1" width="0.1524" layer="91"/>
<junction x="248.92" y="304.8"/>
<pinref part="G188" gate="G$1" pin="2"/>
<wire x1="248.92" y1="292.1" x2="246.38" y2="292.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="292.1" x2="248.92" y2="279.4" width="0.1524" layer="91"/>
<junction x="248.92" y="292.1"/>
<pinref part="G189" gate="G$1" pin="2"/>
<wire x1="248.92" y1="279.4" x2="246.38" y2="279.4" width="0.1524" layer="91"/>
<wire x1="248.92" y1="279.4" x2="248.92" y2="266.7" width="0.1524" layer="91"/>
<junction x="248.92" y="279.4"/>
<pinref part="G190" gate="G$1" pin="2"/>
<wire x1="248.92" y1="266.7" x2="246.38" y2="266.7" width="0.1524" layer="91"/>
<wire x1="248.92" y1="266.7" x2="248.92" y2="254" width="0.1524" layer="91"/>
<junction x="248.92" y="266.7"/>
<pinref part="G191" gate="G$1" pin="2"/>
<wire x1="248.92" y1="254" x2="246.38" y2="254" width="0.1524" layer="91"/>
<wire x1="248.92" y1="254" x2="248.92" y2="241.3" width="0.1524" layer="91"/>
<junction x="248.92" y="254"/>
<pinref part="G192" gate="G$1" pin="2"/>
<wire x1="248.92" y1="241.3" x2="246.38" y2="241.3" width="0.1524" layer="91"/>
<wire x1="248.92" y1="241.3" x2="248.92" y2="236.22" width="0.1524" layer="91"/>
<junction x="248.92" y="241.3"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="7.62" y1="406.4" x2="2.54" y2="406.4" width="0.1524" layer="91"/>
<wire x1="2.54" y1="406.4" x2="2.54" y2="403.86" width="0.1524" layer="91"/>
<wire x1="7.62" y1="403.86" x2="2.54" y2="403.86" width="0.1524" layer="91"/>
<junction x="2.54" y="403.86"/>
<wire x1="7.62" y1="401.32" x2="2.54" y2="401.32" width="0.1524" layer="91"/>
<wire x1="2.54" y1="401.32" x2="2.54" y2="403.86" width="0.1524" layer="91"/>
<junction x="2.54" y="401.32"/>
<wire x1="2.54" y1="401.32" x2="2.54" y2="398.78" width="0.1524" layer="91"/>
<wire x1="7.62" y1="398.78" x2="2.54" y2="398.78" width="0.1524" layer="91"/>
<junction x="2.54" y="398.78"/>
<wire x1="7.62" y1="396.24" x2="2.54" y2="396.24" width="0.1524" layer="91"/>
<wire x1="2.54" y1="396.24" x2="2.54" y2="398.78" width="0.1524" layer="91"/>
<junction x="2.54" y="396.24"/>
<wire x1="2.54" y1="396.24" x2="2.54" y2="393.7" width="0.1524" layer="91"/>
<wire x1="7.62" y1="393.7" x2="2.54" y2="393.7" width="0.1524" layer="91"/>
<junction x="2.54" y="393.7"/>
<wire x1="7.62" y1="391.16" x2="2.54" y2="391.16" width="0.1524" layer="91"/>
<wire x1="2.54" y1="391.16" x2="2.54" y2="393.7" width="0.1524" layer="91"/>
<junction x="2.54" y="391.16"/>
<wire x1="2.54" y1="391.16" x2="2.54" y2="388.62" width="0.1524" layer="91"/>
<wire x1="7.62" y1="388.62" x2="2.54" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="2"/>
<pinref part="SV17" gate="1" pin="4"/>
<pinref part="SV17" gate="1" pin="6"/>
<pinref part="SV17" gate="1" pin="8"/>
<pinref part="SV17" gate="1" pin="10"/>
<pinref part="SV17" gate="1" pin="12"/>
<pinref part="SV17" gate="1" pin="14"/>
<pinref part="SV17" gate="1" pin="16"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="2.54" y1="386.08" x2="2.54" y2="388.62" width="0.1524" layer="91"/>
<junction x="2.54" y="388.62"/>
</segment>
<segment>
<pinref part="SV21" gate="1" pin="4"/>
<wire x1="7.62" y1="289.56" x2="2.54" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="8"/>
<wire x1="7.62" y1="284.48" x2="2.54" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="12"/>
<wire x1="7.62" y1="279.4" x2="2.54" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="16"/>
<wire x1="7.62" y1="274.32" x2="2.54" y2="274.32" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="14"/>
<wire x1="7.62" y1="276.86" x2="2.54" y2="276.86" width="0.1524" layer="91"/>
<wire x1="2.54" y1="276.86" x2="2.54" y2="274.32" width="0.1524" layer="91"/>
<junction x="2.54" y="276.86"/>
<wire x1="2.54" y1="276.86" x2="2.54" y2="279.4" width="0.1524" layer="91"/>
<junction x="2.54" y="279.4"/>
<pinref part="SV21" gate="1" pin="10"/>
<wire x1="7.62" y1="281.94" x2="2.54" y2="281.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="281.94" x2="2.54" y2="279.4" width="0.1524" layer="91"/>
<junction x="2.54" y="281.94"/>
<wire x1="2.54" y1="281.94" x2="2.54" y2="284.48" width="0.1524" layer="91"/>
<junction x="2.54" y="284.48"/>
<pinref part="SV21" gate="1" pin="6"/>
<wire x1="7.62" y1="287.02" x2="2.54" y2="287.02" width="0.1524" layer="91"/>
<wire x1="2.54" y1="287.02" x2="2.54" y2="284.48" width="0.1524" layer="91"/>
<junction x="2.54" y="287.02"/>
<wire x1="2.54" y1="287.02" x2="2.54" y2="289.56" width="0.1524" layer="91"/>
<junction x="2.54" y="289.56"/>
<wire x1="2.54" y1="292.1" x2="2.54" y2="289.56" width="0.1524" layer="91"/>
<wire x1="7.62" y1="292.1" x2="2.54" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="2"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="2.54" y1="271.78" x2="2.54" y2="274.32" width="0.1524" layer="91"/>
<junction x="2.54" y="274.32"/>
</segment>
<segment>
<wire x1="71.12" y1="292.1" x2="66.04" y2="292.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="287.02" x2="66.04" y2="287.02" width="0.1524" layer="91"/>
<wire x1="71.12" y1="281.94" x2="66.04" y2="281.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="276.86" x2="66.04" y2="276.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="279.4" x2="66.04" y2="279.4" width="0.1524" layer="91"/>
<wire x1="66.04" y1="279.4" x2="66.04" y2="276.86" width="0.1524" layer="91"/>
<junction x="66.04" y="279.4"/>
<wire x1="66.04" y1="279.4" x2="66.04" y2="281.94" width="0.1524" layer="91"/>
<junction x="66.04" y="281.94"/>
<wire x1="71.12" y1="284.48" x2="66.04" y2="284.48" width="0.1524" layer="91"/>
<wire x1="66.04" y1="284.48" x2="66.04" y2="281.94" width="0.1524" layer="91"/>
<junction x="66.04" y="284.48"/>
<wire x1="66.04" y1="284.48" x2="66.04" y2="287.02" width="0.1524" layer="91"/>
<junction x="66.04" y="287.02"/>
<wire x1="71.12" y1="289.56" x2="66.04" y2="289.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="289.56" x2="66.04" y2="287.02" width="0.1524" layer="91"/>
<junction x="66.04" y="289.56"/>
<wire x1="66.04" y1="289.56" x2="66.04" y2="292.1" width="0.1524" layer="91"/>
<junction x="66.04" y="292.1"/>
<wire x1="71.12" y1="294.64" x2="66.04" y2="294.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="294.64" x2="66.04" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="2"/>
<pinref part="SV22" gate="1" pin="4"/>
<pinref part="SV22" gate="1" pin="6"/>
<pinref part="SV22" gate="1" pin="8"/>
<pinref part="SV22" gate="1" pin="10"/>
<pinref part="SV22" gate="1" pin="12"/>
<pinref part="SV22" gate="1" pin="14"/>
<pinref part="SV22" gate="1" pin="16"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="66.04" y1="274.32" x2="66.04" y2="276.86" width="0.1524" layer="91"/>
<junction x="66.04" y="276.86"/>
</segment>
<segment>
<wire x1="71.12" y1="406.4" x2="66.04" y2="406.4" width="0.1524" layer="91"/>
<wire x1="66.04" y1="406.4" x2="66.04" y2="403.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="403.86" x2="66.04" y2="403.86" width="0.1524" layer="91"/>
<junction x="66.04" y="403.86"/>
<wire x1="71.12" y1="401.32" x2="66.04" y2="401.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="401.32" x2="66.04" y2="403.86" width="0.1524" layer="91"/>
<junction x="66.04" y="401.32"/>
<wire x1="66.04" y1="401.32" x2="66.04" y2="398.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="398.78" x2="66.04" y2="398.78" width="0.1524" layer="91"/>
<junction x="66.04" y="398.78"/>
<wire x1="71.12" y1="396.24" x2="66.04" y2="396.24" width="0.1524" layer="91"/>
<wire x1="66.04" y1="396.24" x2="66.04" y2="398.78" width="0.1524" layer="91"/>
<junction x="66.04" y="396.24"/>
<wire x1="66.04" y1="396.24" x2="66.04" y2="393.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="393.7" x2="66.04" y2="393.7" width="0.1524" layer="91"/>
<junction x="66.04" y="393.7"/>
<wire x1="71.12" y1="391.16" x2="66.04" y2="391.16" width="0.1524" layer="91"/>
<wire x1="66.04" y1="391.16" x2="66.04" y2="393.7" width="0.1524" layer="91"/>
<junction x="66.04" y="391.16"/>
<wire x1="66.04" y1="391.16" x2="66.04" y2="388.62" width="0.1524" layer="91"/>
<wire x1="71.12" y1="388.62" x2="66.04" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="2"/>
<pinref part="SV18" gate="1" pin="4"/>
<pinref part="SV18" gate="1" pin="6"/>
<pinref part="SV18" gate="1" pin="8"/>
<pinref part="SV18" gate="1" pin="10"/>
<pinref part="SV18" gate="1" pin="12"/>
<pinref part="SV18" gate="1" pin="14"/>
<pinref part="SV18" gate="1" pin="16"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="66.04" y1="386.08" x2="66.04" y2="388.62" width="0.1524" layer="91"/>
<junction x="66.04" y="388.62"/>
</segment>
<segment>
<wire x1="134.62" y1="406.4" x2="129.54" y2="406.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="406.4" x2="129.54" y2="403.86" width="0.1524" layer="91"/>
<wire x1="134.62" y1="403.86" x2="129.54" y2="403.86" width="0.1524" layer="91"/>
<junction x="129.54" y="403.86"/>
<wire x1="134.62" y1="401.32" x2="129.54" y2="401.32" width="0.1524" layer="91"/>
<wire x1="129.54" y1="401.32" x2="129.54" y2="403.86" width="0.1524" layer="91"/>
<junction x="129.54" y="401.32"/>
<wire x1="129.54" y1="401.32" x2="129.54" y2="398.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="398.78" x2="129.54" y2="398.78" width="0.1524" layer="91"/>
<junction x="129.54" y="398.78"/>
<wire x1="134.62" y1="396.24" x2="129.54" y2="396.24" width="0.1524" layer="91"/>
<wire x1="129.54" y1="396.24" x2="129.54" y2="398.78" width="0.1524" layer="91"/>
<junction x="129.54" y="396.24"/>
<wire x1="129.54" y1="396.24" x2="129.54" y2="393.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="393.7" x2="129.54" y2="393.7" width="0.1524" layer="91"/>
<junction x="129.54" y="393.7"/>
<wire x1="134.62" y1="391.16" x2="129.54" y2="391.16" width="0.1524" layer="91"/>
<wire x1="129.54" y1="391.16" x2="129.54" y2="393.7" width="0.1524" layer="91"/>
<junction x="129.54" y="391.16"/>
<wire x1="129.54" y1="391.16" x2="129.54" y2="388.62" width="0.1524" layer="91"/>
<wire x1="134.62" y1="388.62" x2="129.54" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="2"/>
<pinref part="SV19" gate="1" pin="4"/>
<pinref part="SV19" gate="1" pin="6"/>
<pinref part="SV19" gate="1" pin="8"/>
<pinref part="SV19" gate="1" pin="10"/>
<pinref part="SV19" gate="1" pin="12"/>
<pinref part="SV19" gate="1" pin="14"/>
<pinref part="SV19" gate="1" pin="16"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="129.54" y1="386.08" x2="129.54" y2="388.62" width="0.1524" layer="91"/>
<junction x="129.54" y="388.62"/>
</segment>
<segment>
<pinref part="SV23" gate="1" pin="4"/>
<wire x1="134.62" y1="292.1" x2="129.54" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="8"/>
<wire x1="134.62" y1="287.02" x2="129.54" y2="287.02" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="12"/>
<wire x1="134.62" y1="281.94" x2="129.54" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="16"/>
<wire x1="134.62" y1="276.86" x2="129.54" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="14"/>
<wire x1="134.62" y1="279.4" x2="129.54" y2="279.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="279.4" x2="129.54" y2="276.86" width="0.1524" layer="91"/>
<junction x="129.54" y="279.4"/>
<wire x1="129.54" y1="279.4" x2="129.54" y2="281.94" width="0.1524" layer="91"/>
<junction x="129.54" y="281.94"/>
<pinref part="SV23" gate="1" pin="10"/>
<wire x1="134.62" y1="284.48" x2="129.54" y2="284.48" width="0.1524" layer="91"/>
<wire x1="129.54" y1="284.48" x2="129.54" y2="281.94" width="0.1524" layer="91"/>
<junction x="129.54" y="284.48"/>
<wire x1="129.54" y1="284.48" x2="129.54" y2="287.02" width="0.1524" layer="91"/>
<junction x="129.54" y="287.02"/>
<pinref part="SV23" gate="1" pin="6"/>
<wire x1="134.62" y1="289.56" x2="129.54" y2="289.56" width="0.1524" layer="91"/>
<wire x1="129.54" y1="289.56" x2="129.54" y2="287.02" width="0.1524" layer="91"/>
<junction x="129.54" y="289.56"/>
<wire x1="129.54" y1="289.56" x2="129.54" y2="292.1" width="0.1524" layer="91"/>
<junction x="129.54" y="292.1"/>
<wire x1="129.54" y1="294.64" x2="129.54" y2="292.1" width="0.1524" layer="91"/>
<wire x1="134.62" y1="294.64" x2="129.54" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="2"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="129.54" y1="274.32" x2="129.54" y2="276.86" width="0.1524" layer="91"/>
<junction x="129.54" y="276.86"/>
</segment>
<segment>
<wire x1="198.12" y1="406.4" x2="193.04" y2="406.4" width="0.1524" layer="91"/>
<wire x1="193.04" y1="406.4" x2="193.04" y2="403.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="403.86" x2="193.04" y2="403.86" width="0.1524" layer="91"/>
<junction x="193.04" y="403.86"/>
<wire x1="198.12" y1="401.32" x2="193.04" y2="401.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="401.32" x2="193.04" y2="403.86" width="0.1524" layer="91"/>
<junction x="193.04" y="401.32"/>
<wire x1="193.04" y1="401.32" x2="193.04" y2="398.78" width="0.1524" layer="91"/>
<wire x1="198.12" y1="398.78" x2="193.04" y2="398.78" width="0.1524" layer="91"/>
<junction x="193.04" y="398.78"/>
<wire x1="198.12" y1="396.24" x2="193.04" y2="396.24" width="0.1524" layer="91"/>
<wire x1="193.04" y1="396.24" x2="193.04" y2="398.78" width="0.1524" layer="91"/>
<junction x="193.04" y="396.24"/>
<wire x1="193.04" y1="396.24" x2="193.04" y2="393.7" width="0.1524" layer="91"/>
<wire x1="198.12" y1="393.7" x2="193.04" y2="393.7" width="0.1524" layer="91"/>
<junction x="193.04" y="393.7"/>
<wire x1="198.12" y1="388.62" x2="193.04" y2="388.62" width="0.1524" layer="91"/>
<wire x1="198.12" y1="391.16" x2="193.04" y2="391.16" width="0.1524" layer="91"/>
<wire x1="193.04" y1="391.16" x2="193.04" y2="388.62" width="0.1524" layer="91"/>
<wire x1="193.04" y1="391.16" x2="193.04" y2="393.7" width="0.1524" layer="91"/>
<junction x="193.04" y="391.16"/>
<pinref part="SV20" gate="1" pin="2"/>
<pinref part="SV20" gate="1" pin="4"/>
<pinref part="SV20" gate="1" pin="6"/>
<pinref part="SV20" gate="1" pin="8"/>
<pinref part="SV20" gate="1" pin="10"/>
<pinref part="SV20" gate="1" pin="12"/>
<pinref part="SV20" gate="1" pin="14"/>
<pinref part="SV20" gate="1" pin="16"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="193.04" y1="386.08" x2="193.04" y2="388.62" width="0.1524" layer="91"/>
<junction x="193.04" y="388.62"/>
</segment>
<segment>
<wire x1="454.66" y1="281.94" x2="449.58" y2="281.94" width="0.1524" layer="91"/>
<wire x1="449.58" y1="281.94" x2="449.58" y2="279.4" width="0.1524" layer="91"/>
<wire x1="454.66" y1="279.4" x2="449.58" y2="279.4" width="0.1524" layer="91"/>
<wire x1="454.66" y1="287.02" x2="449.58" y2="287.02" width="0.1524" layer="91"/>
<wire x1="449.58" y1="287.02" x2="449.58" y2="284.48" width="0.1524" layer="91"/>
<wire x1="454.66" y1="284.48" x2="449.58" y2="284.48" width="0.1524" layer="91"/>
<wire x1="454.66" y1="292.1" x2="449.58" y2="292.1" width="0.1524" layer="91"/>
<wire x1="449.58" y1="292.1" x2="449.58" y2="289.56" width="0.1524" layer="91"/>
<wire x1="454.66" y1="289.56" x2="449.58" y2="289.56" width="0.1524" layer="91"/>
<wire x1="454.66" y1="297.18" x2="449.58" y2="297.18" width="0.1524" layer="91"/>
<wire x1="449.58" y1="297.18" x2="449.58" y2="294.64" width="0.1524" layer="91"/>
<wire x1="454.66" y1="294.64" x2="449.58" y2="294.64" width="0.1524" layer="91"/>
<wire x1="449.58" y1="292.1" x2="449.58" y2="294.64" width="0.1524" layer="91"/>
<junction x="449.58" y="292.1"/>
<junction x="449.58" y="294.64"/>
<wire x1="449.58" y1="287.02" x2="449.58" y2="289.56" width="0.1524" layer="91"/>
<junction x="449.58" y="287.02"/>
<junction x="449.58" y="289.56"/>
<wire x1="449.58" y1="281.94" x2="449.58" y2="284.48" width="0.1524" layer="91"/>
<junction x="449.58" y="281.94"/>
<junction x="449.58" y="284.48"/>
<wire x1="449.58" y1="279.4" x2="449.58" y2="276.86" width="0.1524" layer="91"/>
<junction x="449.58" y="279.4"/>
<pinref part="GND65" gate="1" pin="GND"/>
<pinref part="SV40" gate="1" pin="2"/>
<pinref part="SV40" gate="1" pin="4"/>
<pinref part="SV40" gate="1" pin="6"/>
<pinref part="SV40" gate="1" pin="8"/>
<pinref part="SV40" gate="1" pin="10"/>
<pinref part="SV40" gate="1" pin="12"/>
<pinref part="SV40" gate="1" pin="14"/>
<pinref part="SV40" gate="1" pin="16"/>
</segment>
<segment>
<pinref part="G257" gate="G$1" pin="2"/>
<wire x1="312.42" y1="444.5" x2="314.96" y2="444.5" width="0.1524" layer="91"/>
<wire x1="314.96" y1="444.5" x2="314.96" y2="431.8" width="0.1524" layer="91"/>
<pinref part="G258" gate="G$1" pin="2"/>
<wire x1="314.96" y1="431.8" x2="312.42" y2="431.8" width="0.1524" layer="91"/>
<wire x1="314.96" y1="431.8" x2="314.96" y2="419.1" width="0.1524" layer="91"/>
<junction x="314.96" y="431.8"/>
<pinref part="G259" gate="G$1" pin="2"/>
<wire x1="314.96" y1="419.1" x2="312.42" y2="419.1" width="0.1524" layer="91"/>
<wire x1="314.96" y1="419.1" x2="314.96" y2="406.4" width="0.1524" layer="91"/>
<junction x="314.96" y="419.1"/>
<pinref part="G260" gate="G$1" pin="2"/>
<wire x1="314.96" y1="406.4" x2="312.42" y2="406.4" width="0.1524" layer="91"/>
<wire x1="314.96" y1="406.4" x2="314.96" y2="393.7" width="0.1524" layer="91"/>
<junction x="314.96" y="406.4"/>
<pinref part="G261" gate="G$1" pin="2"/>
<wire x1="314.96" y1="393.7" x2="312.42" y2="393.7" width="0.1524" layer="91"/>
<wire x1="314.96" y1="393.7" x2="314.96" y2="381" width="0.1524" layer="91"/>
<junction x="314.96" y="393.7"/>
<pinref part="G262" gate="G$1" pin="2"/>
<wire x1="314.96" y1="381" x2="312.42" y2="381" width="0.1524" layer="91"/>
<wire x1="314.96" y1="381" x2="314.96" y2="368.3" width="0.1524" layer="91"/>
<junction x="314.96" y="381"/>
<pinref part="G263" gate="G$1" pin="2"/>
<wire x1="314.96" y1="368.3" x2="312.42" y2="368.3" width="0.1524" layer="91"/>
<wire x1="314.96" y1="368.3" x2="314.96" y2="355.6" width="0.1524" layer="91"/>
<junction x="314.96" y="368.3"/>
<pinref part="G264" gate="G$1" pin="2"/>
<wire x1="314.96" y1="355.6" x2="312.42" y2="355.6" width="0.1524" layer="91"/>
<wire x1="314.96" y1="355.6" x2="314.96" y2="350.52" width="0.1524" layer="91"/>
<junction x="314.96" y="355.6"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G265" gate="G$1" pin="2"/>
<wire x1="375.92" y1="444.5" x2="378.46" y2="444.5" width="0.1524" layer="91"/>
<wire x1="378.46" y1="444.5" x2="378.46" y2="431.8" width="0.1524" layer="91"/>
<pinref part="G266" gate="G$1" pin="2"/>
<wire x1="378.46" y1="431.8" x2="375.92" y2="431.8" width="0.1524" layer="91"/>
<wire x1="378.46" y1="431.8" x2="378.46" y2="419.1" width="0.1524" layer="91"/>
<junction x="378.46" y="431.8"/>
<pinref part="G267" gate="G$1" pin="2"/>
<wire x1="378.46" y1="419.1" x2="375.92" y2="419.1" width="0.1524" layer="91"/>
<wire x1="378.46" y1="419.1" x2="378.46" y2="406.4" width="0.1524" layer="91"/>
<junction x="378.46" y="419.1"/>
<pinref part="G268" gate="G$1" pin="2"/>
<wire x1="378.46" y1="406.4" x2="375.92" y2="406.4" width="0.1524" layer="91"/>
<wire x1="378.46" y1="406.4" x2="378.46" y2="393.7" width="0.1524" layer="91"/>
<junction x="378.46" y="406.4"/>
<pinref part="G269" gate="G$1" pin="2"/>
<wire x1="378.46" y1="393.7" x2="375.92" y2="393.7" width="0.1524" layer="91"/>
<wire x1="378.46" y1="393.7" x2="378.46" y2="381" width="0.1524" layer="91"/>
<junction x="378.46" y="393.7"/>
<pinref part="G270" gate="G$1" pin="2"/>
<wire x1="378.46" y1="381" x2="375.92" y2="381" width="0.1524" layer="91"/>
<wire x1="378.46" y1="381" x2="378.46" y2="368.3" width="0.1524" layer="91"/>
<junction x="378.46" y="381"/>
<pinref part="G271" gate="G$1" pin="2"/>
<wire x1="378.46" y1="368.3" x2="375.92" y2="368.3" width="0.1524" layer="91"/>
<wire x1="378.46" y1="368.3" x2="378.46" y2="355.6" width="0.1524" layer="91"/>
<junction x="378.46" y="368.3"/>
<pinref part="G272" gate="G$1" pin="2"/>
<wire x1="378.46" y1="355.6" x2="375.92" y2="355.6" width="0.1524" layer="91"/>
<wire x1="378.46" y1="355.6" x2="378.46" y2="350.52" width="0.1524" layer="91"/>
<junction x="378.46" y="355.6"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G273" gate="G$1" pin="2"/>
<wire x1="439.42" y1="444.5" x2="441.96" y2="444.5" width="0.1524" layer="91"/>
<wire x1="441.96" y1="444.5" x2="441.96" y2="431.8" width="0.1524" layer="91"/>
<pinref part="G274" gate="G$1" pin="2"/>
<wire x1="441.96" y1="431.8" x2="439.42" y2="431.8" width="0.1524" layer="91"/>
<wire x1="441.96" y1="431.8" x2="441.96" y2="419.1" width="0.1524" layer="91"/>
<junction x="441.96" y="431.8"/>
<pinref part="G275" gate="G$1" pin="2"/>
<wire x1="441.96" y1="419.1" x2="439.42" y2="419.1" width="0.1524" layer="91"/>
<wire x1="441.96" y1="419.1" x2="441.96" y2="406.4" width="0.1524" layer="91"/>
<junction x="441.96" y="419.1"/>
<pinref part="G276" gate="G$1" pin="2"/>
<wire x1="441.96" y1="406.4" x2="439.42" y2="406.4" width="0.1524" layer="91"/>
<wire x1="441.96" y1="406.4" x2="441.96" y2="393.7" width="0.1524" layer="91"/>
<junction x="441.96" y="406.4"/>
<pinref part="G277" gate="G$1" pin="2"/>
<wire x1="441.96" y1="393.7" x2="439.42" y2="393.7" width="0.1524" layer="91"/>
<wire x1="441.96" y1="393.7" x2="441.96" y2="381" width="0.1524" layer="91"/>
<junction x="441.96" y="393.7"/>
<pinref part="G278" gate="G$1" pin="2"/>
<wire x1="441.96" y1="381" x2="439.42" y2="381" width="0.1524" layer="91"/>
<wire x1="441.96" y1="381" x2="441.96" y2="368.3" width="0.1524" layer="91"/>
<junction x="441.96" y="381"/>
<pinref part="G279" gate="G$1" pin="2"/>
<wire x1="441.96" y1="368.3" x2="439.42" y2="368.3" width="0.1524" layer="91"/>
<wire x1="441.96" y1="368.3" x2="441.96" y2="355.6" width="0.1524" layer="91"/>
<junction x="441.96" y="368.3"/>
<pinref part="G280" gate="G$1" pin="2"/>
<wire x1="441.96" y1="355.6" x2="439.42" y2="355.6" width="0.1524" layer="91"/>
<wire x1="441.96" y1="355.6" x2="441.96" y2="350.52" width="0.1524" layer="91"/>
<junction x="441.96" y="355.6"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G281" gate="G$1" pin="2"/>
<wire x1="502.92" y1="444.5" x2="505.46" y2="444.5" width="0.1524" layer="91"/>
<wire x1="505.46" y1="444.5" x2="505.46" y2="431.8" width="0.1524" layer="91"/>
<pinref part="G282" gate="G$1" pin="2"/>
<wire x1="505.46" y1="431.8" x2="502.92" y2="431.8" width="0.1524" layer="91"/>
<wire x1="505.46" y1="431.8" x2="505.46" y2="419.1" width="0.1524" layer="91"/>
<junction x="505.46" y="431.8"/>
<pinref part="G283" gate="G$1" pin="2"/>
<wire x1="505.46" y1="419.1" x2="502.92" y2="419.1" width="0.1524" layer="91"/>
<wire x1="505.46" y1="419.1" x2="505.46" y2="406.4" width="0.1524" layer="91"/>
<junction x="505.46" y="419.1"/>
<pinref part="G284" gate="G$1" pin="2"/>
<wire x1="505.46" y1="406.4" x2="502.92" y2="406.4" width="0.1524" layer="91"/>
<wire x1="505.46" y1="406.4" x2="505.46" y2="393.7" width="0.1524" layer="91"/>
<junction x="505.46" y="406.4"/>
<pinref part="G285" gate="G$1" pin="2"/>
<wire x1="505.46" y1="393.7" x2="502.92" y2="393.7" width="0.1524" layer="91"/>
<wire x1="505.46" y1="393.7" x2="505.46" y2="381" width="0.1524" layer="91"/>
<junction x="505.46" y="393.7"/>
<pinref part="G286" gate="G$1" pin="2"/>
<wire x1="505.46" y1="381" x2="502.92" y2="381" width="0.1524" layer="91"/>
<wire x1="505.46" y1="381" x2="505.46" y2="368.3" width="0.1524" layer="91"/>
<junction x="505.46" y="381"/>
<pinref part="G287" gate="G$1" pin="2"/>
<wire x1="505.46" y1="368.3" x2="502.92" y2="368.3" width="0.1524" layer="91"/>
<wire x1="505.46" y1="368.3" x2="505.46" y2="355.6" width="0.1524" layer="91"/>
<junction x="505.46" y="368.3"/>
<pinref part="G288" gate="G$1" pin="2"/>
<wire x1="505.46" y1="355.6" x2="502.92" y2="355.6" width="0.1524" layer="91"/>
<wire x1="505.46" y1="355.6" x2="505.46" y2="350.52" width="0.1524" layer="91"/>
<junction x="505.46" y="355.6"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G289" gate="G$1" pin="2"/>
<wire x1="312.42" y1="330.2" x2="314.96" y2="330.2" width="0.1524" layer="91"/>
<wire x1="314.96" y1="330.2" x2="314.96" y2="317.5" width="0.1524" layer="91"/>
<pinref part="G290" gate="G$1" pin="2"/>
<wire x1="314.96" y1="317.5" x2="312.42" y2="317.5" width="0.1524" layer="91"/>
<wire x1="314.96" y1="317.5" x2="314.96" y2="304.8" width="0.1524" layer="91"/>
<junction x="314.96" y="317.5"/>
<pinref part="G291" gate="G$1" pin="2"/>
<wire x1="314.96" y1="304.8" x2="312.42" y2="304.8" width="0.1524" layer="91"/>
<wire x1="314.96" y1="304.8" x2="314.96" y2="292.1" width="0.1524" layer="91"/>
<junction x="314.96" y="304.8"/>
<pinref part="G292" gate="G$1" pin="2"/>
<wire x1="314.96" y1="292.1" x2="312.42" y2="292.1" width="0.1524" layer="91"/>
<wire x1="314.96" y1="292.1" x2="314.96" y2="279.4" width="0.1524" layer="91"/>
<junction x="314.96" y="292.1"/>
<pinref part="G293" gate="G$1" pin="2"/>
<wire x1="314.96" y1="279.4" x2="312.42" y2="279.4" width="0.1524" layer="91"/>
<wire x1="314.96" y1="279.4" x2="314.96" y2="266.7" width="0.1524" layer="91"/>
<junction x="314.96" y="279.4"/>
<pinref part="G294" gate="G$1" pin="2"/>
<wire x1="314.96" y1="266.7" x2="312.42" y2="266.7" width="0.1524" layer="91"/>
<wire x1="314.96" y1="266.7" x2="314.96" y2="254" width="0.1524" layer="91"/>
<junction x="314.96" y="266.7"/>
<pinref part="G295" gate="G$1" pin="2"/>
<wire x1="314.96" y1="254" x2="312.42" y2="254" width="0.1524" layer="91"/>
<wire x1="314.96" y1="254" x2="314.96" y2="241.3" width="0.1524" layer="91"/>
<junction x="314.96" y="254"/>
<pinref part="G296" gate="G$1" pin="2"/>
<wire x1="314.96" y1="241.3" x2="312.42" y2="241.3" width="0.1524" layer="91"/>
<wire x1="314.96" y1="241.3" x2="314.96" y2="236.22" width="0.1524" layer="91"/>
<junction x="314.96" y="241.3"/>
<pinref part="GND70" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G297" gate="G$1" pin="2"/>
<wire x1="375.92" y1="332.74" x2="378.46" y2="332.74" width="0.1524" layer="91"/>
<wire x1="378.46" y1="332.74" x2="378.46" y2="320.04" width="0.1524" layer="91"/>
<pinref part="G298" gate="G$1" pin="2"/>
<wire x1="378.46" y1="320.04" x2="375.92" y2="320.04" width="0.1524" layer="91"/>
<wire x1="378.46" y1="320.04" x2="378.46" y2="307.34" width="0.1524" layer="91"/>
<junction x="378.46" y="320.04"/>
<pinref part="G299" gate="G$1" pin="2"/>
<wire x1="378.46" y1="307.34" x2="375.92" y2="307.34" width="0.1524" layer="91"/>
<wire x1="378.46" y1="307.34" x2="378.46" y2="294.64" width="0.1524" layer="91"/>
<junction x="378.46" y="307.34"/>
<pinref part="G300" gate="G$1" pin="2"/>
<wire x1="378.46" y1="294.64" x2="375.92" y2="294.64" width="0.1524" layer="91"/>
<wire x1="378.46" y1="294.64" x2="378.46" y2="281.94" width="0.1524" layer="91"/>
<junction x="378.46" y="294.64"/>
<pinref part="G301" gate="G$1" pin="2"/>
<wire x1="378.46" y1="281.94" x2="375.92" y2="281.94" width="0.1524" layer="91"/>
<wire x1="378.46" y1="281.94" x2="378.46" y2="269.24" width="0.1524" layer="91"/>
<junction x="378.46" y="281.94"/>
<pinref part="G302" gate="G$1" pin="2"/>
<wire x1="378.46" y1="269.24" x2="375.92" y2="269.24" width="0.1524" layer="91"/>
<wire x1="378.46" y1="269.24" x2="378.46" y2="256.54" width="0.1524" layer="91"/>
<junction x="378.46" y="269.24"/>
<pinref part="G303" gate="G$1" pin="2"/>
<wire x1="378.46" y1="256.54" x2="375.92" y2="256.54" width="0.1524" layer="91"/>
<wire x1="378.46" y1="256.54" x2="378.46" y2="243.84" width="0.1524" layer="91"/>
<junction x="378.46" y="256.54"/>
<pinref part="G304" gate="G$1" pin="2"/>
<wire x1="378.46" y1="243.84" x2="375.92" y2="243.84" width="0.1524" layer="91"/>
<wire x1="378.46" y1="243.84" x2="378.46" y2="238.76" width="0.1524" layer="91"/>
<junction x="378.46" y="243.84"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G305" gate="G$1" pin="2"/>
<wire x1="439.42" y1="332.74" x2="441.96" y2="332.74" width="0.1524" layer="91"/>
<wire x1="441.96" y1="332.74" x2="441.96" y2="320.04" width="0.1524" layer="91"/>
<pinref part="G306" gate="G$1" pin="2"/>
<wire x1="441.96" y1="320.04" x2="439.42" y2="320.04" width="0.1524" layer="91"/>
<wire x1="441.96" y1="320.04" x2="441.96" y2="307.34" width="0.1524" layer="91"/>
<junction x="441.96" y="320.04"/>
<pinref part="G307" gate="G$1" pin="2"/>
<wire x1="441.96" y1="307.34" x2="439.42" y2="307.34" width="0.1524" layer="91"/>
<wire x1="441.96" y1="307.34" x2="441.96" y2="294.64" width="0.1524" layer="91"/>
<junction x="441.96" y="307.34"/>
<pinref part="G308" gate="G$1" pin="2"/>
<wire x1="441.96" y1="294.64" x2="439.42" y2="294.64" width="0.1524" layer="91"/>
<wire x1="441.96" y1="294.64" x2="441.96" y2="281.94" width="0.1524" layer="91"/>
<junction x="441.96" y="294.64"/>
<pinref part="G309" gate="G$1" pin="2"/>
<wire x1="441.96" y1="281.94" x2="439.42" y2="281.94" width="0.1524" layer="91"/>
<wire x1="441.96" y1="281.94" x2="441.96" y2="269.24" width="0.1524" layer="91"/>
<junction x="441.96" y="281.94"/>
<pinref part="G310" gate="G$1" pin="2"/>
<wire x1="441.96" y1="269.24" x2="439.42" y2="269.24" width="0.1524" layer="91"/>
<wire x1="441.96" y1="269.24" x2="441.96" y2="256.54" width="0.1524" layer="91"/>
<junction x="441.96" y="269.24"/>
<pinref part="G311" gate="G$1" pin="2"/>
<wire x1="441.96" y1="256.54" x2="439.42" y2="256.54" width="0.1524" layer="91"/>
<wire x1="441.96" y1="256.54" x2="441.96" y2="243.84" width="0.1524" layer="91"/>
<junction x="441.96" y="256.54"/>
<pinref part="G312" gate="G$1" pin="2"/>
<wire x1="441.96" y1="243.84" x2="439.42" y2="243.84" width="0.1524" layer="91"/>
<wire x1="441.96" y1="243.84" x2="441.96" y2="238.76" width="0.1524" layer="91"/>
<junction x="441.96" y="243.84"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="G313" gate="G$1" pin="2"/>
<wire x1="502.92" y1="332.74" x2="505.46" y2="332.74" width="0.1524" layer="91"/>
<wire x1="505.46" y1="332.74" x2="505.46" y2="320.04" width="0.1524" layer="91"/>
<pinref part="G314" gate="G$1" pin="2"/>
<wire x1="505.46" y1="320.04" x2="502.92" y2="320.04" width="0.1524" layer="91"/>
<wire x1="505.46" y1="320.04" x2="505.46" y2="307.34" width="0.1524" layer="91"/>
<junction x="505.46" y="320.04"/>
<pinref part="G315" gate="G$1" pin="2"/>
<wire x1="505.46" y1="307.34" x2="502.92" y2="307.34" width="0.1524" layer="91"/>
<wire x1="505.46" y1="307.34" x2="505.46" y2="294.64" width="0.1524" layer="91"/>
<junction x="505.46" y="307.34"/>
<pinref part="G316" gate="G$1" pin="2"/>
<wire x1="505.46" y1="294.64" x2="502.92" y2="294.64" width="0.1524" layer="91"/>
<wire x1="505.46" y1="294.64" x2="505.46" y2="281.94" width="0.1524" layer="91"/>
<junction x="505.46" y="294.64"/>
<pinref part="G317" gate="G$1" pin="2"/>
<wire x1="505.46" y1="281.94" x2="502.92" y2="281.94" width="0.1524" layer="91"/>
<wire x1="505.46" y1="281.94" x2="505.46" y2="269.24" width="0.1524" layer="91"/>
<junction x="505.46" y="281.94"/>
<pinref part="G318" gate="G$1" pin="2"/>
<wire x1="505.46" y1="269.24" x2="502.92" y2="269.24" width="0.1524" layer="91"/>
<wire x1="505.46" y1="269.24" x2="505.46" y2="256.54" width="0.1524" layer="91"/>
<junction x="505.46" y="269.24"/>
<pinref part="G319" gate="G$1" pin="2"/>
<wire x1="505.46" y1="256.54" x2="502.92" y2="256.54" width="0.1524" layer="91"/>
<wire x1="505.46" y1="256.54" x2="505.46" y2="243.84" width="0.1524" layer="91"/>
<junction x="505.46" y="256.54"/>
<pinref part="G320" gate="G$1" pin="2"/>
<wire x1="505.46" y1="243.84" x2="502.92" y2="243.84" width="0.1524" layer="91"/>
<wire x1="505.46" y1="243.84" x2="505.46" y2="238.76" width="0.1524" layer="91"/>
<junction x="505.46" y="243.84"/>
<pinref part="GND73" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="264.16" y1="408.94" x2="259.08" y2="408.94" width="0.1524" layer="91"/>
<wire x1="259.08" y1="408.94" x2="259.08" y2="406.4" width="0.1524" layer="91"/>
<wire x1="264.16" y1="406.4" x2="259.08" y2="406.4" width="0.1524" layer="91"/>
<junction x="259.08" y="406.4"/>
<wire x1="264.16" y1="403.86" x2="259.08" y2="403.86" width="0.1524" layer="91"/>
<wire x1="259.08" y1="403.86" x2="259.08" y2="406.4" width="0.1524" layer="91"/>
<junction x="259.08" y="403.86"/>
<wire x1="259.08" y1="403.86" x2="259.08" y2="401.32" width="0.1524" layer="91"/>
<wire x1="264.16" y1="401.32" x2="259.08" y2="401.32" width="0.1524" layer="91"/>
<junction x="259.08" y="401.32"/>
<wire x1="264.16" y1="398.78" x2="259.08" y2="398.78" width="0.1524" layer="91"/>
<wire x1="259.08" y1="398.78" x2="259.08" y2="401.32" width="0.1524" layer="91"/>
<junction x="259.08" y="398.78"/>
<wire x1="259.08" y1="398.78" x2="259.08" y2="396.24" width="0.1524" layer="91"/>
<wire x1="264.16" y1="396.24" x2="259.08" y2="396.24" width="0.1524" layer="91"/>
<junction x="259.08" y="396.24"/>
<wire x1="264.16" y1="393.7" x2="259.08" y2="393.7" width="0.1524" layer="91"/>
<wire x1="259.08" y1="393.7" x2="259.08" y2="396.24" width="0.1524" layer="91"/>
<junction x="259.08" y="393.7"/>
<wire x1="259.08" y1="393.7" x2="259.08" y2="391.16" width="0.1524" layer="91"/>
<wire x1="264.16" y1="391.16" x2="259.08" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="2"/>
<pinref part="SV33" gate="1" pin="4"/>
<pinref part="SV33" gate="1" pin="6"/>
<pinref part="SV33" gate="1" pin="8"/>
<pinref part="SV33" gate="1" pin="10"/>
<pinref part="SV33" gate="1" pin="12"/>
<pinref part="SV33" gate="1" pin="14"/>
<pinref part="SV33" gate="1" pin="16"/>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="259.08" y1="388.62" x2="259.08" y2="391.16" width="0.1524" layer="91"/>
<junction x="259.08" y="391.16"/>
</segment>
<segment>
<pinref part="SV37" gate="1" pin="4"/>
<wire x1="264.16" y1="292.1" x2="259.08" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="8"/>
<wire x1="264.16" y1="287.02" x2="259.08" y2="287.02" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="12"/>
<wire x1="264.16" y1="281.94" x2="259.08" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="16"/>
<wire x1="264.16" y1="276.86" x2="259.08" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="14"/>
<wire x1="264.16" y1="279.4" x2="259.08" y2="279.4" width="0.1524" layer="91"/>
<wire x1="259.08" y1="279.4" x2="259.08" y2="276.86" width="0.1524" layer="91"/>
<junction x="259.08" y="279.4"/>
<wire x1="259.08" y1="279.4" x2="259.08" y2="281.94" width="0.1524" layer="91"/>
<junction x="259.08" y="281.94"/>
<pinref part="SV37" gate="1" pin="10"/>
<wire x1="264.16" y1="284.48" x2="259.08" y2="284.48" width="0.1524" layer="91"/>
<wire x1="259.08" y1="284.48" x2="259.08" y2="281.94" width="0.1524" layer="91"/>
<junction x="259.08" y="284.48"/>
<wire x1="259.08" y1="284.48" x2="259.08" y2="287.02" width="0.1524" layer="91"/>
<junction x="259.08" y="287.02"/>
<pinref part="SV37" gate="1" pin="6"/>
<wire x1="264.16" y1="289.56" x2="259.08" y2="289.56" width="0.1524" layer="91"/>
<wire x1="259.08" y1="289.56" x2="259.08" y2="287.02" width="0.1524" layer="91"/>
<junction x="259.08" y="289.56"/>
<wire x1="259.08" y1="289.56" x2="259.08" y2="292.1" width="0.1524" layer="91"/>
<junction x="259.08" y="292.1"/>
<wire x1="259.08" y1="294.64" x2="259.08" y2="292.1" width="0.1524" layer="91"/>
<wire x1="264.16" y1="294.64" x2="259.08" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="2"/>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="259.08" y1="274.32" x2="259.08" y2="276.86" width="0.1524" layer="91"/>
<junction x="259.08" y="276.86"/>
</segment>
<segment>
<wire x1="327.66" y1="294.64" x2="322.58" y2="294.64" width="0.1524" layer="91"/>
<wire x1="327.66" y1="289.56" x2="322.58" y2="289.56" width="0.1524" layer="91"/>
<wire x1="327.66" y1="284.48" x2="322.58" y2="284.48" width="0.1524" layer="91"/>
<wire x1="327.66" y1="279.4" x2="322.58" y2="279.4" width="0.1524" layer="91"/>
<wire x1="327.66" y1="281.94" x2="322.58" y2="281.94" width="0.1524" layer="91"/>
<wire x1="322.58" y1="281.94" x2="322.58" y2="279.4" width="0.1524" layer="91"/>
<junction x="322.58" y="281.94"/>
<wire x1="322.58" y1="281.94" x2="322.58" y2="284.48" width="0.1524" layer="91"/>
<junction x="322.58" y="284.48"/>
<wire x1="327.66" y1="287.02" x2="322.58" y2="287.02" width="0.1524" layer="91"/>
<wire x1="322.58" y1="287.02" x2="322.58" y2="284.48" width="0.1524" layer="91"/>
<junction x="322.58" y="287.02"/>
<wire x1="322.58" y1="287.02" x2="322.58" y2="289.56" width="0.1524" layer="91"/>
<junction x="322.58" y="289.56"/>
<wire x1="327.66" y1="292.1" x2="322.58" y2="292.1" width="0.1524" layer="91"/>
<wire x1="322.58" y1="292.1" x2="322.58" y2="289.56" width="0.1524" layer="91"/>
<junction x="322.58" y="292.1"/>
<wire x1="322.58" y1="292.1" x2="322.58" y2="294.64" width="0.1524" layer="91"/>
<junction x="322.58" y="294.64"/>
<wire x1="327.66" y1="297.18" x2="322.58" y2="297.18" width="0.1524" layer="91"/>
<wire x1="322.58" y1="297.18" x2="322.58" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="2"/>
<pinref part="SV38" gate="1" pin="4"/>
<pinref part="SV38" gate="1" pin="6"/>
<pinref part="SV38" gate="1" pin="8"/>
<pinref part="SV38" gate="1" pin="10"/>
<pinref part="SV38" gate="1" pin="12"/>
<pinref part="SV38" gate="1" pin="14"/>
<pinref part="SV38" gate="1" pin="16"/>
<pinref part="GND77" gate="1" pin="GND"/>
<wire x1="322.58" y1="276.86" x2="322.58" y2="279.4" width="0.1524" layer="91"/>
<junction x="322.58" y="279.4"/>
</segment>
<segment>
<wire x1="327.66" y1="408.94" x2="322.58" y2="408.94" width="0.1524" layer="91"/>
<wire x1="322.58" y1="408.94" x2="322.58" y2="406.4" width="0.1524" layer="91"/>
<wire x1="327.66" y1="406.4" x2="322.58" y2="406.4" width="0.1524" layer="91"/>
<junction x="322.58" y="406.4"/>
<wire x1="327.66" y1="403.86" x2="322.58" y2="403.86" width="0.1524" layer="91"/>
<wire x1="322.58" y1="403.86" x2="322.58" y2="406.4" width="0.1524" layer="91"/>
<junction x="322.58" y="403.86"/>
<wire x1="322.58" y1="403.86" x2="322.58" y2="401.32" width="0.1524" layer="91"/>
<wire x1="327.66" y1="401.32" x2="322.58" y2="401.32" width="0.1524" layer="91"/>
<junction x="322.58" y="401.32"/>
<wire x1="327.66" y1="398.78" x2="322.58" y2="398.78" width="0.1524" layer="91"/>
<wire x1="322.58" y1="398.78" x2="322.58" y2="401.32" width="0.1524" layer="91"/>
<junction x="322.58" y="398.78"/>
<wire x1="322.58" y1="398.78" x2="322.58" y2="396.24" width="0.1524" layer="91"/>
<wire x1="327.66" y1="396.24" x2="322.58" y2="396.24" width="0.1524" layer="91"/>
<junction x="322.58" y="396.24"/>
<wire x1="327.66" y1="393.7" x2="322.58" y2="393.7" width="0.1524" layer="91"/>
<wire x1="322.58" y1="393.7" x2="322.58" y2="396.24" width="0.1524" layer="91"/>
<junction x="322.58" y="393.7"/>
<wire x1="322.58" y1="393.7" x2="322.58" y2="391.16" width="0.1524" layer="91"/>
<wire x1="327.66" y1="391.16" x2="322.58" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="2"/>
<pinref part="SV34" gate="1" pin="4"/>
<pinref part="SV34" gate="1" pin="6"/>
<pinref part="SV34" gate="1" pin="8"/>
<pinref part="SV34" gate="1" pin="10"/>
<pinref part="SV34" gate="1" pin="12"/>
<pinref part="SV34" gate="1" pin="14"/>
<pinref part="SV34" gate="1" pin="16"/>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="322.58" y1="388.62" x2="322.58" y2="391.16" width="0.1524" layer="91"/>
<junction x="322.58" y="391.16"/>
</segment>
<segment>
<wire x1="391.16" y1="408.94" x2="386.08" y2="408.94" width="0.1524" layer="91"/>
<wire x1="386.08" y1="408.94" x2="386.08" y2="406.4" width="0.1524" layer="91"/>
<wire x1="391.16" y1="406.4" x2="386.08" y2="406.4" width="0.1524" layer="91"/>
<junction x="386.08" y="406.4"/>
<wire x1="391.16" y1="403.86" x2="386.08" y2="403.86" width="0.1524" layer="91"/>
<wire x1="386.08" y1="403.86" x2="386.08" y2="406.4" width="0.1524" layer="91"/>
<junction x="386.08" y="403.86"/>
<wire x1="386.08" y1="403.86" x2="386.08" y2="401.32" width="0.1524" layer="91"/>
<wire x1="391.16" y1="401.32" x2="386.08" y2="401.32" width="0.1524" layer="91"/>
<junction x="386.08" y="401.32"/>
<wire x1="391.16" y1="398.78" x2="386.08" y2="398.78" width="0.1524" layer="91"/>
<wire x1="386.08" y1="398.78" x2="386.08" y2="401.32" width="0.1524" layer="91"/>
<junction x="386.08" y="398.78"/>
<wire x1="386.08" y1="398.78" x2="386.08" y2="396.24" width="0.1524" layer="91"/>
<wire x1="391.16" y1="396.24" x2="386.08" y2="396.24" width="0.1524" layer="91"/>
<junction x="386.08" y="396.24"/>
<wire x1="391.16" y1="393.7" x2="386.08" y2="393.7" width="0.1524" layer="91"/>
<wire x1="386.08" y1="393.7" x2="386.08" y2="396.24" width="0.1524" layer="91"/>
<junction x="386.08" y="393.7"/>
<wire x1="386.08" y1="393.7" x2="386.08" y2="391.16" width="0.1524" layer="91"/>
<wire x1="391.16" y1="391.16" x2="386.08" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="2"/>
<pinref part="SV35" gate="1" pin="4"/>
<pinref part="SV35" gate="1" pin="6"/>
<pinref part="SV35" gate="1" pin="8"/>
<pinref part="SV35" gate="1" pin="10"/>
<pinref part="SV35" gate="1" pin="12"/>
<pinref part="SV35" gate="1" pin="14"/>
<pinref part="SV35" gate="1" pin="16"/>
<pinref part="GND75" gate="1" pin="GND"/>
<wire x1="386.08" y1="388.62" x2="386.08" y2="391.16" width="0.1524" layer="91"/>
<junction x="386.08" y="391.16"/>
</segment>
<segment>
<pinref part="SV39" gate="1" pin="4"/>
<wire x1="391.16" y1="294.64" x2="386.08" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="8"/>
<wire x1="391.16" y1="289.56" x2="386.08" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="12"/>
<wire x1="391.16" y1="284.48" x2="386.08" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="16"/>
<wire x1="391.16" y1="279.4" x2="386.08" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="14"/>
<wire x1="391.16" y1="281.94" x2="386.08" y2="281.94" width="0.1524" layer="91"/>
<wire x1="386.08" y1="281.94" x2="386.08" y2="279.4" width="0.1524" layer="91"/>
<junction x="386.08" y="281.94"/>
<wire x1="386.08" y1="281.94" x2="386.08" y2="284.48" width="0.1524" layer="91"/>
<junction x="386.08" y="284.48"/>
<pinref part="SV39" gate="1" pin="10"/>
<wire x1="391.16" y1="287.02" x2="386.08" y2="287.02" width="0.1524" layer="91"/>
<wire x1="386.08" y1="287.02" x2="386.08" y2="284.48" width="0.1524" layer="91"/>
<junction x="386.08" y="287.02"/>
<wire x1="386.08" y1="287.02" x2="386.08" y2="289.56" width="0.1524" layer="91"/>
<junction x="386.08" y="289.56"/>
<pinref part="SV39" gate="1" pin="6"/>
<wire x1="391.16" y1="292.1" x2="386.08" y2="292.1" width="0.1524" layer="91"/>
<wire x1="386.08" y1="292.1" x2="386.08" y2="289.56" width="0.1524" layer="91"/>
<junction x="386.08" y="292.1"/>
<wire x1="386.08" y1="292.1" x2="386.08" y2="294.64" width="0.1524" layer="91"/>
<junction x="386.08" y="294.64"/>
<wire x1="386.08" y1="297.18" x2="386.08" y2="294.64" width="0.1524" layer="91"/>
<wire x1="391.16" y1="297.18" x2="386.08" y2="297.18" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="2"/>
<pinref part="GND76" gate="1" pin="GND"/>
<wire x1="386.08" y1="276.86" x2="386.08" y2="279.4" width="0.1524" layer="91"/>
<junction x="386.08" y="279.4"/>
</segment>
<segment>
<wire x1="454.66" y1="408.94" x2="449.58" y2="408.94" width="0.1524" layer="91"/>
<wire x1="449.58" y1="408.94" x2="449.58" y2="406.4" width="0.1524" layer="91"/>
<wire x1="454.66" y1="406.4" x2="449.58" y2="406.4" width="0.1524" layer="91"/>
<junction x="449.58" y="406.4"/>
<wire x1="454.66" y1="403.86" x2="449.58" y2="403.86" width="0.1524" layer="91"/>
<wire x1="449.58" y1="403.86" x2="449.58" y2="406.4" width="0.1524" layer="91"/>
<junction x="449.58" y="403.86"/>
<wire x1="449.58" y1="403.86" x2="449.58" y2="401.32" width="0.1524" layer="91"/>
<wire x1="454.66" y1="401.32" x2="449.58" y2="401.32" width="0.1524" layer="91"/>
<junction x="449.58" y="401.32"/>
<wire x1="454.66" y1="398.78" x2="449.58" y2="398.78" width="0.1524" layer="91"/>
<wire x1="449.58" y1="398.78" x2="449.58" y2="401.32" width="0.1524" layer="91"/>
<junction x="449.58" y="398.78"/>
<wire x1="449.58" y1="398.78" x2="449.58" y2="396.24" width="0.1524" layer="91"/>
<wire x1="454.66" y1="396.24" x2="449.58" y2="396.24" width="0.1524" layer="91"/>
<junction x="449.58" y="396.24"/>
<wire x1="454.66" y1="391.16" x2="449.58" y2="391.16" width="0.1524" layer="91"/>
<wire x1="454.66" y1="393.7" x2="449.58" y2="393.7" width="0.1524" layer="91"/>
<wire x1="449.58" y1="393.7" x2="449.58" y2="391.16" width="0.1524" layer="91"/>
<wire x1="449.58" y1="393.7" x2="449.58" y2="396.24" width="0.1524" layer="91"/>
<junction x="449.58" y="393.7"/>
<pinref part="SV36" gate="1" pin="2"/>
<pinref part="SV36" gate="1" pin="4"/>
<pinref part="SV36" gate="1" pin="6"/>
<pinref part="SV36" gate="1" pin="8"/>
<pinref part="SV36" gate="1" pin="10"/>
<pinref part="SV36" gate="1" pin="12"/>
<pinref part="SV36" gate="1" pin="14"/>
<pinref part="SV36" gate="1" pin="16"/>
<pinref part="GND74" gate="1" pin="GND"/>
<wire x1="449.58" y1="388.62" x2="449.58" y2="391.16" width="0.1524" layer="91"/>
<junction x="449.58" y="391.16"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<wire x1="281.94" y1="180.34" x2="287.02" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="1"/>
<wire x1="287.02" y1="180.34" x2="287.02" y2="215.9" width="0.1524" layer="91"/>
<pinref part="G193" gate="G$1" pin="1"/>
<wire x1="287.02" y1="215.9" x2="304.8" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<wire x1="281.94" y1="177.8" x2="289.56" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="3"/>
<wire x1="289.56" y1="177.8" x2="289.56" y2="203.2" width="0.1524" layer="91"/>
<pinref part="G194" gate="G$1" pin="1"/>
<wire x1="289.56" y1="203.2" x2="304.8" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<wire x1="281.94" y1="175.26" x2="292.1" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="5"/>
<wire x1="292.1" y1="175.26" x2="292.1" y2="190.5" width="0.1524" layer="91"/>
<pinref part="G195" gate="G$1" pin="1"/>
<wire x1="292.1" y1="190.5" x2="304.8" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$195" class="0">
<segment>
<pinref part="SV25" gate="1" pin="7"/>
<wire x1="281.94" y1="172.72" x2="294.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="294.64" y1="172.72" x2="294.64" y2="177.8" width="0.1524" layer="91"/>
<pinref part="G196" gate="G$1" pin="1"/>
<wire x1="294.64" y1="177.8" x2="304.8" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$196" class="0">
<segment>
<pinref part="SV25" gate="1" pin="9"/>
<wire x1="281.94" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="170.18" x2="294.64" y2="165.1" width="0.1524" layer="91"/>
<pinref part="G197" gate="G$1" pin="1"/>
<wire x1="294.64" y1="165.1" x2="304.8" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<wire x1="281.94" y1="167.64" x2="292.1" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="11"/>
<wire x1="292.1" y1="167.64" x2="292.1" y2="152.4" width="0.1524" layer="91"/>
<pinref part="G198" gate="G$1" pin="1"/>
<wire x1="292.1" y1="152.4" x2="304.8" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$198" class="0">
<segment>
<wire x1="281.94" y1="165.1" x2="289.56" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="13"/>
<wire x1="289.56" y1="165.1" x2="289.56" y2="139.7" width="0.1524" layer="91"/>
<pinref part="G199" gate="G$1" pin="1"/>
<wire x1="289.56" y1="139.7" x2="304.8" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$199" class="0">
<segment>
<wire x1="281.94" y1="162.56" x2="287.02" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV25" gate="1" pin="15"/>
<wire x1="287.02" y1="162.56" x2="287.02" y2="127" width="0.1524" layer="91"/>
<pinref part="G200" gate="G$1" pin="1"/>
<wire x1="287.02" y1="127" x2="304.8" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$200" class="0">
<segment>
<wire x1="345.44" y1="180.34" x2="353.06" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="1"/>
<wire x1="353.06" y1="180.34" x2="353.06" y2="215.9" width="0.1524" layer="91"/>
<pinref part="G201" gate="G$1" pin="1"/>
<wire x1="353.06" y1="215.9" x2="368.3" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$201" class="0">
<segment>
<pinref part="G202" gate="G$1" pin="1"/>
<wire x1="355.6" y1="203.2" x2="368.3" y2="203.2" width="0.1524" layer="91"/>
<wire x1="355.6" y1="203.2" x2="355.6" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="3"/>
<wire x1="355.6" y1="177.8" x2="345.44" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$202" class="0">
<segment>
<wire x1="345.44" y1="175.26" x2="358.14" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="5"/>
<wire x1="358.14" y1="175.26" x2="358.14" y2="190.5" width="0.1524" layer="91"/>
<pinref part="G203" gate="G$1" pin="1"/>
<wire x1="358.14" y1="190.5" x2="368.3" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<pinref part="SV26" gate="1" pin="7"/>
<wire x1="345.44" y1="172.72" x2="360.68" y2="172.72" width="0.1524" layer="91"/>
<pinref part="G204" gate="G$1" pin="1"/>
<wire x1="360.68" y1="177.8" x2="368.3" y2="177.8" width="0.1524" layer="91"/>
<wire x1="360.68" y1="172.72" x2="360.68" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$204" class="0">
<segment>
<pinref part="SV26" gate="1" pin="9"/>
<pinref part="G205" gate="G$1" pin="1"/>
<wire x1="360.68" y1="165.1" x2="368.3" y2="165.1" width="0.1524" layer="91"/>
<wire x1="345.44" y1="170.18" x2="360.68" y2="170.18" width="0.1524" layer="91"/>
<wire x1="360.68" y1="170.18" x2="360.68" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$205" class="0">
<segment>
<wire x1="345.44" y1="167.64" x2="358.14" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="11"/>
<wire x1="358.14" y1="167.64" x2="358.14" y2="152.4" width="0.1524" layer="91"/>
<pinref part="G206" gate="G$1" pin="1"/>
<wire x1="358.14" y1="152.4" x2="368.3" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$206" class="0">
<segment>
<wire x1="345.44" y1="165.1" x2="355.6" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="13"/>
<wire x1="355.6" y1="165.1" x2="355.6" y2="139.7" width="0.1524" layer="91"/>
<pinref part="G207" gate="G$1" pin="1"/>
<wire x1="355.6" y1="139.7" x2="368.3" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$207" class="0">
<segment>
<wire x1="345.44" y1="162.56" x2="353.06" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV26" gate="1" pin="15"/>
<wire x1="353.06" y1="162.56" x2="353.06" y2="127" width="0.1524" layer="91"/>
<pinref part="G208" gate="G$1" pin="1"/>
<wire x1="353.06" y1="127" x2="368.3" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$208" class="0">
<segment>
<wire x1="408.94" y1="180.34" x2="416.56" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="1"/>
<wire x1="416.56" y1="180.34" x2="416.56" y2="215.9" width="0.1524" layer="91"/>
<pinref part="G209" gate="G$1" pin="1"/>
<wire x1="416.56" y1="215.9" x2="431.8" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$209" class="0">
<segment>
<pinref part="G210" gate="G$1" pin="1"/>
<wire x1="419.1" y1="203.2" x2="431.8" y2="203.2" width="0.1524" layer="91"/>
<wire x1="419.1" y1="203.2" x2="419.1" y2="177.8" width="0.1524" layer="91"/>
<wire x1="408.94" y1="177.8" x2="419.1" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="3"/>
</segment>
</net>
<net name="N$210" class="0">
<segment>
<pinref part="G211" gate="G$1" pin="1"/>
<wire x1="421.64" y1="190.5" x2="431.8" y2="190.5" width="0.1524" layer="91"/>
<wire x1="421.64" y1="190.5" x2="421.64" y2="175.26" width="0.1524" layer="91"/>
<wire x1="408.94" y1="175.26" x2="421.64" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="5"/>
</segment>
</net>
<net name="N$211" class="0">
<segment>
<pinref part="SV27" gate="1" pin="7"/>
<wire x1="408.94" y1="172.72" x2="424.18" y2="172.72" width="0.1524" layer="91"/>
<pinref part="G212" gate="G$1" pin="1"/>
<wire x1="424.18" y1="177.8" x2="431.8" y2="177.8" width="0.1524" layer="91"/>
<wire x1="424.18" y1="172.72" x2="424.18" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$212" class="0">
<segment>
<pinref part="G213" gate="G$1" pin="1"/>
<wire x1="424.18" y1="165.1" x2="431.8" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="9"/>
<wire x1="424.18" y1="165.1" x2="424.18" y2="170.18" width="0.1524" layer="91"/>
<wire x1="424.18" y1="170.18" x2="408.94" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$213" class="0">
<segment>
<wire x1="408.94" y1="167.64" x2="421.64" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="11"/>
<wire x1="421.64" y1="167.64" x2="421.64" y2="152.4" width="0.1524" layer="91"/>
<pinref part="G214" gate="G$1" pin="1"/>
<wire x1="421.64" y1="152.4" x2="431.8" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$214" class="0">
<segment>
<wire x1="408.94" y1="165.1" x2="419.1" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="13"/>
<wire x1="419.1" y1="165.1" x2="419.1" y2="139.7" width="0.1524" layer="91"/>
<pinref part="G215" gate="G$1" pin="1"/>
<wire x1="419.1" y1="139.7" x2="431.8" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$215" class="0">
<segment>
<wire x1="408.94" y1="162.56" x2="416.56" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV27" gate="1" pin="15"/>
<wire x1="416.56" y1="162.56" x2="416.56" y2="127" width="0.1524" layer="91"/>
<pinref part="G216" gate="G$1" pin="1"/>
<wire x1="416.56" y1="127" x2="431.8" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$216" class="0">
<segment>
<wire x1="472.44" y1="180.34" x2="480.06" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="1"/>
<wire x1="480.06" y1="180.34" x2="480.06" y2="215.9" width="0.1524" layer="91"/>
<pinref part="G217" gate="G$1" pin="1"/>
<wire x1="480.06" y1="215.9" x2="495.3" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$217" class="0">
<segment>
<pinref part="G218" gate="G$1" pin="1"/>
<wire x1="482.6" y1="203.2" x2="495.3" y2="203.2" width="0.1524" layer="91"/>
<wire x1="482.6" y1="203.2" x2="482.6" y2="177.8" width="0.1524" layer="91"/>
<wire x1="472.44" y1="177.8" x2="482.6" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="3"/>
</segment>
</net>
<net name="N$218" class="0">
<segment>
<pinref part="G219" gate="G$1" pin="1"/>
<wire x1="485.14" y1="190.5" x2="495.3" y2="190.5" width="0.1524" layer="91"/>
<wire x1="485.14" y1="190.5" x2="485.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="472.44" y1="175.26" x2="485.14" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="5"/>
</segment>
</net>
<net name="N$219" class="0">
<segment>
<pinref part="SV28" gate="1" pin="7"/>
<wire x1="472.44" y1="172.72" x2="487.68" y2="172.72" width="0.1524" layer="91"/>
<pinref part="G220" gate="G$1" pin="1"/>
<wire x1="487.68" y1="177.8" x2="495.3" y2="177.8" width="0.1524" layer="91"/>
<wire x1="487.68" y1="172.72" x2="487.68" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$220" class="0">
<segment>
<pinref part="G221" gate="G$1" pin="1"/>
<wire x1="487.68" y1="165.1" x2="495.3" y2="165.1" width="0.1524" layer="91"/>
<wire x1="487.68" y1="165.1" x2="487.68" y2="170.18" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="9"/>
<wire x1="487.68" y1="170.18" x2="472.44" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$221" class="0">
<segment>
<wire x1="472.44" y1="167.64" x2="485.14" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="11"/>
<wire x1="485.14" y1="167.64" x2="485.14" y2="152.4" width="0.1524" layer="91"/>
<pinref part="G222" gate="G$1" pin="1"/>
<wire x1="485.14" y1="152.4" x2="495.3" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$222" class="0">
<segment>
<wire x1="472.44" y1="165.1" x2="482.6" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="13"/>
<wire x1="482.6" y1="165.1" x2="482.6" y2="139.7" width="0.1524" layer="91"/>
<pinref part="G223" gate="G$1" pin="1"/>
<wire x1="482.6" y1="139.7" x2="495.3" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$223" class="0">
<segment>
<wire x1="472.44" y1="162.56" x2="480.06" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SV28" gate="1" pin="15"/>
<wire x1="480.06" y1="162.56" x2="480.06" y2="127" width="0.1524" layer="91"/>
<pinref part="G224" gate="G$1" pin="1"/>
<wire x1="480.06" y1="127" x2="495.3" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$224" class="0">
<segment>
<wire x1="281.94" y1="66.04" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="1"/>
<wire x1="289.56" y1="66.04" x2="289.56" y2="101.6" width="0.1524" layer="91"/>
<pinref part="G225" gate="G$1" pin="1"/>
<wire x1="289.56" y1="101.6" x2="304.8" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$225" class="0">
<segment>
<pinref part="G226" gate="G$1" pin="1"/>
<wire x1="292.1" y1="88.9" x2="304.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="281.94" y1="63.5" x2="292.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="3"/>
<wire x1="292.1" y1="88.9" x2="292.1" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$226" class="0">
<segment>
<pinref part="G227" gate="G$1" pin="1"/>
<wire x1="294.64" y1="76.2" x2="304.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="294.64" y1="76.2" x2="294.64" y2="60.96" width="0.1524" layer="91"/>
<wire x1="281.94" y1="60.96" x2="294.64" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="5"/>
</segment>
</net>
<net name="N$227" class="0">
<segment>
<pinref part="G228" gate="G$1" pin="1"/>
<wire x1="297.18" y1="63.5" x2="304.8" y2="63.5" width="0.1524" layer="91"/>
<wire x1="297.18" y1="63.5" x2="297.18" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="7"/>
<wire x1="297.18" y1="58.42" x2="281.94" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$228" class="0">
<segment>
<pinref part="SV29" gate="1" pin="9"/>
<wire x1="281.94" y1="55.88" x2="297.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="G229" gate="G$1" pin="1"/>
<wire x1="297.18" y1="50.8" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="297.18" y1="55.88" x2="297.18" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$229" class="0">
<segment>
<wire x1="281.94" y1="53.34" x2="294.64" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="11"/>
<wire x1="294.64" y1="53.34" x2="294.64" y2="38.1" width="0.1524" layer="91"/>
<pinref part="G230" gate="G$1" pin="1"/>
<wire x1="294.64" y1="38.1" x2="304.8" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$230" class="0">
<segment>
<wire x1="281.94" y1="50.8" x2="292.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="13"/>
<pinref part="G231" gate="G$1" pin="1"/>
<wire x1="292.1" y1="25.4" x2="304.8" y2="25.4" width="0.1524" layer="91"/>
<wire x1="292.1" y1="50.8" x2="292.1" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$231" class="0">
<segment>
<wire x1="281.94" y1="48.26" x2="289.56" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV29" gate="1" pin="15"/>
<pinref part="G232" gate="G$1" pin="1"/>
<wire x1="289.56" y1="48.26" x2="289.56" y2="12.7" width="0.1524" layer="91"/>
<wire x1="289.56" y1="12.7" x2="304.8" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$232" class="0">
<segment>
<wire x1="345.44" y1="68.58" x2="353.06" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="1"/>
<pinref part="G233" gate="G$1" pin="1"/>
<wire x1="353.06" y1="68.58" x2="353.06" y2="104.14" width="0.1524" layer="91"/>
<wire x1="353.06" y1="104.14" x2="368.3" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$233" class="0">
<segment>
<pinref part="G234" gate="G$1" pin="1"/>
<wire x1="355.6" y1="91.44" x2="368.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="355.6" y1="91.44" x2="355.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="345.44" y1="66.04" x2="355.6" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="3"/>
</segment>
</net>
<net name="N$234" class="0">
<segment>
<pinref part="G235" gate="G$1" pin="1"/>
<wire x1="358.14" y1="78.74" x2="368.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="358.14" y1="78.74" x2="358.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="345.44" y1="63.5" x2="358.14" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="5"/>
</segment>
</net>
<net name="N$235" class="0">
<segment>
<pinref part="SV30" gate="1" pin="7"/>
<pinref part="G236" gate="G$1" pin="1"/>
<wire x1="360.68" y1="66.04" x2="368.3" y2="66.04" width="0.1524" layer="91"/>
<wire x1="345.44" y1="60.96" x2="360.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="360.68" y1="60.96" x2="360.68" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$236" class="0">
<segment>
<pinref part="G237" gate="G$1" pin="1"/>
<wire x1="360.68" y1="53.34" x2="368.3" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="9"/>
<wire x1="360.68" y1="53.34" x2="360.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="360.68" y1="58.42" x2="345.44" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$237" class="0">
<segment>
<wire x1="345.44" y1="55.88" x2="358.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="11"/>
<wire x1="358.14" y1="55.88" x2="358.14" y2="40.64" width="0.1524" layer="91"/>
<pinref part="G238" gate="G$1" pin="1"/>
<wire x1="358.14" y1="40.64" x2="368.3" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$238" class="0">
<segment>
<wire x1="345.44" y1="53.34" x2="355.6" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="13"/>
<wire x1="355.6" y1="53.34" x2="355.6" y2="27.94" width="0.1524" layer="91"/>
<pinref part="G239" gate="G$1" pin="1"/>
<wire x1="355.6" y1="27.94" x2="368.3" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$239" class="0">
<segment>
<wire x1="345.44" y1="50.8" x2="353.06" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV30" gate="1" pin="15"/>
<pinref part="G240" gate="G$1" pin="1"/>
<wire x1="353.06" y1="50.8" x2="353.06" y2="15.24" width="0.1524" layer="91"/>
<wire x1="353.06" y1="15.24" x2="368.3" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$240" class="0">
<segment>
<wire x1="408.94" y1="68.58" x2="416.56" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="1"/>
<pinref part="G241" gate="G$1" pin="1"/>
<wire x1="416.56" y1="68.58" x2="416.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="416.56" y1="104.14" x2="431.8" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$241" class="0">
<segment>
<pinref part="G242" gate="G$1" pin="1"/>
<wire x1="419.1" y1="91.44" x2="431.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="419.1" y1="91.44" x2="419.1" y2="66.04" width="0.1524" layer="91"/>
<wire x1="408.94" y1="66.04" x2="419.1" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="3"/>
</segment>
</net>
<net name="N$242" class="0">
<segment>
<pinref part="G243" gate="G$1" pin="1"/>
<wire x1="421.64" y1="78.74" x2="431.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="421.64" y1="78.74" x2="421.64" y2="63.5" width="0.1524" layer="91"/>
<wire x1="408.94" y1="63.5" x2="421.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="5"/>
</segment>
</net>
<net name="N$243" class="0">
<segment>
<pinref part="SV31" gate="1" pin="7"/>
<pinref part="G244" gate="G$1" pin="1"/>
<wire x1="424.18" y1="66.04" x2="431.8" y2="66.04" width="0.1524" layer="91"/>
<wire x1="408.94" y1="60.96" x2="424.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="424.18" y1="60.96" x2="424.18" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$244" class="0">
<segment>
<pinref part="SV31" gate="1" pin="9"/>
<pinref part="G245" gate="G$1" pin="1"/>
<wire x1="424.18" y1="53.34" x2="431.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="408.94" y1="58.42" x2="424.18" y2="58.42" width="0.1524" layer="91"/>
<wire x1="424.18" y1="58.42" x2="424.18" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$245" class="0">
<segment>
<wire x1="408.94" y1="55.88" x2="421.64" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="11"/>
<wire x1="421.64" y1="55.88" x2="421.64" y2="40.64" width="0.1524" layer="91"/>
<pinref part="G246" gate="G$1" pin="1"/>
<wire x1="421.64" y1="40.64" x2="431.8" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$246" class="0">
<segment>
<wire x1="408.94" y1="53.34" x2="419.1" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="13"/>
<wire x1="419.1" y1="53.34" x2="419.1" y2="27.94" width="0.1524" layer="91"/>
<pinref part="G247" gate="G$1" pin="1"/>
<wire x1="419.1" y1="27.94" x2="431.8" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$247" class="0">
<segment>
<wire x1="408.94" y1="50.8" x2="416.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV31" gate="1" pin="15"/>
<pinref part="G248" gate="G$1" pin="1"/>
<wire x1="416.56" y1="50.8" x2="416.56" y2="15.24" width="0.1524" layer="91"/>
<wire x1="416.56" y1="15.24" x2="431.8" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$248" class="0">
<segment>
<wire x1="472.44" y1="68.58" x2="480.06" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="1"/>
<pinref part="G249" gate="G$1" pin="1"/>
<wire x1="480.06" y1="68.58" x2="480.06" y2="104.14" width="0.1524" layer="91"/>
<wire x1="480.06" y1="104.14" x2="495.3" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$249" class="0">
<segment>
<pinref part="G250" gate="G$1" pin="1"/>
<wire x1="482.6" y1="91.44" x2="495.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="482.6" y1="91.44" x2="482.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="472.44" y1="66.04" x2="482.6" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="3"/>
</segment>
</net>
<net name="N$250" class="0">
<segment>
<pinref part="G251" gate="G$1" pin="1"/>
<wire x1="485.14" y1="78.74" x2="495.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="485.14" y1="78.74" x2="485.14" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="5"/>
<wire x1="485.14" y1="63.5" x2="472.44" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$251" class="0">
<segment>
<pinref part="SV32" gate="1" pin="7"/>
<pinref part="G252" gate="G$1" pin="1"/>
<wire x1="487.68" y1="66.04" x2="495.3" y2="66.04" width="0.1524" layer="91"/>
<wire x1="472.44" y1="60.96" x2="487.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="487.68" y1="60.96" x2="487.68" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$252" class="0">
<segment>
<pinref part="G253" gate="G$1" pin="1"/>
<wire x1="487.68" y1="53.34" x2="495.3" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="9"/>
<wire x1="487.68" y1="53.34" x2="487.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="487.68" y1="58.42" x2="472.44" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$253" class="0">
<segment>
<wire x1="472.44" y1="55.88" x2="485.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="11"/>
<wire x1="485.14" y1="55.88" x2="485.14" y2="40.64" width="0.1524" layer="91"/>
<pinref part="G254" gate="G$1" pin="1"/>
<wire x1="485.14" y1="40.64" x2="495.3" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$254" class="0">
<segment>
<wire x1="472.44" y1="53.34" x2="482.6" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="13"/>
<wire x1="482.6" y1="53.34" x2="482.6" y2="27.94" width="0.1524" layer="91"/>
<pinref part="G255" gate="G$1" pin="1"/>
<wire x1="482.6" y1="27.94" x2="495.3" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$255" class="0">
<segment>
<wire x1="472.44" y1="50.8" x2="480.06" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SV32" gate="1" pin="15"/>
<pinref part="G256" gate="G$1" pin="1"/>
<wire x1="480.06" y1="50.8" x2="480.06" y2="15.24" width="0.1524" layer="91"/>
<wire x1="480.06" y1="15.24" x2="495.3" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<wire x1="22.86" y1="406.4" x2="27.94" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="1"/>
<wire x1="27.94" y1="406.4" x2="27.94" y2="441.96" width="0.1524" layer="91"/>
<pinref part="G129" gate="G$1" pin="1"/>
<wire x1="27.94" y1="441.96" x2="45.72" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<wire x1="22.86" y1="403.86" x2="30.48" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="3"/>
<wire x1="30.48" y1="403.86" x2="30.48" y2="429.26" width="0.1524" layer="91"/>
<pinref part="G130" gate="G$1" pin="1"/>
<wire x1="30.48" y1="429.26" x2="45.72" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<wire x1="22.86" y1="401.32" x2="33.02" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="5"/>
<wire x1="33.02" y1="401.32" x2="33.02" y2="416.56" width="0.1524" layer="91"/>
<pinref part="G131" gate="G$1" pin="1"/>
<wire x1="33.02" y1="416.56" x2="45.72" y2="416.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="SV17" gate="1" pin="7"/>
<wire x1="22.86" y1="398.78" x2="35.56" y2="398.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="398.78" x2="35.56" y2="403.86" width="0.1524" layer="91"/>
<pinref part="G132" gate="G$1" pin="1"/>
<wire x1="35.56" y1="403.86" x2="45.72" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="SV17" gate="1" pin="9"/>
<wire x1="22.86" y1="396.24" x2="35.56" y2="396.24" width="0.1524" layer="91"/>
<wire x1="35.56" y1="396.24" x2="35.56" y2="391.16" width="0.1524" layer="91"/>
<pinref part="G133" gate="G$1" pin="1"/>
<wire x1="35.56" y1="391.16" x2="45.72" y2="391.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<wire x1="22.86" y1="393.7" x2="33.02" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="11"/>
<wire x1="33.02" y1="393.7" x2="33.02" y2="378.46" width="0.1524" layer="91"/>
<pinref part="G134" gate="G$1" pin="1"/>
<wire x1="33.02" y1="378.46" x2="45.72" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="22.86" y1="391.16" x2="30.48" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="13"/>
<wire x1="30.48" y1="391.16" x2="30.48" y2="365.76" width="0.1524" layer="91"/>
<pinref part="G135" gate="G$1" pin="1"/>
<wire x1="30.48" y1="365.76" x2="45.72" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<wire x1="22.86" y1="388.62" x2="27.94" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV17" gate="1" pin="15"/>
<wire x1="27.94" y1="388.62" x2="27.94" y2="353.06" width="0.1524" layer="91"/>
<pinref part="G136" gate="G$1" pin="1"/>
<wire x1="27.94" y1="353.06" x2="45.72" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<wire x1="86.36" y1="406.4" x2="93.98" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="1"/>
<wire x1="93.98" y1="406.4" x2="93.98" y2="441.96" width="0.1524" layer="91"/>
<pinref part="G137" gate="G$1" pin="1"/>
<wire x1="93.98" y1="441.96" x2="109.22" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="G138" gate="G$1" pin="1"/>
<wire x1="96.52" y1="429.26" x2="109.22" y2="429.26" width="0.1524" layer="91"/>
<wire x1="96.52" y1="429.26" x2="96.52" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="3"/>
<wire x1="96.52" y1="403.86" x2="86.36" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<wire x1="86.36" y1="401.32" x2="99.06" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="5"/>
<wire x1="99.06" y1="401.32" x2="99.06" y2="416.56" width="0.1524" layer="91"/>
<pinref part="G139" gate="G$1" pin="1"/>
<wire x1="99.06" y1="416.56" x2="109.22" y2="416.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="SV18" gate="1" pin="7"/>
<wire x1="86.36" y1="398.78" x2="101.6" y2="398.78" width="0.1524" layer="91"/>
<pinref part="G140" gate="G$1" pin="1"/>
<wire x1="101.6" y1="403.86" x2="109.22" y2="403.86" width="0.1524" layer="91"/>
<wire x1="101.6" y1="398.78" x2="101.6" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="SV18" gate="1" pin="9"/>
<pinref part="G141" gate="G$1" pin="1"/>
<wire x1="101.6" y1="391.16" x2="109.22" y2="391.16" width="0.1524" layer="91"/>
<wire x1="86.36" y1="396.24" x2="101.6" y2="396.24" width="0.1524" layer="91"/>
<wire x1="101.6" y1="396.24" x2="101.6" y2="391.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<wire x1="86.36" y1="393.7" x2="99.06" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="11"/>
<wire x1="99.06" y1="393.7" x2="99.06" y2="378.46" width="0.1524" layer="91"/>
<pinref part="G142" gate="G$1" pin="1"/>
<wire x1="99.06" y1="378.46" x2="109.22" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<wire x1="86.36" y1="391.16" x2="96.52" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="13"/>
<wire x1="96.52" y1="391.16" x2="96.52" y2="365.76" width="0.1524" layer="91"/>
<pinref part="G143" gate="G$1" pin="1"/>
<wire x1="96.52" y1="365.76" x2="109.22" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<wire x1="86.36" y1="388.62" x2="93.98" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV18" gate="1" pin="15"/>
<wire x1="93.98" y1="388.62" x2="93.98" y2="353.06" width="0.1524" layer="91"/>
<pinref part="G144" gate="G$1" pin="1"/>
<wire x1="93.98" y1="353.06" x2="109.22" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<wire x1="149.86" y1="406.4" x2="157.48" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="1"/>
<wire x1="157.48" y1="406.4" x2="157.48" y2="441.96" width="0.1524" layer="91"/>
<pinref part="G145" gate="G$1" pin="1"/>
<wire x1="157.48" y1="441.96" x2="172.72" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="G146" gate="G$1" pin="1"/>
<wire x1="160.02" y1="429.26" x2="172.72" y2="429.26" width="0.1524" layer="91"/>
<wire x1="160.02" y1="429.26" x2="160.02" y2="403.86" width="0.1524" layer="91"/>
<wire x1="149.86" y1="403.86" x2="160.02" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="3"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="G147" gate="G$1" pin="1"/>
<wire x1="162.56" y1="416.56" x2="172.72" y2="416.56" width="0.1524" layer="91"/>
<wire x1="162.56" y1="416.56" x2="162.56" y2="401.32" width="0.1524" layer="91"/>
<wire x1="149.86" y1="401.32" x2="162.56" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="5"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="SV19" gate="1" pin="7"/>
<wire x1="149.86" y1="398.78" x2="165.1" y2="398.78" width="0.1524" layer="91"/>
<pinref part="G148" gate="G$1" pin="1"/>
<wire x1="165.1" y1="403.86" x2="172.72" y2="403.86" width="0.1524" layer="91"/>
<wire x1="165.1" y1="398.78" x2="165.1" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="G149" gate="G$1" pin="1"/>
<wire x1="165.1" y1="391.16" x2="172.72" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="9"/>
<wire x1="165.1" y1="391.16" x2="165.1" y2="396.24" width="0.1524" layer="91"/>
<wire x1="165.1" y1="396.24" x2="149.86" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<wire x1="149.86" y1="393.7" x2="162.56" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="11"/>
<wire x1="162.56" y1="393.7" x2="162.56" y2="378.46" width="0.1524" layer="91"/>
<pinref part="G150" gate="G$1" pin="1"/>
<wire x1="162.56" y1="378.46" x2="172.72" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<wire x1="149.86" y1="391.16" x2="160.02" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="13"/>
<wire x1="160.02" y1="391.16" x2="160.02" y2="365.76" width="0.1524" layer="91"/>
<pinref part="G151" gate="G$1" pin="1"/>
<wire x1="160.02" y1="365.76" x2="172.72" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<wire x1="149.86" y1="388.62" x2="157.48" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV19" gate="1" pin="15"/>
<wire x1="157.48" y1="388.62" x2="157.48" y2="353.06" width="0.1524" layer="91"/>
<pinref part="G152" gate="G$1" pin="1"/>
<wire x1="157.48" y1="353.06" x2="172.72" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$152" class="0">
<segment>
<wire x1="213.36" y1="406.4" x2="220.98" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="1"/>
<wire x1="220.98" y1="406.4" x2="220.98" y2="441.96" width="0.1524" layer="91"/>
<pinref part="G153" gate="G$1" pin="1"/>
<wire x1="220.98" y1="441.96" x2="236.22" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="G154" gate="G$1" pin="1"/>
<wire x1="223.52" y1="429.26" x2="236.22" y2="429.26" width="0.1524" layer="91"/>
<wire x1="223.52" y1="429.26" x2="223.52" y2="403.86" width="0.1524" layer="91"/>
<wire x1="213.36" y1="403.86" x2="223.52" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="3"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<pinref part="G155" gate="G$1" pin="1"/>
<wire x1="226.06" y1="416.56" x2="236.22" y2="416.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="416.56" x2="226.06" y2="401.32" width="0.1524" layer="91"/>
<wire x1="213.36" y1="401.32" x2="226.06" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="5"/>
</segment>
</net>
<net name="N$155" class="0">
<segment>
<pinref part="SV20" gate="1" pin="7"/>
<wire x1="213.36" y1="398.78" x2="228.6" y2="398.78" width="0.1524" layer="91"/>
<pinref part="G156" gate="G$1" pin="1"/>
<wire x1="228.6" y1="403.86" x2="236.22" y2="403.86" width="0.1524" layer="91"/>
<wire x1="228.6" y1="398.78" x2="228.6" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$156" class="0">
<segment>
<pinref part="G157" gate="G$1" pin="1"/>
<wire x1="228.6" y1="391.16" x2="236.22" y2="391.16" width="0.1524" layer="91"/>
<wire x1="228.6" y1="391.16" x2="228.6" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="9"/>
<wire x1="228.6" y1="396.24" x2="213.36" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$157" class="0">
<segment>
<wire x1="213.36" y1="393.7" x2="226.06" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="11"/>
<wire x1="226.06" y1="393.7" x2="226.06" y2="378.46" width="0.1524" layer="91"/>
<pinref part="G158" gate="G$1" pin="1"/>
<wire x1="226.06" y1="378.46" x2="236.22" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<wire x1="213.36" y1="391.16" x2="223.52" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="13"/>
<wire x1="223.52" y1="391.16" x2="223.52" y2="365.76" width="0.1524" layer="91"/>
<pinref part="G159" gate="G$1" pin="1"/>
<wire x1="223.52" y1="365.76" x2="236.22" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<wire x1="213.36" y1="388.62" x2="220.98" y2="388.62" width="0.1524" layer="91"/>
<pinref part="SV20" gate="1" pin="15"/>
<wire x1="220.98" y1="388.62" x2="220.98" y2="353.06" width="0.1524" layer="91"/>
<pinref part="G160" gate="G$1" pin="1"/>
<wire x1="220.98" y1="353.06" x2="236.22" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$160" class="0">
<segment>
<wire x1="22.86" y1="292.1" x2="30.48" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="1"/>
<wire x1="30.48" y1="292.1" x2="30.48" y2="327.66" width="0.1524" layer="91"/>
<pinref part="G161" gate="G$1" pin="1"/>
<wire x1="30.48" y1="327.66" x2="45.72" y2="327.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$161" class="0">
<segment>
<pinref part="G162" gate="G$1" pin="1"/>
<wire x1="33.02" y1="314.96" x2="45.72" y2="314.96" width="0.1524" layer="91"/>
<wire x1="22.86" y1="289.56" x2="33.02" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="3"/>
<wire x1="33.02" y1="314.96" x2="33.02" y2="289.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$162" class="0">
<segment>
<pinref part="G163" gate="G$1" pin="1"/>
<wire x1="35.56" y1="302.26" x2="45.72" y2="302.26" width="0.1524" layer="91"/>
<wire x1="35.56" y1="302.26" x2="35.56" y2="287.02" width="0.1524" layer="91"/>
<wire x1="22.86" y1="287.02" x2="35.56" y2="287.02" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="5"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<pinref part="G164" gate="G$1" pin="1"/>
<wire x1="38.1" y1="289.56" x2="45.72" y2="289.56" width="0.1524" layer="91"/>
<wire x1="38.1" y1="289.56" x2="38.1" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="7"/>
<wire x1="38.1" y1="284.48" x2="22.86" y2="284.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<pinref part="SV21" gate="1" pin="9"/>
<wire x1="22.86" y1="281.94" x2="38.1" y2="281.94" width="0.1524" layer="91"/>
<pinref part="G165" gate="G$1" pin="1"/>
<wire x1="38.1" y1="276.86" x2="45.72" y2="276.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="281.94" x2="38.1" y2="276.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<wire x1="22.86" y1="279.4" x2="35.56" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="11"/>
<wire x1="35.56" y1="279.4" x2="35.56" y2="264.16" width="0.1524" layer="91"/>
<pinref part="G166" gate="G$1" pin="1"/>
<wire x1="35.56" y1="264.16" x2="45.72" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$166" class="0">
<segment>
<wire x1="22.86" y1="276.86" x2="33.02" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="13"/>
<pinref part="G167" gate="G$1" pin="1"/>
<wire x1="33.02" y1="251.46" x2="45.72" y2="251.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="276.86" x2="33.02" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$167" class="0">
<segment>
<wire x1="22.86" y1="274.32" x2="30.48" y2="274.32" width="0.1524" layer="91"/>
<pinref part="SV21" gate="1" pin="15"/>
<pinref part="G168" gate="G$1" pin="1"/>
<wire x1="30.48" y1="274.32" x2="30.48" y2="238.76" width="0.1524" layer="91"/>
<wire x1="30.48" y1="238.76" x2="45.72" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<wire x1="86.36" y1="294.64" x2="93.98" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="1"/>
<pinref part="G169" gate="G$1" pin="1"/>
<wire x1="93.98" y1="294.64" x2="93.98" y2="330.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="330.2" x2="109.22" y2="330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<pinref part="G170" gate="G$1" pin="1"/>
<wire x1="96.52" y1="317.5" x2="109.22" y2="317.5" width="0.1524" layer="91"/>
<wire x1="96.52" y1="317.5" x2="96.52" y2="292.1" width="0.1524" layer="91"/>
<wire x1="86.36" y1="292.1" x2="96.52" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="3"/>
</segment>
</net>
<net name="N$170" class="0">
<segment>
<pinref part="G171" gate="G$1" pin="1"/>
<wire x1="99.06" y1="304.8" x2="109.22" y2="304.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="304.8" x2="99.06" y2="289.56" width="0.1524" layer="91"/>
<wire x1="86.36" y1="289.56" x2="99.06" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="5"/>
</segment>
</net>
<net name="N$171" class="0">
<segment>
<pinref part="SV22" gate="1" pin="7"/>
<pinref part="G172" gate="G$1" pin="1"/>
<wire x1="101.6" y1="292.1" x2="109.22" y2="292.1" width="0.1524" layer="91"/>
<wire x1="86.36" y1="287.02" x2="101.6" y2="287.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="287.02" x2="101.6" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$172" class="0">
<segment>
<pinref part="G173" gate="G$1" pin="1"/>
<wire x1="101.6" y1="279.4" x2="109.22" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="9"/>
<wire x1="101.6" y1="279.4" x2="101.6" y2="284.48" width="0.1524" layer="91"/>
<wire x1="101.6" y1="284.48" x2="86.36" y2="284.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<wire x1="86.36" y1="281.94" x2="99.06" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="11"/>
<wire x1="99.06" y1="281.94" x2="99.06" y2="266.7" width="0.1524" layer="91"/>
<pinref part="G174" gate="G$1" pin="1"/>
<wire x1="99.06" y1="266.7" x2="109.22" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<wire x1="86.36" y1="279.4" x2="96.52" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="13"/>
<wire x1="96.52" y1="279.4" x2="96.52" y2="254" width="0.1524" layer="91"/>
<pinref part="G175" gate="G$1" pin="1"/>
<wire x1="96.52" y1="254" x2="109.22" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<wire x1="86.36" y1="276.86" x2="93.98" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV22" gate="1" pin="15"/>
<pinref part="G176" gate="G$1" pin="1"/>
<wire x1="93.98" y1="276.86" x2="93.98" y2="241.3" width="0.1524" layer="91"/>
<wire x1="93.98" y1="241.3" x2="109.22" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<wire x1="149.86" y1="294.64" x2="157.48" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="1"/>
<pinref part="G177" gate="G$1" pin="1"/>
<wire x1="157.48" y1="294.64" x2="157.48" y2="330.2" width="0.1524" layer="91"/>
<wire x1="157.48" y1="330.2" x2="172.72" y2="330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$177" class="0">
<segment>
<pinref part="G178" gate="G$1" pin="1"/>
<wire x1="160.02" y1="317.5" x2="172.72" y2="317.5" width="0.1524" layer="91"/>
<wire x1="160.02" y1="317.5" x2="160.02" y2="292.1" width="0.1524" layer="91"/>
<wire x1="149.86" y1="292.1" x2="160.02" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="3"/>
</segment>
</net>
<net name="N$178" class="0">
<segment>
<pinref part="G179" gate="G$1" pin="1"/>
<wire x1="162.56" y1="304.8" x2="172.72" y2="304.8" width="0.1524" layer="91"/>
<wire x1="162.56" y1="304.8" x2="162.56" y2="289.56" width="0.1524" layer="91"/>
<wire x1="149.86" y1="289.56" x2="162.56" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="5"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="SV23" gate="1" pin="7"/>
<pinref part="G180" gate="G$1" pin="1"/>
<wire x1="165.1" y1="292.1" x2="172.72" y2="292.1" width="0.1524" layer="91"/>
<wire x1="149.86" y1="287.02" x2="165.1" y2="287.02" width="0.1524" layer="91"/>
<wire x1="165.1" y1="287.02" x2="165.1" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$180" class="0">
<segment>
<pinref part="SV23" gate="1" pin="9"/>
<pinref part="G181" gate="G$1" pin="1"/>
<wire x1="165.1" y1="279.4" x2="172.72" y2="279.4" width="0.1524" layer="91"/>
<wire x1="149.86" y1="284.48" x2="165.1" y2="284.48" width="0.1524" layer="91"/>
<wire x1="165.1" y1="284.48" x2="165.1" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<wire x1="149.86" y1="281.94" x2="162.56" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="11"/>
<wire x1="162.56" y1="281.94" x2="162.56" y2="266.7" width="0.1524" layer="91"/>
<pinref part="G182" gate="G$1" pin="1"/>
<wire x1="162.56" y1="266.7" x2="172.72" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$182" class="0">
<segment>
<wire x1="149.86" y1="279.4" x2="160.02" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="13"/>
<wire x1="160.02" y1="279.4" x2="160.02" y2="254" width="0.1524" layer="91"/>
<pinref part="G183" gate="G$1" pin="1"/>
<wire x1="160.02" y1="254" x2="172.72" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$183" class="0">
<segment>
<wire x1="149.86" y1="276.86" x2="157.48" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV23" gate="1" pin="15"/>
<pinref part="G184" gate="G$1" pin="1"/>
<wire x1="157.48" y1="276.86" x2="157.48" y2="241.3" width="0.1524" layer="91"/>
<wire x1="157.48" y1="241.3" x2="172.72" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$184" class="0">
<segment>
<wire x1="213.36" y1="294.64" x2="220.98" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="1"/>
<pinref part="G185" gate="G$1" pin="1"/>
<wire x1="220.98" y1="294.64" x2="220.98" y2="330.2" width="0.1524" layer="91"/>
<wire x1="220.98" y1="330.2" x2="236.22" y2="330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$185" class="0">
<segment>
<pinref part="G186" gate="G$1" pin="1"/>
<wire x1="223.52" y1="317.5" x2="236.22" y2="317.5" width="0.1524" layer="91"/>
<wire x1="223.52" y1="317.5" x2="223.52" y2="292.1" width="0.1524" layer="91"/>
<wire x1="213.36" y1="292.1" x2="223.52" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="3"/>
</segment>
</net>
<net name="N$186" class="0">
<segment>
<pinref part="G187" gate="G$1" pin="1"/>
<wire x1="226.06" y1="304.8" x2="236.22" y2="304.8" width="0.1524" layer="91"/>
<wire x1="226.06" y1="304.8" x2="226.06" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="5"/>
<wire x1="226.06" y1="289.56" x2="213.36" y2="289.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$187" class="0">
<segment>
<pinref part="SV24" gate="1" pin="7"/>
<pinref part="G188" gate="G$1" pin="1"/>
<wire x1="228.6" y1="292.1" x2="236.22" y2="292.1" width="0.1524" layer="91"/>
<wire x1="213.36" y1="287.02" x2="228.6" y2="287.02" width="0.1524" layer="91"/>
<wire x1="228.6" y1="287.02" x2="228.6" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$188" class="0">
<segment>
<pinref part="G189" gate="G$1" pin="1"/>
<wire x1="228.6" y1="279.4" x2="236.22" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="9"/>
<wire x1="228.6" y1="279.4" x2="228.6" y2="284.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="284.48" x2="213.36" y2="284.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$189" class="0">
<segment>
<wire x1="213.36" y1="281.94" x2="226.06" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="11"/>
<wire x1="226.06" y1="281.94" x2="226.06" y2="266.7" width="0.1524" layer="91"/>
<pinref part="G190" gate="G$1" pin="1"/>
<wire x1="226.06" y1="266.7" x2="236.22" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$190" class="0">
<segment>
<wire x1="213.36" y1="279.4" x2="223.52" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="13"/>
<wire x1="223.52" y1="279.4" x2="223.52" y2="254" width="0.1524" layer="91"/>
<pinref part="G191" gate="G$1" pin="1"/>
<wire x1="223.52" y1="254" x2="236.22" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$191" class="0">
<segment>
<wire x1="213.36" y1="276.86" x2="220.98" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV24" gate="1" pin="15"/>
<pinref part="G192" gate="G$1" pin="1"/>
<wire x1="220.98" y1="276.86" x2="220.98" y2="241.3" width="0.1524" layer="91"/>
<wire x1="220.98" y1="241.3" x2="236.22" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$256" class="0">
<segment>
<wire x1="279.4" y1="408.94" x2="284.48" y2="408.94" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="1"/>
<wire x1="284.48" y1="408.94" x2="284.48" y2="444.5" width="0.1524" layer="91"/>
<pinref part="G257" gate="G$1" pin="1"/>
<wire x1="284.48" y1="444.5" x2="302.26" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$257" class="0">
<segment>
<wire x1="279.4" y1="406.4" x2="287.02" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="3"/>
<wire x1="287.02" y1="406.4" x2="287.02" y2="431.8" width="0.1524" layer="91"/>
<pinref part="G258" gate="G$1" pin="1"/>
<wire x1="287.02" y1="431.8" x2="302.26" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$258" class="0">
<segment>
<wire x1="279.4" y1="403.86" x2="289.56" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="5"/>
<wire x1="289.56" y1="403.86" x2="289.56" y2="419.1" width="0.1524" layer="91"/>
<pinref part="G259" gate="G$1" pin="1"/>
<wire x1="289.56" y1="419.1" x2="302.26" y2="419.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$259" class="0">
<segment>
<pinref part="SV33" gate="1" pin="7"/>
<wire x1="279.4" y1="401.32" x2="292.1" y2="401.32" width="0.1524" layer="91"/>
<wire x1="292.1" y1="401.32" x2="292.1" y2="406.4" width="0.1524" layer="91"/>
<pinref part="G260" gate="G$1" pin="1"/>
<wire x1="292.1" y1="406.4" x2="302.26" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$260" class="0">
<segment>
<pinref part="SV33" gate="1" pin="9"/>
<wire x1="279.4" y1="398.78" x2="292.1" y2="398.78" width="0.1524" layer="91"/>
<wire x1="292.1" y1="398.78" x2="292.1" y2="393.7" width="0.1524" layer="91"/>
<pinref part="G261" gate="G$1" pin="1"/>
<wire x1="292.1" y1="393.7" x2="302.26" y2="393.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$261" class="0">
<segment>
<wire x1="279.4" y1="396.24" x2="289.56" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="11"/>
<wire x1="289.56" y1="396.24" x2="289.56" y2="381" width="0.1524" layer="91"/>
<pinref part="G262" gate="G$1" pin="1"/>
<wire x1="289.56" y1="381" x2="302.26" y2="381" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$262" class="0">
<segment>
<wire x1="279.4" y1="393.7" x2="287.02" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="13"/>
<wire x1="287.02" y1="393.7" x2="287.02" y2="368.3" width="0.1524" layer="91"/>
<pinref part="G263" gate="G$1" pin="1"/>
<wire x1="287.02" y1="368.3" x2="302.26" y2="368.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$263" class="0">
<segment>
<wire x1="279.4" y1="391.16" x2="284.48" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV33" gate="1" pin="15"/>
<wire x1="284.48" y1="391.16" x2="284.48" y2="355.6" width="0.1524" layer="91"/>
<pinref part="G264" gate="G$1" pin="1"/>
<wire x1="284.48" y1="355.6" x2="302.26" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$264" class="0">
<segment>
<wire x1="342.9" y1="408.94" x2="350.52" y2="408.94" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="1"/>
<wire x1="350.52" y1="408.94" x2="350.52" y2="444.5" width="0.1524" layer="91"/>
<pinref part="G265" gate="G$1" pin="1"/>
<wire x1="350.52" y1="444.5" x2="365.76" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$265" class="0">
<segment>
<pinref part="G266" gate="G$1" pin="1"/>
<wire x1="353.06" y1="431.8" x2="365.76" y2="431.8" width="0.1524" layer="91"/>
<wire x1="353.06" y1="431.8" x2="353.06" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="3"/>
<wire x1="353.06" y1="406.4" x2="342.9" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$266" class="0">
<segment>
<wire x1="342.9" y1="403.86" x2="355.6" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="5"/>
<wire x1="355.6" y1="403.86" x2="355.6" y2="419.1" width="0.1524" layer="91"/>
<pinref part="G267" gate="G$1" pin="1"/>
<wire x1="355.6" y1="419.1" x2="365.76" y2="419.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$267" class="0">
<segment>
<pinref part="SV34" gate="1" pin="7"/>
<wire x1="342.9" y1="401.32" x2="358.14" y2="401.32" width="0.1524" layer="91"/>
<pinref part="G268" gate="G$1" pin="1"/>
<wire x1="358.14" y1="406.4" x2="365.76" y2="406.4" width="0.1524" layer="91"/>
<wire x1="358.14" y1="401.32" x2="358.14" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$268" class="0">
<segment>
<pinref part="SV34" gate="1" pin="9"/>
<pinref part="G269" gate="G$1" pin="1"/>
<wire x1="358.14" y1="393.7" x2="365.76" y2="393.7" width="0.1524" layer="91"/>
<wire x1="342.9" y1="398.78" x2="358.14" y2="398.78" width="0.1524" layer="91"/>
<wire x1="358.14" y1="398.78" x2="358.14" y2="393.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$269" class="0">
<segment>
<wire x1="342.9" y1="396.24" x2="355.6" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="11"/>
<wire x1="355.6" y1="396.24" x2="355.6" y2="381" width="0.1524" layer="91"/>
<pinref part="G270" gate="G$1" pin="1"/>
<wire x1="355.6" y1="381" x2="365.76" y2="381" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$270" class="0">
<segment>
<wire x1="342.9" y1="393.7" x2="353.06" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="13"/>
<wire x1="353.06" y1="393.7" x2="353.06" y2="368.3" width="0.1524" layer="91"/>
<pinref part="G271" gate="G$1" pin="1"/>
<wire x1="353.06" y1="368.3" x2="365.76" y2="368.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$271" class="0">
<segment>
<wire x1="342.9" y1="391.16" x2="350.52" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV34" gate="1" pin="15"/>
<wire x1="350.52" y1="391.16" x2="350.52" y2="355.6" width="0.1524" layer="91"/>
<pinref part="G272" gate="G$1" pin="1"/>
<wire x1="350.52" y1="355.6" x2="365.76" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$272" class="0">
<segment>
<wire x1="406.4" y1="408.94" x2="414.02" y2="408.94" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="1"/>
<wire x1="414.02" y1="408.94" x2="414.02" y2="444.5" width="0.1524" layer="91"/>
<pinref part="G273" gate="G$1" pin="1"/>
<wire x1="414.02" y1="444.5" x2="429.26" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$273" class="0">
<segment>
<pinref part="G274" gate="G$1" pin="1"/>
<wire x1="416.56" y1="431.8" x2="429.26" y2="431.8" width="0.1524" layer="91"/>
<wire x1="416.56" y1="431.8" x2="416.56" y2="406.4" width="0.1524" layer="91"/>
<wire x1="406.4" y1="406.4" x2="416.56" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="3"/>
</segment>
</net>
<net name="N$274" class="0">
<segment>
<pinref part="G275" gate="G$1" pin="1"/>
<wire x1="419.1" y1="419.1" x2="429.26" y2="419.1" width="0.1524" layer="91"/>
<wire x1="419.1" y1="419.1" x2="419.1" y2="403.86" width="0.1524" layer="91"/>
<wire x1="406.4" y1="403.86" x2="419.1" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="5"/>
</segment>
</net>
<net name="N$275" class="0">
<segment>
<pinref part="SV35" gate="1" pin="7"/>
<wire x1="406.4" y1="401.32" x2="421.64" y2="401.32" width="0.1524" layer="91"/>
<pinref part="G276" gate="G$1" pin="1"/>
<wire x1="421.64" y1="406.4" x2="429.26" y2="406.4" width="0.1524" layer="91"/>
<wire x1="421.64" y1="401.32" x2="421.64" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$276" class="0">
<segment>
<pinref part="G277" gate="G$1" pin="1"/>
<wire x1="421.64" y1="393.7" x2="429.26" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="9"/>
<wire x1="421.64" y1="393.7" x2="421.64" y2="398.78" width="0.1524" layer="91"/>
<wire x1="421.64" y1="398.78" x2="406.4" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$277" class="0">
<segment>
<wire x1="406.4" y1="396.24" x2="419.1" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="11"/>
<wire x1="419.1" y1="396.24" x2="419.1" y2="381" width="0.1524" layer="91"/>
<pinref part="G278" gate="G$1" pin="1"/>
<wire x1="419.1" y1="381" x2="429.26" y2="381" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$278" class="0">
<segment>
<wire x1="406.4" y1="393.7" x2="416.56" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="13"/>
<wire x1="416.56" y1="393.7" x2="416.56" y2="368.3" width="0.1524" layer="91"/>
<pinref part="G279" gate="G$1" pin="1"/>
<wire x1="416.56" y1="368.3" x2="429.26" y2="368.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$279" class="0">
<segment>
<wire x1="406.4" y1="391.16" x2="414.02" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV35" gate="1" pin="15"/>
<wire x1="414.02" y1="391.16" x2="414.02" y2="355.6" width="0.1524" layer="91"/>
<pinref part="G280" gate="G$1" pin="1"/>
<wire x1="414.02" y1="355.6" x2="429.26" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$280" class="0">
<segment>
<wire x1="469.9" y1="408.94" x2="477.52" y2="408.94" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="1"/>
<wire x1="477.52" y1="408.94" x2="477.52" y2="444.5" width="0.1524" layer="91"/>
<pinref part="G281" gate="G$1" pin="1"/>
<wire x1="477.52" y1="444.5" x2="492.76" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$281" class="0">
<segment>
<pinref part="G282" gate="G$1" pin="1"/>
<wire x1="480.06" y1="431.8" x2="492.76" y2="431.8" width="0.1524" layer="91"/>
<wire x1="480.06" y1="431.8" x2="480.06" y2="406.4" width="0.1524" layer="91"/>
<wire x1="469.9" y1="406.4" x2="480.06" y2="406.4" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="3"/>
</segment>
</net>
<net name="N$282" class="0">
<segment>
<pinref part="G283" gate="G$1" pin="1"/>
<wire x1="482.6" y1="419.1" x2="492.76" y2="419.1" width="0.1524" layer="91"/>
<wire x1="482.6" y1="419.1" x2="482.6" y2="403.86" width="0.1524" layer="91"/>
<wire x1="469.9" y1="403.86" x2="482.6" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="5"/>
</segment>
</net>
<net name="N$283" class="0">
<segment>
<pinref part="SV36" gate="1" pin="7"/>
<wire x1="469.9" y1="401.32" x2="485.14" y2="401.32" width="0.1524" layer="91"/>
<pinref part="G284" gate="G$1" pin="1"/>
<wire x1="485.14" y1="406.4" x2="492.76" y2="406.4" width="0.1524" layer="91"/>
<wire x1="485.14" y1="401.32" x2="485.14" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$284" class="0">
<segment>
<pinref part="G285" gate="G$1" pin="1"/>
<wire x1="485.14" y1="393.7" x2="492.76" y2="393.7" width="0.1524" layer="91"/>
<wire x1="485.14" y1="393.7" x2="485.14" y2="398.78" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="9"/>
<wire x1="485.14" y1="398.78" x2="469.9" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$285" class="0">
<segment>
<wire x1="469.9" y1="396.24" x2="482.6" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="11"/>
<wire x1="482.6" y1="396.24" x2="482.6" y2="381" width="0.1524" layer="91"/>
<pinref part="G286" gate="G$1" pin="1"/>
<wire x1="482.6" y1="381" x2="492.76" y2="381" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$286" class="0">
<segment>
<wire x1="469.9" y1="393.7" x2="480.06" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="13"/>
<wire x1="480.06" y1="393.7" x2="480.06" y2="368.3" width="0.1524" layer="91"/>
<pinref part="G287" gate="G$1" pin="1"/>
<wire x1="480.06" y1="368.3" x2="492.76" y2="368.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$287" class="0">
<segment>
<wire x1="469.9" y1="391.16" x2="477.52" y2="391.16" width="0.1524" layer="91"/>
<pinref part="SV36" gate="1" pin="15"/>
<wire x1="477.52" y1="391.16" x2="477.52" y2="355.6" width="0.1524" layer="91"/>
<pinref part="G288" gate="G$1" pin="1"/>
<wire x1="477.52" y1="355.6" x2="492.76" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$288" class="0">
<segment>
<wire x1="279.4" y1="294.64" x2="287.02" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="1"/>
<wire x1="287.02" y1="294.64" x2="287.02" y2="330.2" width="0.1524" layer="91"/>
<pinref part="G289" gate="G$1" pin="1"/>
<wire x1="287.02" y1="330.2" x2="302.26" y2="330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$289" class="0">
<segment>
<pinref part="G290" gate="G$1" pin="1"/>
<wire x1="289.56" y1="317.5" x2="302.26" y2="317.5" width="0.1524" layer="91"/>
<wire x1="279.4" y1="292.1" x2="289.56" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="3"/>
<wire x1="289.56" y1="317.5" x2="289.56" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$290" class="0">
<segment>
<pinref part="G291" gate="G$1" pin="1"/>
<wire x1="292.1" y1="304.8" x2="302.26" y2="304.8" width="0.1524" layer="91"/>
<wire x1="292.1" y1="304.8" x2="292.1" y2="289.56" width="0.1524" layer="91"/>
<wire x1="279.4" y1="289.56" x2="292.1" y2="289.56" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="5"/>
</segment>
</net>
<net name="N$291" class="0">
<segment>
<pinref part="G292" gate="G$1" pin="1"/>
<wire x1="294.64" y1="292.1" x2="302.26" y2="292.1" width="0.1524" layer="91"/>
<wire x1="294.64" y1="292.1" x2="294.64" y2="287.02" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="7"/>
<wire x1="294.64" y1="287.02" x2="279.4" y2="287.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$292" class="0">
<segment>
<pinref part="SV37" gate="1" pin="9"/>
<wire x1="279.4" y1="284.48" x2="294.64" y2="284.48" width="0.1524" layer="91"/>
<pinref part="G293" gate="G$1" pin="1"/>
<wire x1="294.64" y1="279.4" x2="302.26" y2="279.4" width="0.1524" layer="91"/>
<wire x1="294.64" y1="284.48" x2="294.64" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$293" class="0">
<segment>
<wire x1="279.4" y1="281.94" x2="292.1" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="11"/>
<wire x1="292.1" y1="281.94" x2="292.1" y2="266.7" width="0.1524" layer="91"/>
<pinref part="G294" gate="G$1" pin="1"/>
<wire x1="292.1" y1="266.7" x2="302.26" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$294" class="0">
<segment>
<wire x1="279.4" y1="279.4" x2="289.56" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="13"/>
<pinref part="G295" gate="G$1" pin="1"/>
<wire x1="289.56" y1="254" x2="302.26" y2="254" width="0.1524" layer="91"/>
<wire x1="289.56" y1="279.4" x2="289.56" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$295" class="0">
<segment>
<wire x1="279.4" y1="276.86" x2="287.02" y2="276.86" width="0.1524" layer="91"/>
<pinref part="SV37" gate="1" pin="15"/>
<pinref part="G296" gate="G$1" pin="1"/>
<wire x1="287.02" y1="276.86" x2="287.02" y2="241.3" width="0.1524" layer="91"/>
<wire x1="287.02" y1="241.3" x2="302.26" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$296" class="0">
<segment>
<wire x1="342.9" y1="297.18" x2="350.52" y2="297.18" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="1"/>
<pinref part="G297" gate="G$1" pin="1"/>
<wire x1="350.52" y1="297.18" x2="350.52" y2="332.74" width="0.1524" layer="91"/>
<wire x1="350.52" y1="332.74" x2="365.76" y2="332.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$297" class="0">
<segment>
<pinref part="G298" gate="G$1" pin="1"/>
<wire x1="353.06" y1="320.04" x2="365.76" y2="320.04" width="0.1524" layer="91"/>
<wire x1="353.06" y1="320.04" x2="353.06" y2="294.64" width="0.1524" layer="91"/>
<wire x1="342.9" y1="294.64" x2="353.06" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="3"/>
</segment>
</net>
<net name="N$298" class="0">
<segment>
<pinref part="G299" gate="G$1" pin="1"/>
<wire x1="355.6" y1="307.34" x2="365.76" y2="307.34" width="0.1524" layer="91"/>
<wire x1="355.6" y1="307.34" x2="355.6" y2="292.1" width="0.1524" layer="91"/>
<wire x1="342.9" y1="292.1" x2="355.6" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="5"/>
</segment>
</net>
<net name="N$299" class="0">
<segment>
<pinref part="SV38" gate="1" pin="7"/>
<pinref part="G300" gate="G$1" pin="1"/>
<wire x1="358.14" y1="294.64" x2="365.76" y2="294.64" width="0.1524" layer="91"/>
<wire x1="342.9" y1="289.56" x2="358.14" y2="289.56" width="0.1524" layer="91"/>
<wire x1="358.14" y1="289.56" x2="358.14" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$300" class="0">
<segment>
<pinref part="G301" gate="G$1" pin="1"/>
<wire x1="358.14" y1="281.94" x2="365.76" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="9"/>
<wire x1="358.14" y1="281.94" x2="358.14" y2="287.02" width="0.1524" layer="91"/>
<wire x1="358.14" y1="287.02" x2="342.9" y2="287.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$301" class="0">
<segment>
<wire x1="342.9" y1="284.48" x2="355.6" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="11"/>
<wire x1="355.6" y1="284.48" x2="355.6" y2="269.24" width="0.1524" layer="91"/>
<pinref part="G302" gate="G$1" pin="1"/>
<wire x1="355.6" y1="269.24" x2="365.76" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$302" class="0">
<segment>
<wire x1="342.9" y1="281.94" x2="353.06" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="13"/>
<wire x1="353.06" y1="281.94" x2="353.06" y2="256.54" width="0.1524" layer="91"/>
<pinref part="G303" gate="G$1" pin="1"/>
<wire x1="353.06" y1="256.54" x2="365.76" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$303" class="0">
<segment>
<wire x1="342.9" y1="279.4" x2="350.52" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV38" gate="1" pin="15"/>
<pinref part="G304" gate="G$1" pin="1"/>
<wire x1="350.52" y1="279.4" x2="350.52" y2="243.84" width="0.1524" layer="91"/>
<wire x1="350.52" y1="243.84" x2="365.76" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$304" class="0">
<segment>
<wire x1="406.4" y1="297.18" x2="414.02" y2="297.18" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="1"/>
<pinref part="G305" gate="G$1" pin="1"/>
<wire x1="414.02" y1="297.18" x2="414.02" y2="332.74" width="0.1524" layer="91"/>
<wire x1="414.02" y1="332.74" x2="429.26" y2="332.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$305" class="0">
<segment>
<pinref part="G306" gate="G$1" pin="1"/>
<wire x1="416.56" y1="320.04" x2="429.26" y2="320.04" width="0.1524" layer="91"/>
<wire x1="416.56" y1="320.04" x2="416.56" y2="294.64" width="0.1524" layer="91"/>
<wire x1="406.4" y1="294.64" x2="416.56" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="3"/>
</segment>
</net>
<net name="N$306" class="0">
<segment>
<pinref part="G307" gate="G$1" pin="1"/>
<wire x1="419.1" y1="307.34" x2="429.26" y2="307.34" width="0.1524" layer="91"/>
<wire x1="419.1" y1="307.34" x2="419.1" y2="292.1" width="0.1524" layer="91"/>
<wire x1="406.4" y1="292.1" x2="419.1" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="5"/>
</segment>
</net>
<net name="N$307" class="0">
<segment>
<pinref part="SV39" gate="1" pin="7"/>
<pinref part="G308" gate="G$1" pin="1"/>
<wire x1="421.64" y1="294.64" x2="429.26" y2="294.64" width="0.1524" layer="91"/>
<wire x1="406.4" y1="289.56" x2="421.64" y2="289.56" width="0.1524" layer="91"/>
<wire x1="421.64" y1="289.56" x2="421.64" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$308" class="0">
<segment>
<pinref part="SV39" gate="1" pin="9"/>
<pinref part="G309" gate="G$1" pin="1"/>
<wire x1="421.64" y1="281.94" x2="429.26" y2="281.94" width="0.1524" layer="91"/>
<wire x1="406.4" y1="287.02" x2="421.64" y2="287.02" width="0.1524" layer="91"/>
<wire x1="421.64" y1="287.02" x2="421.64" y2="281.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$309" class="0">
<segment>
<wire x1="406.4" y1="284.48" x2="419.1" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="11"/>
<wire x1="419.1" y1="284.48" x2="419.1" y2="269.24" width="0.1524" layer="91"/>
<pinref part="G310" gate="G$1" pin="1"/>
<wire x1="419.1" y1="269.24" x2="429.26" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$310" class="0">
<segment>
<wire x1="406.4" y1="281.94" x2="416.56" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="13"/>
<wire x1="416.56" y1="281.94" x2="416.56" y2="256.54" width="0.1524" layer="91"/>
<pinref part="G311" gate="G$1" pin="1"/>
<wire x1="416.56" y1="256.54" x2="429.26" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$311" class="0">
<segment>
<wire x1="406.4" y1="279.4" x2="414.02" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV39" gate="1" pin="15"/>
<pinref part="G312" gate="G$1" pin="1"/>
<wire x1="414.02" y1="279.4" x2="414.02" y2="243.84" width="0.1524" layer="91"/>
<wire x1="414.02" y1="243.84" x2="429.26" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$312" class="0">
<segment>
<wire x1="469.9" y1="297.18" x2="477.52" y2="297.18" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="1"/>
<pinref part="G313" gate="G$1" pin="1"/>
<wire x1="477.52" y1="297.18" x2="477.52" y2="332.74" width="0.1524" layer="91"/>
<wire x1="477.52" y1="332.74" x2="492.76" y2="332.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$313" class="0">
<segment>
<pinref part="G314" gate="G$1" pin="1"/>
<wire x1="480.06" y1="320.04" x2="492.76" y2="320.04" width="0.1524" layer="91"/>
<wire x1="480.06" y1="320.04" x2="480.06" y2="294.64" width="0.1524" layer="91"/>
<wire x1="469.9" y1="294.64" x2="480.06" y2="294.64" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="3"/>
</segment>
</net>
<net name="N$314" class="0">
<segment>
<pinref part="G315" gate="G$1" pin="1"/>
<wire x1="482.6" y1="307.34" x2="492.76" y2="307.34" width="0.1524" layer="91"/>
<wire x1="482.6" y1="307.34" x2="482.6" y2="292.1" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="5"/>
<wire x1="482.6" y1="292.1" x2="469.9" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$315" class="0">
<segment>
<pinref part="SV40" gate="1" pin="7"/>
<pinref part="G316" gate="G$1" pin="1"/>
<wire x1="485.14" y1="294.64" x2="492.76" y2="294.64" width="0.1524" layer="91"/>
<wire x1="469.9" y1="289.56" x2="485.14" y2="289.56" width="0.1524" layer="91"/>
<wire x1="485.14" y1="289.56" x2="485.14" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$316" class="0">
<segment>
<pinref part="G317" gate="G$1" pin="1"/>
<wire x1="485.14" y1="281.94" x2="492.76" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="9"/>
<wire x1="485.14" y1="281.94" x2="485.14" y2="287.02" width="0.1524" layer="91"/>
<wire x1="485.14" y1="287.02" x2="469.9" y2="287.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$317" class="0">
<segment>
<wire x1="469.9" y1="284.48" x2="482.6" y2="284.48" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="11"/>
<wire x1="482.6" y1="284.48" x2="482.6" y2="269.24" width="0.1524" layer="91"/>
<pinref part="G318" gate="G$1" pin="1"/>
<wire x1="482.6" y1="269.24" x2="492.76" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$318" class="0">
<segment>
<wire x1="469.9" y1="281.94" x2="480.06" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="13"/>
<wire x1="480.06" y1="281.94" x2="480.06" y2="256.54" width="0.1524" layer="91"/>
<pinref part="G319" gate="G$1" pin="1"/>
<wire x1="480.06" y1="256.54" x2="492.76" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$319" class="0">
<segment>
<wire x1="469.9" y1="279.4" x2="477.52" y2="279.4" width="0.1524" layer="91"/>
<pinref part="SV40" gate="1" pin="15"/>
<pinref part="G320" gate="G$1" pin="1"/>
<wire x1="477.52" y1="279.4" x2="477.52" y2="243.84" width="0.1524" layer="91"/>
<wire x1="477.52" y1="243.84" x2="492.76" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
